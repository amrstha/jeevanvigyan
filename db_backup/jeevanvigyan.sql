-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 23, 2017 at 01:43 AM
-- Server version: 10.0.28-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jeevanvigyan`
--

DELIMITER $$
--
-- Procedures
--
CREATE PROCEDURE `proc_access_control` ()  BEGIN
	SET @sql_query = NULL;

	SELECT
		GROUP_CONCAT(DISTINCT
			CONCAT(
				'MAX(IF(A.amodule_id = ',
				id,
				', 1, IF(G.id = 1, 1, 0))) AS ',
				name
			)
		) INTO @sql_query
	FROM core_access_modules;

	SET @sql_query = CONCAT('SELECT G.id AS group_id
				, G.name AS groups, ', @sql_query, '
			FROM auth_groups G
			LEFT JOIN core_access_control AS A
				ON G.id = A.ugroup_id
			GROUP BY G.id');

	PREPARE stmt FROM @sql_query;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `api_users_token`
--

CREATE TABLE `api_users_token` (
  `id` int(11) UNSIGNED NOT NULL,
  `user` mediumint(8) UNSIGNED NOT NULL,
  `token` text NOT NULL,
  `ttl` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `api_users_token`
--

INSERT INTO `api_users_token` (`id`, `user`, `token`, `ttl`, `created_at`) VALUES
(1, 2, 'eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzI1NiJ9.eyJjb25zdW1lcktleSI6Ilp3WlNVVTUwT1cyZG9VVFhOSFNVTllhR1pPV1V3UTBjM1oyZG9Vd1EwY05ZVVUzWlVUMDlTTkhYNXdaU1VhRzkiLCJ1c2VySWQiOiIyIiwiaXNzdWVkQXQiOiIyMDE3LTAxLTIzVDAwOjUyOjI5KzA1NDUiLCJ0dGwiOjg2NDAwfQ.g8n_5hhpFBksDIT1xFkKck0X-LvSC-l6eWbq9WlWhPU', '1485198449', '2017-01-20 15:40:36');

-- --------------------------------------------------------

--
-- Table structure for table `auth_customer`
--

CREATE TABLE `auth_customer` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `ip_address` varchar(16) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(80) NOT NULL,
  `salt` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `dob` varchar(50) DEFAULT NULL,
  `gender` enum('male','female','other') DEFAULT NULL,
  `addressline` varchar(255) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `occupation` varchar(100) DEFAULT NULL,
  `organization` varchar(100) DEFAULT NULL,
  `education` varchar(100) DEFAULT NULL,
  `institution` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `profile_pic` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_customer`
--

INSERT INTO `auth_customer` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `dob`, `gender`, `addressline`, `city`, `country`, `occupation`, `organization`, `education`, `institution`, `phone`, `profile_pic`) VALUES
(1, '127.0.0.1', 'user', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'user@user.com', '', NULL, NULL, NULL, 1268889823, 1268889823, 1, 'Front', 'User', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '127.0.0.1', 'puncoz', '$2y$08$ZHb1vPpe3VPqQKPJVgu/0u5rQxBf4.S8VYgg9fXa9fau5.TR.tfga', '', 'info@puncoz.com', NULL, NULL, NULL, NULL, 1484919245, 1485112049, 1, 'Puncoz', 'Nepal', '1992-03-18', 'male', 'Balkot', 'Bhaktapur', 'Nepal', 'Developer', 'My Life Mark', 'Engineering', 'IOE', '9849057718', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_groups`
--

CREATE TABLE `auth_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `is_editable` tinyint(4) DEFAULT '1',
  `is_userdeletable` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_groups`
--

INSERT INTO `auth_groups` (`id`, `name`, `description`, `is_editable`, `is_userdeletable`) VALUES
(1, 'superadmin', 'Super Administrator', 0, 0),
(2, 'members', 'General User', 1, 1),
(3, 'admin', 'Administrator', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `auth_login_attempts`
--

CREATE TABLE `auth_login_attempts` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `ip_address` varchar(16) NOT NULL,
  `login` varchar(100) DEFAULT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_users`
--

CREATE TABLE `auth_users` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `ip_address` varchar(16) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(80) NOT NULL,
  `salt` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT '1',
  `name_prefix` varchar(20) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `name_suffix` varchar(100) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `profile_pic` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_users`
--

INSERT INTO `auth_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `name_prefix`, `first_name`, `middle_name`, `last_name`, `name_suffix`, `gender`, `company`, `designation`, `phone`, `profile_pic`) VALUES
(1, '127.0.0.1', 'admin', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, '/I/ffKV9bS8qiSdjsCzaee', 1268889823, 1485108901, 1, 'Er.', 'Puncoz', '', 'Nepal', '', 'male', 'MyLifeMark', 'Developer', '9849057718', 'd94274ba19286998c698aa67d026c4e2.jpg'),
(2, '127.0.0.1', 'test', '$2y$08$RUTeH0kTiYq7EmzQmw1xheJiwQaBhPp4Y2AwvWNHOyP/EdZespkna', '', 'test.user@gmail.com', NULL, NULL, NULL, NULL, 1484640056, 1484649147, 1, '', 'Test', '', 'Test', '', 'female', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `auth_users_groups`
--

CREATE TABLE `auth_users_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `user_id` mediumint(8) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_users_groups`
--

INSERT INTO `auth_users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(14, 2, 2),
(15, 2, 3),
(16, 2, 2),
(17, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `core_access_control`
--

CREATE TABLE `core_access_control` (
  `id` int(11) UNSIGNED NOT NULL,
  `ugroup_id` mediumint(8) UNSIGNED NOT NULL,
  `amodule_id` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `core_access_modules`
--

CREATE TABLE `core_access_modules` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` int(5) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `core_access_modules`
--

INSERT INTO `core_access_modules` (`id`, `name`, `description`, `status`, `created_at`) VALUES
(1, 'test', 'test modules', 1, '2017-01-17 11:15:07');

-- --------------------------------------------------------

--
-- Table structure for table `jeevan_sessions`
--

CREATE TABLE `jeevan_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jeevan_sessions`
--

INSERT INTO `jeevan_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('0ieccm9gnmkousn7um0bsi70f2vdovso', '127.0.0.1', 1484753076, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438343735323833343b6964656e746974797c733a353a2261646d696e223b757365726e616d657c733a353a2261646d696e223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343834373330383138223b737563636573735f6e6f746966797c733a32333a2255706461746564205375636365737366756c6c79212121223b5f5f63695f766172737c613a313a7b733a31343a22737563636573735f6e6f74696679223b733a333a226f6c64223b7d),
('601l5c29p35spkt8fuq2tehokbern1ln', '127.0.0.1', 1484687857, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438343638373633323b6964656e746974797c733a353a2261646d696e223b757365726e616d657c733a353a2261646d696e223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343834363835353538223b),
('cgv9bqf3b1bn28ngu3e1lj8opeot6497', '127.0.0.1', 1485033685, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438353033333636303b),
('dh4ui84oaud76obuhmv04i6e91n1t905', '127.0.0.1', 1484742414, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438343734323430333b),
('do5us2t38li77lcc261vgahlomoiouaa', '127.0.0.1', 1485075081, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438353037353037393b),
('hcpodb0581r7vvnq5s1otm8jkfcecb7j', '127.0.0.1', 1485081905, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438353037373533313b6964656e746974797c733a353a2261646d696e223b757365726e616d657c733a353a2261646d696e223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343835303332323230223b),
('hohmvhv600e035lujhvk477k3icedaiq', '127.0.0.1', 1484938176, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438343933383030373b6964656e746974797c733a363a2270756e636f7a223b757365726e616d657c733a363a2270756e636f7a223b656d61696c7c733a31353a22696e666f4070756e636f7a2e636f6d223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343834393330323739223b),
('hr42lk4k5kf3elvijb0en9kt8hmmo1ec', '127.0.0.1', 1484599811, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438343539393830393b737563636573735f616c6572747c733a32333a224c6f67676564204f7574205375636365737366756c6c79223b5f5f63695f766172737c613a313a7b733a31333a22737563636573735f616c657274223b733a333a226f6c64223b7d),
('ikqevl8gu7gio1j2an2jss8ck7375vlm', '127.0.0.1', 1484938965, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438343933383933383b6964656e746974797c733a353a2261646d696e223b757365726e616d657c733a353a2261646d696e223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343834383136383932223b),
('j6ph6qc4vjdhvo18hbh07entq27vsgfa', '127.0.0.1', 1484665484, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438343636353438313b7761726e696e675f616c6572747c733a32383a22596f75206d757374206c6f6720696e20746f20636f6e74696e75652e223b5f5f63695f766172737c613a313a7b733a31333a227761726e696e675f616c657274223b733a333a226f6c64223b7d),
('kjcou2tnf19udkl50fncr65b8s1ath5g', '127.0.0.1', 1484761732, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438343736313637393b6964656e746974797c733a353a2261646d696e223b757365726e616d657c733a353a2261646d696e223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343834373533303530223b),
('rfnlj2m8274trc63angdtrtsohmhft85', '127.0.0.1', 1484649322, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438343634393332303b737563636573735f616c6572747c733a32333a224c6f67676564204f7574205375636365737366756c6c79223b5f5f63695f766172737c613a313a7b733a31333a22737563636573735f616c657274223b733a333a226f6c64223b7d),
('rglhmjju7ugaf3455016o9a1tbdb9hve', '127.0.0.1', 1484653039, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438343635323833383b6964656e746974797c733a353a2261646d696e223b757365726e616d657c733a353a2261646d696e223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343834353838383236223b),
('s4oltd6kpih7hq62p0c2bves0ol0esg7', '127.0.0.1', 1484738643, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438343733383634333b6964656e746974797c733a353a2261646d696e223b757365726e616d657c733a353a2261646d696e223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343834363836363435223b),
('sunsk775ervi8ef2f0lnlr75kfdv10tt', '127.0.0.1', 1484822491, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438343832323334323b6964656e746974797c733a353a2261646d696e223b757365726e616d657c733a353a2261646d696e223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343834373539373035223b),
('td3bif0q6mkjdfhajk0hvhu6g1387uv3', '127.0.0.1', 1485112332, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438353131323332333b6964656e746974797c733a363a2270756e636f7a223b757365726e616d657c733a363a2270756e636f7a223b656d61696c7c733a31353a22696e666f4070756e636f7a2e636f6d223b757365725f69647c733a313a2232223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343835313131393330223b),
('vl88qio6dbtqbg25ks7r1fl6v8gd27mn', '127.0.0.1', 1485113968, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438353131333932393b6964656e746974797c733a353a2261646d696e223b757365726e616d657c733a353a2261646d696e223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231343835303735383538223b);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(20170123000257);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_events`
--

CREATE TABLE `tbl_events` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(150) NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `description` text,
  `cover_pics` varchar(150) DEFAULT NULL,
  `status` int(5) NOT NULL DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_events`
--

INSERT INTO `tbl_events` (`id`, `title`, `date_start`, `date_end`, `description`, `cover_pics`, `status`, `updated_at`, `created_at`) VALUES
(1, 'test event title', '2017-01-25 00:00:00', '2017-01-27 00:00:00', 'this is a description of test event title', 'cover-image-test-event.jpg', 1, NULL, '2017-01-22 18:33:03'),
(2, 'test event second title', '2017-01-24 00:00:00', '2017-01-31 00:00:00', 'this is a description of test event title', 'cover-image-test-event.jpg', 1, NULL, '2017-01-22 18:33:03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_events_book`
--

CREATE TABLE `tbl_events_book` (
  `id` int(11) UNSIGNED NOT NULL,
  `user` mediumint(8) UNSIGNED NOT NULL,
  `event` int(11) UNSIGNED NOT NULL,
  `status` int(5) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sliders`
--

CREATE TABLE `tbl_sliders` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL,
  `title_np` varchar(100) NOT NULL,
  `caption` varchar(255) DEFAULT NULL,
  `caption_np` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` int(5) NOT NULL DEFAULT '0',
  `order_by` int(5) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_sliders`
--

INSERT INTO `tbl_sliders` (`id`, `title`, `title_np`, `caption`, `caption_np`, `image`, `status`, `order_by`, `created_at`) VALUES
(2, 'Slider title', 'अञ्चल कार्यालयहरु title', 'Slider Caption', 'अञ्चल कार्यालयहरु caption', '70ee4dde181603fd0f63709ab4fed8c6.jpg', 1, 1, '2017-01-18 11:09:25'),
(3, 'Another Slider title', 'title in nepali', 'another slider caption', 'another slider caption in nepali', '5e4cd1fab3a663f64f67d8bc38f09351.jpg', 1, 2, '2017-01-18 11:11:10'),
(4, 'Slider 3', 'Slider 3 Nepali', 'Caption third English', 'caption third nepali', 'a384eb9be5c46b98e72b4f7c164fba04.jpg', 1, 3, '2017-01-21 21:03:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `api_users_token`
--
ALTER TABLE `api_users_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_customer`
--
ALTER TABLE `auth_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_groups`
--
ALTER TABLE `auth_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_login_attempts`
--
ALTER TABLE `auth_login_attempts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ip_address` (`ip_address`),
  ADD KEY `login` (`login`);

--
-- Indexes for table `auth_users`
--
ALTER TABLE `auth_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_users_groups`
--
ALTER TABLE `auth_users_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_groups_user_id` (`user_id`),
  ADD KEY `fk_users_groups_group_id` (`group_id`);

--
-- Indexes for table `core_access_control`
--
ALTER TABLE `core_access_control`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_access_control_ugroup_id` (`ugroup_id`),
  ADD KEY `fk_access_control_amodule_id` (`amodule_id`);

--
-- Indexes for table `core_access_modules`
--
ALTER TABLE `core_access_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jeevan_sessions`
--
ALTER TABLE `jeevan_sessions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `jeevan_sessions_id_ip` (`id`,`ip_address`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `tbl_events`
--
ALTER TABLE `tbl_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_events_book`
--
ALTER TABLE `tbl_events_book`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_event_booking_user_customer_id` (`user`),
  ADD KEY `fk_event_booking_event_event_id` (`event`);

--
-- Indexes for table `tbl_sliders`
--
ALTER TABLE `tbl_sliders`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `api_users_token`
--
ALTER TABLE `api_users_token`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `auth_customer`
--
ALTER TABLE `auth_customer`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `auth_groups`
--
ALTER TABLE `auth_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `auth_login_attempts`
--
ALTER TABLE `auth_login_attempts`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_users`
--
ALTER TABLE `auth_users`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `auth_users_groups`
--
ALTER TABLE `auth_users_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `core_access_control`
--
ALTER TABLE `core_access_control`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `core_access_modules`
--
ALTER TABLE `core_access_modules`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_events`
--
ALTER TABLE `tbl_events`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_events_book`
--
ALTER TABLE `tbl_events_book`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_sliders`
--
ALTER TABLE `tbl_sliders`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_users_groups`
--
ALTER TABLE `auth_users_groups`
  ADD CONSTRAINT `fk_users_groups_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_groups` (`id`),
  ADD CONSTRAINT `fk_users_groups_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_users` (`id`);

--
-- Constraints for table `core_access_control`
--
ALTER TABLE `core_access_control`
  ADD CONSTRAINT `fk_access_control_amodule_id` FOREIGN KEY (`amodule_id`) REFERENCES `core_access_modules` (`id`),
  ADD CONSTRAINT `fk_access_control_ugroup_id` FOREIGN KEY (`ugroup_id`) REFERENCES `auth_groups` (`id`);

--
-- Constraints for table `tbl_events_book`
--
ALTER TABLE `tbl_events_book`
  ADD CONSTRAINT `fk_event_booking_event_event_id` FOREIGN KEY (`event`) REFERENCES `tbl_events` (`id`),
  ADD CONSTRAINT `fk_event_booking_user_customer_id` FOREIGN KEY (`user`) REFERENCES `auth_customer` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
