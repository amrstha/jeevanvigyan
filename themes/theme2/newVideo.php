<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<meta name ="description" content="Jeevan Vigyan a guide to your  physical and spiritual prosperity">
	<meta name="author" content="#">
	<title>Jeevan Vigyan</title>
	<!--Bootstrap CSS -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.css">
	<link rel="stylesheet" href="css/style.css">

	<!--HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		   <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		   <script src="https://oss.maxcdn.com/respond/3.7.2/respond.min.js"></script>
		   <![endif]-->
		   <link rel="icon" href="images/meditation.png" type="image/x-icon"/>
		   <link rel="shortcut icon" href="images/meditation.png" type="image/x-icon"/>

		</head>


		<body>
			<header class="l-header">
				<div class="row">
					<div class="col-sm-12">
						<div class="c-top" style="background-color:#0094de; color:#ffffff;">
							<div class="container">
								<div class="row">
									<div class="c-top__wrapper clearfix">
										<div class="col-sm-5">
											<ul class="c-contact">
												<li class="c-contact__mail">
													<i class="fa fa-envelope-o" aria-hidden="true"></i>
													<a href="mailto:info@jeevanvigyan.com" style="color:#ffffff;">info@jeevanvigyan.com</a>
												</li>
												<li class="c-contact__phone">
													<i class="fa fa-phone" aria-hidden="true"></i>
													<a href="tel:97714472830" style="color:#ffffff;">977-1-4472830</a>
												</li>
											</ul>	
										</div>
										<div class="col-sm-4">
											<ul class="top__social">
												<li>
													<a href="https://www.facebook.com/jeevanvigyannp/?fref=ts" class="facebook"><img src="images/facebook.png" alt="Facebook"></a>
												</li>
												<li>
													<a href="https://twitter.com/jvyouth" class="twitter"><img src="images/twitter.png" alt="Twitter"></a>
												</li>
												<li>
													<a href="https://www.youtube.com/user/jeevanvigyan" class="youtube"><img src="images/youtube.png" alt="Youtube"></a>
												</li>
											</ul>
										</div>
										<div class="col-sm-3 text-right">
											<ul class="c-lang__language">
												<li><a href="#" class="btn btn-primary">Login</a></li>
												<li><a href="#"><img src="images/uk.png"></a></li>
												<li><a href="#"><img src="images/nepal.png"></a></li>
											</ul>
										</div>
									</div>
								</div>	
							</div>
						</div>
					</div>
					<!--Collapseable navbar starts here-->
					<div class="row" style="margin-top:80px; margin-bottom:0px; margin-left:10%;margin-right:10%;">
						<nav class="navbar navbar-default">
							<div class="container-fluid">
								<!-- Brand and toggle get grouped for better mobile display -->
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav--menu" aria-expanded="false">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
									<a class="navbar-brand" href="home.php" style="margin-left:-20%;"><img src="images/logo.png" alt="Jeevan Vigyan" style="height:80px; width:80%; margin-top:-40px;"></a>
								</div>

								<!-- Collect the nav links, forms, and other content for toggling -->
								<div class="collapse navbar-collapse" id="nav--menu">


									<ul class="nav navbar-nav navbar-right">
										<li class="active"><a href="#">Home </a></li>
										<li>
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About <span class="caret"></span></a>
											<ul class="dropdown-menu">
												<li><a href="#">About Kendra</a></li>
												<li><a href="#">Feedback</a></li>
												<li><a href="#">Contact</a></li>
											</ul>
										</li>
										<li>
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Program <span class="caret"></span></a>
											<ul class="dropdown-menu">
												<li><a href="#">Aishwarya Vigyan</a></li>
												<li><a href="#">Laya Vigyan</a></li>
												<li><a href="#">Dhyan Vigyan</a></li>
												<li><a href="#">Vyaktitwa Vigyan</a></li>
												<li><a href="#">Chakra Upachar</a></li>
											</ul>
										</li>
										<li>
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Activities <span class="caret"></span></a>
											<ul class="dropdown-menu">
												<li><a href="#">dropdown</a></li>
											</ul>
										</li>
										<li>
											<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Publications <span class="caret"></span></a>
											<ul class="dropdown-menu">
												<li><a href="#">Videos</a></li>
												<li><a href="#">Songs</a></li>
												<li><a href="#">Meditation Discourse</a></li>
												<li><a href="#">Online Radio</a></li>
											</ul>
										</li>
										<li><a href="#">Forum </a></li>
										<li><a href="#">Gallery </a></li>
										<li><a href="#">News </a></li>
									</ul>
								</li>
							</ul>
						</div><!-- /.navbar-collapse -->
					</div><!-- /.container-fluid -->
				</nav>
			</div>
			<!--Collpaseable navbar ends here-->
		</div>
	</header>
	<!--REmaining code starts from here-->
	<main class="l-main">
	<div class="row" style="background:#7f7f7f;">
		<div class="col-sm-12 col-md-12 col-lg-12" style="margin-top:10px; margin-bottom:10px;">

			<iframe id="ytplayer" type="text/html" style="margin-left:5%;" width="90%" height="450"
			src="https:   //www.youtube.com/embed/8TwBaYn6J4I?autoplay=1"
			frameborder="0" allowfullscreen autoplay></iframe>
		</div>


	</div>
	<section class="c-update" id="latest-update" style="background:#fff; color:#000; margin-bottom:2px;">
			<div class="container">
				<div class="">
					
					<div class="c-content">
					<div class="row" style="margin-top:10px;margin-bottom:10px;margin-left:40%;">
						<div class="btn-group btn-group-lg">
  <button type="button" class="btn btn-primary active" onclick="myFunction()">Life Sciece</button>
  <button type="button" class="btn btn-primary" onclick="">&nbsp;&nbsp;Geeta&nbsp;&nbsp;&nbsp;&nbsp;</button>
  
</div>
					</div>
						<div class="row">
						<div id="lfscience">
							
								<div class="c-update__schedule" style="margin-left:15%;margin-right:15%;">
									<div class="schedule__box">
										<div class="row">
										    <a href="#" style="color:#fff;">
											<div class="col-sm-9" style="">
												<div class="schedule__box--info">
													<h4>Success and Joy</h4>
													<p>Aishwarya (Prosperity, Abundance and Wealth) is the product of man's</p>
													<span style="font-size: 12px;margin-left:86%;">11 Nov 2015</span>
												</div>		
											</div>
											<div class="col-sm-3">
												<div class="schedule__box--date">
													<span><a href="#"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true" style="margin-top: 20px;"></i></a></span>
												</div>	
											</div>
											</a>
										</div>
									</div>
									<div class="schedule__box">
										<div class="row">
										    <a href="#" style="color:#fff;">
											<div class="col-sm-9" style="">
												<div class="schedule__box--info">
													<h4>Success and Joy</h4>
													<p>Aishwarya (Prosperity, Abundance and Wealth) is the product of man's</p>
													<span style="font-size: 12px;margin-left:86%;">11 Nov 2015</span>
												</div>		
											</div>
											<div class="col-sm-3">
												<div class="schedule__box--date">
													<span><a href="#"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true" style="margin-top: 20px;"></i></a></span>
												</div>	
											</div>
											</a>
										</div>
									</div>
									<div class="schedule__box">
										<div class="row">
										    <a href="#" style="color:#fff;">
											<div class="col-sm-9" style="">
												<div class="schedule__box--info">
													<h4>Success and Joy</h4>
													<p>Aishwarya (Prosperity, Abundance and Wealth) is the product of man's</p>
													<span style="font-size: 12px;margin-left:86%;">11 Nov 2015</span>
												</div>		
											</div>
											<div class="col-sm-3">
												<div class="schedule__box--date">
													<span><a href="#"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true" style="margin-top: 20px;"></i></a></span>
												</div>	
											</div>
											</a>
										</div>
									</div>
									<div class="schedule__box">
										<div class="row">
										    <a href="#" style="color:#fff;">
											<div class="col-sm-9" style="">
												<div class="schedule__box--info">
													<h4>Success and Joy</h4>
													<p>Aishwarya (Prosperity, Abundance and Wealth) is the product of man's</p>
													<span style="font-size: 12px;margin-left:86%;">11 Nov 2015</span>
												</div>		
											</div>
											<div class="col-sm-3">
												<div class="schedule__box--date">
													<span><a href="#"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true" style="margin-top: 20px;"></i></a></span>
												</div>	
											</div>
											</a>
										</div>
									</div>
									<div class="schedule__box">
										<div class="row">
										    <a href="#" style="color:#fff;">
											<div class="col-sm-9" style="">
												<div class="schedule__box--info">
													<h4>Success and Joy</h4>
													<p>Aishwarya (Prosperity, Abundance and Wealth) is the product of man's</p>
													<span style="font-size: 12px;margin-left:86%;">11 Nov 2015</span>
												</div>		
											</div>
											<div class="col-sm-3">
												<div class="schedule__box--date">
													<span><a href="#"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true" style="margin-top: 20px;"></i></a></span>
												</div>	
											</div>
											</a>
										</div>
									</div>
									
                                      <div class="accordion-group">
    <div id="collapseTwo" class="accordion-body collapse">
      <div class="accordion-inner">
        <div class="schedule__box">
										<div class="row">
										    <a href="#" style="color:#fff;">
											<div class="col-sm-9" style="">
												<div class="schedule__box--info">
													<h4>Success and Joy</h4>
													<p>Aishwarya (Prosperity, Abundance and Wealth) is the product of man's</p>
													<span style="font-size: 12px;margin-left:86%;">11 Nov 2015</span>
												</div>		
											</div>
											<div class="col-sm-3">
												<div class="schedule__box--date">
													<span><a href="#"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true" style="margin-top: 20px;"></i></a></span>
												</div>	
											</div>
											</a>
										</div>
									</div>
								<div class="schedule__box">
										<div class="row">
										    <a href="#" style="color:#fff;">
											<div class="col-sm-9" style="">
												<div class="schedule__box--info">
													<h4>Success and Joy</h4>
													<p>Aishwarya (Prosperity, Abundance and Wealth) is the product of man's</p>
													<span style="font-size: 12px;margin-left:86%;">11 Nov 2015</span>
												</div>		
											</div>
											<div class="col-sm-3">
												<div class="schedule__box--date">
													<span><a href="#"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true" style="margin-top: 20px;"></i></a></span>
												</div>	
											</div>
											</a>
										</div>
									</div>
								<div class="schedule__box">
										<div class="row">
										    <a href="#" style="color:#fff;">
											<div class="col-sm-9" style="">
												<div class="schedule__box--info">
													<h4>Success and Joy</h4>
													<p>Aishwarya (Prosperity, Abundance and Wealth) is the product of man's</p>
													<span style="font-size: 12px;margin-left:86%;">11 Nov 2015</span>
												</div>		
											</div>
											<div class="col-sm-3">
												<div class="schedule__box--date">
													<span><a href="#"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true" style="margin-top: 20px;"></i></a></span>
												</div>	
											</div>
											</a>
										</div>
									</div>
								<div class="schedule__box">
										<div class="row">
										    <a href="#" style="color:#fff;">
											<div class="col-sm-9" style="">
												<div class="schedule__box--info">
													<h4>Success and Joy</h4>
													<p>Aishwarya (Prosperity, Abundance and Wealth) is the product of man's</p>
													<span style="font-size: 12px;margin-left:86%;">11 Nov 2015</span>
												</div>		
											</div>
											<div class="col-sm-3">
												<div class="schedule__box--date">
													<span><a href="#"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true" style="margin-top: 20px;"></i></a></span>
												</div>	
											</div>
											</a>
										</div>
									</div>
								<div class="schedule__box">
										<div class="row">
										    <a href="#" style="color:#fff;">
											<div class="col-sm-9" style="">
												<div class="schedule__box--info">
													<h4>Success and Joy</h4>
													<p>Aishwarya (Prosperity, Abundance and Wealth) is the product of man's</p>
													<span style="font-size: 12px;margin-left:86%;">11 Nov 2015</span>
												</div>		
											</div>
											<div class="col-sm-3">
												<div class="schedule__box--date">
													<span><a href="#"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true" style="margin-top: 20px;"></i></a></span>
												</div>	
											</div>
											</a>
										</div>
									</div>
								
        </div>
    </div>
    <button class="SeeMore btn btn-primary" data-toggle="collapse" href="#collapseTwo" style="margin-left: 40%;">See All</button>
    </div>

								</div>
							</div>
							</div>
							<div class="accordion" id="accordion2">
  
 
  

</div>
 
<div id="gitaval" style="display:none;">
							
								<div class="c-update__schedule" style="margin-left:15%;margin-right:15%;">
									<div class="schedule__box">
										<div class="row">
										    <a href="#" style="color:#fff;">
											<div class="col-sm-9" style="">
												<div class="schedule__box--info">
													<h4>Gitaval</h4>
													<p>Aishwarya (Prosperity, Abundance and Wealth) is the product of man's</p>
													<span style="font-size: 12px;margin-left:86%;">11 Nov 2015</span>
												</div>		
											</div>
											<div class="col-sm-3">
												<div class="schedule__box--date">
													<span><a href="#"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true" style="margin-top: 20px;"></i></a></span>
												</div>	
											</div>
											</a>
										</div>
									</div>
									<div class="schedule__box">
										<div class="row">
										    <a href="#" style="color:#fff;">
											<div class="col-sm-9" style="">
												<div class="schedule__box--info">
													<h4>Success and Joy</h4>
													<p>Aishwarya (Prosperity, Abundance and Wealth) is the product of man's</p>
													<span style="font-size: 12px;margin-left:86%;">11 Nov 2015</span>
												</div>		
											</div>
											<div class="col-sm-3">
												<div class="schedule__box--date">
													<span><a href="#"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true" style="margin-top: 20px;"></i></a></span>
												</div>	
											</div>
											</a>
										</div>
									</div>
									<div class="schedule__box">
										<div class="row">
										    <a href="#" style="color:#fff;">
											<div class="col-sm-9" style="">
												<div class="schedule__box--info">
													<h4>Success and Joy</h4>
													<p>Aishwarya (Prosperity, Abundance and Wealth) is the product of man's</p>
													<span style="font-size: 12px;margin-left:86%;">11 Nov 2015</span>
												</div>		
											</div>
											<div class="col-sm-3">
												<div class="schedule__box--date">
													<span><a href="#"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true" style="margin-top: 20px;"></i></a></span>
												</div>	
											</div>
											</a>
										</div>
									</div>
									<div class="schedule__box">
										<div class="row">
										    <a href="#" style="color:#fff;">
											<div class="col-sm-9" style="">
												<div class="schedule__box--info">
													<h4>Success and Joy</h4>
													<p>Aishwarya (Prosperity, Abundance and Wealth) is the product of man's</p>
													<span style="font-size: 12px;margin-left:86%;">11 Nov 2015</span>
												</div>		
											</div>
											<div class="col-sm-3">
												<div class="schedule__box--date">
													<span><a href="#"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true" style="margin-top: 20px;"></i></a></span>
												</div>	
											</div>
											</a>
										</div>
									</div>
									<div class="schedule__box">
										<div class="row">
										    <a href="#" style="color:#fff;">
											<div class="col-sm-9" style="">
												<div class="schedule__box--info">
													<h4>Success and Joy</h4>
													<p>Aishwarya (Prosperity, Abundance and Wealth) is the product of man's</p>
													<span style="font-size: 12px;margin-left:86%;">11 Nov 2015</span>
												</div>		
											</div>
											<div class="col-sm-3">
												<div class="schedule__box--date">
													<span><a href="#"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true" style="margin-top: 20px;"></i></a></span>
												</div>	
											</div>
											</a>
										</div>
									</div>
									
                                      <div class="accordion-group">
    <div id="collapseTwo" class="accordion-body collapse">
      <div class="accordion-inner">
        <div class="schedule__box">
										<div class="row">
										    <a href="#" style="color:#fff;">
											<div class="col-sm-9" style="">
												<div class="schedule__box--info">
													<h4>Success and Joy</h4>
													<p>Aishwarya (Prosperity, Abundance and Wealth) is the product of man's</p>
													<span style="font-size: 12px;margin-left:86%;">11 Nov 2015</span>
												</div>		
											</div>
											<div class="col-sm-3">
												<div class="schedule__box--date">
													<span><a href="#"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true" style="margin-top: 20px;"></i></a></span>
												</div>	
											</div>
											</a>
										</div>
									</div>
								<div class="schedule__box">
										<div class="row">
										    <a href="#" style="color:#fff;">
											<div class="col-sm-9" style="">
												<div class="schedule__box--info">
													<h4>Success and Joy</h4>
													<p>Aishwarya (Prosperity, Abundance and Wealth) is the product of man's</p>
													<span style="font-size: 12px;margin-left:86%;">11 Nov 2015</span>
												</div>		
											</div>
											<div class="col-sm-3">
												<div class="schedule__box--date">
													<span><a href="#"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true" style="margin-top: 20px;"></i></a></span>
												</div>	
											</div>
											</a>
										</div>
									</div>
								<div class="schedule__box">
										<div class="row">
										    <a href="#" style="color:#fff;">
											<div class="col-sm-9" style="">
												<div class="schedule__box--info">
													<h4>Success and Joy</h4>
													<p>Aishwarya (Prosperity, Abundance and Wealth) is the product of man's</p>
													<span style="font-size: 12px;margin-left:86%;">11 Nov 2015</span>
												</div>		
											</div>
											<div class="col-sm-3">
												<div class="schedule__box--date">
													<span><a href="#"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true" style="margin-top: 20px;"></i></a></span>
												</div>	
											</div>
											</a>
										</div>
									</div>
								<div class="schedule__box">
										<div class="row">
										    <a href="#" style="color:#fff;">
											<div class="col-sm-9" style="">
												<div class="schedule__box--info">
													<h4>Success and Joy</h4>
													<p>Aishwarya (Prosperity, Abundance and Wealth) is the product of man's</p>
													<span style="font-size: 12px;margin-left:86%;">11 Nov 2015</span>
												</div>		
											</div>
											<div class="col-sm-3">
												<div class="schedule__box--date">
													<span><a href="#"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true" style="margin-top: 20px;"></i></a></span>
												</div>	
											</div>
											</a>
										</div>
									</div>
								<div class="schedule__box">
										<div class="row">
										    <a href="#" style="color:#fff;">
											<div class="col-sm-9" style="">
												<div class="schedule__box--info">
													<h4>Success and Joy</h4>
													<p>Aishwarya (Prosperity, Abundance and Wealth) is the product of man's</p>
													<span style="font-size: 12px;margin-left:86%;">11 Nov 2015</span>
												</div>		
											</div>
											<div class="col-sm-3">
												<div class="schedule__box--date">
													<span><a href="#"><i class="fa fa-chevron-circle-right fa-4x" aria-hidden="true" style="margin-top: 20px;"></i></a></span>
												</div>	
											</div>
											</a>
										</div>
									</div>
								
        </div>
    </div>
    <button class="SeeMore btn btn-primary" data-toggle="collapse" href="#collapseTwo" style="margin-left: 40%;">See All</button>
    </div>

								</div>
							</div>
							</div>
							
					</div>
				</div>
			</div>
		</section>
	</main>	

	<!--Remaining code goes here-->
	<footer class="l-footer">
		<div class="c-footer__top text-center">
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<img src="images/location.png">
						<ul>
							<li>Ganesh Marga (Near Ganesh Temple)</li>
							<li>Chabahil, Kathmandu</li>
						</ul>
					</div>
					<div class="col-sm-4">
						<img src="images/phone.png">
						<ul>
							<li><a href="tel:97714472830">977-1-4472830</a></li>
						</ul>
					</div>
					<div class="col-sm-4">
						<img src="images/mail.png">
						<ul>
							<li><a href="mailto:info@jeevanvigyan.org">info@jeevanvigyan.org</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="social">
			<div class="container">
				<div class="row">
					<ul class="footer__social">
						<li>
							<a href="https://www.facebook.com/jeevanvigyannp/?fref=ts" class="facebook"><img src="images/facebook.png" alt="Facebook"></a>
						</li>
						<li>
							<a href="https://twitter.com/jvyouth" class="twitter"><img src="images/twitter.png" alt="Twitter"></a>
						</li>
						<li>
							<a href="https://www.youtube.com/user/jeevanvigyan" class="youtube"><img src="images/youtube.png" alt="Youtube"></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="c-footer__bottom text-center">
			<div class="container">
				<div class="row">
						<!-- <div class="logo">
							<img src="images/logo-footer.png" alt="Jeevan Vigyan">
						</div> -->
						<ul class="footer__nav">
							<li><a href="#">Home</a></li>
							<li><a href="#">About</a></li>
							<li><a href="#">Training</a></li>
							<li><a href="#">Radio Program</a></li>
							<li><a href="#">Videos</a></li>
							<li><a href="#">News</a></li>
							<li><a href="#">Songs</a></li>
							<li><a href="#">Contact</a></li>
						</ul>
						<p class="copyright">&copy; Copyrights 2016 Jeevan Vigyan Kendra. All rights reserved</p>
					</div>
				</div>
			</div>
		</footer>
		<div class="l-question">
			<a href="#">Question Answer <i class="fa fa-chevron-circle-down" aria-hidden="true"></i></a>
		</div>
		<div class="top pull-right">
			<a href="#"><img src="images/top.png" alt="to top"></a>
		</div>
<script>
function myFunction() {
    function toggler(divId) {
    $("#" + gitaval).toggle();
}
}
</script>


		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.js"></script>
		<script src="js/app.js"></script>

	</body>
	</html>
