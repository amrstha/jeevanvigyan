<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/18/17
 * Time: 3:35 PM
 */
?>

<div class="row">
    <div class="col-sm-12">

        <?php
        $attributes = [
            'class' => 'form-horizontal',
            'role'  => 'form',
            'name'  => 'popup_form',
            'id'    => 'popup_form'
        ];
        $action = admin_url('slider/'.(isset($edit) ? 'edit/'.url_encrypt($edit->id) : 'add'));
        echo form_open($action, $attributes);
        ?>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="title">Title: </label>
            <div class="col-sm-10">
                <input type="text" name="title" id="title" class="form-control" placeholder="Title" value="<?=(isset($edit) ? $edit->title : '')?>" />
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="title_np">Title (in Nepali): </label>
            <div class="col-sm-10">
                <input type="text" name="title_np" id="title_np" class="form-control" placeholder="Title (in Nepali)" value="<?=(isset($edit) ? $edit->title_np : '')?>" />
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="caption">Caption:</label>
            <div class="col-sm-10">
                <textarea class="form-control" name="caption" id="caption" placeholder="Caption"><?=(isset($edit) ? $edit->caption : '')?></textarea>
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="caption_np">Caption (in Nepali):</label>
            <div class="col-sm-10">
                <textarea class="form-control" name="caption_np" id="caption_np" placeholder="Caption (in Nepali)"><?=(isset($edit) ? $edit->caption_np : '')?></textarea>
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="order_by">Order: <span class="text-red">*</span></label>
            <div class="col-sm-10">
                <input type="text" name="order_by" id="order_by" class="form-control required" placeholder="Order" value="<?=(isset($edit) ? $edit->order_by : '0')?>" />
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-12 pull-left" for="image">Image (JPG,PNG format only) - Maximum: 2MB [780px * 430px]: <span class="text-red">*</span></label>
            <?php if(isset($edit)): ?>
                <div class="field-group col-sm-4">
                    <img src="<?php echo base_url(Utility::getUploadDir('slider').$edit->image) ?>" alt="$edit->title" class="img-responsive" />
                    <input type="hidden" name="prevImageName" id="prevImageName" value="<?php echo $edit->image ?>">
                    <br/><br/>
                </div>
            <?php endif ?>
            <div class="col-sm-<?php echo isset($edit) ? '8' : '10 col-sm-offset-2' ?>">
                <input type="file" class="form-control <?php echo isset($edit) ? '' : 'required' ?>" name="image" id="image">
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>

        <?=form_close()?>

    </div>
</div>
