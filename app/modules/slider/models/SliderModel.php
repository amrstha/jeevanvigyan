<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/18/17
 * Time: 3:19 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class SliderModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add($data)
    {
        $this->db->insert(TBL_SLIDER, $data);
        return $this->db->affected_rows() > 0;
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id)->update(TBL_SLIDER, $data);
        return $this->db->affected_rows() > 0;
    }

    public function status($id)
    {
        $this->db->query("UPDATE {$this->db->dbprefix(TBL_SLIDER)} SET status = 1 - status WHERE id = {$id}");
        return $this->db->affected_rows() > 0;
    }

    public function delete($id)
    {
        $prevData = $this->getById($id);
        $this->deleteImage($prevData->image);
        $this->db->where('id', $id)->delete(TBL_SLIDER);
        return $this->db->affected_rows() > 0;
    }

    public function deleteImage($imageFile)
    {
        $imageFile = Utility::getUploadDir('slider').$imageFile;
        if (file_exists($imageFile)) {
            @unlink($imageFile);
            return true;
        }
        return false;
    }

    public function getAll($activeOnly = false, $limit = null, $offset = 0)
    {
        if ($activeOnly) {
            $this->db->where('status', 1);
        }

        if (!is_null($limit)) {
            $this->db->limit($limit, $offset);
        }

        return $this->db->order_by('order_by, status, created_at')->get(TBL_SLIDER)->result();
    }

    public function getById($id)
    {
        $data = $this->db->where('id', $id)->get(TBL_SLIDER);
        return $data->num_rows() > 0 ? $data->row() : false;
    }
}