<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 3/14/17
 * Time: 12:07 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class QnaModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add($data)
    {
        $this->db->insert(TBL_QNA, $data);
        return $this->db->affected_rows() > 0 ? true : false;
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id)->update(TBL_QNA, $data);
        return $this->db->affected_rows() > 0;
    }

    public function getAll($activeOnly = false, $limit = null, $offset = 0)
    {
        if ($activeOnly) {
            $this->db->where('status', 1);
        }

        if (!is_null($limit)) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by('created_at', 'DESC');
        return $this->db->get(TBL_QNA)->result();
    }

    public function getById($id)
    {
        $data = $this->db->where('id', $id)->get(TBL_QNA);
        return $data->num_rows() > 0 ? $data->row() : false;
    }

    public function status($id)
    {
        $this->db->query("UPDATE {$this->db->dbprefix(TBL_QNA)} SET status = 1 - status WHERE id = {$id}");
        return $this->db->affected_rows() > 0;
    }
}