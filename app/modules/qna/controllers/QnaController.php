<?php

/**
 * Created by PhpStorm.
 * User: prabesh
 * Date: 1/18/17
 * Time: 3:17 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class QnaController extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('qna_module')
        ];
        $this->breadcrumb->append('Qna', admin_url('qna'));

        // load qna model
        $this->load->model('qnaModel', 'qna');
    }

    public function index(){
        $this->lists();
    }

    public function lists()
    {
        $this->breadcrumb->append('List', admin_url('qna'));
        self::$viewData['pageDetail'] = (object)[
            'title'     => 'Questions & Answers: List',
            'subTitle'  => 'List of all Questions and Answers.'
        ];

        self::$viewData['qnas'] = $this->qna->getAll();

        $this->load->admintheme('lists', self::$viewData);
    }

    public function edit()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $id = Utility::getIdFromUri(4);
        if ($id === false) {
            $this->output->json([
                'status'    => 'error',
                'data'      => NULL,
                'message'   => $this->lang->line('invalid_request')
            ]);
        }

        $response = [];
        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('question', 'Question', 'required');
            $this->form_validation->set_rules('answer', 'Answer', 'required');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Save Information in database
                $updateData = [
                    'question'          => $this->input->post('question'),
                    'answer'            => $this->input->post('answer')
                ]; 

                $result = $this->qna->update($id, $updateData);
                if(!$result) {
                    $response = [
                        'status'    => 'error',
                        'data'      => NULL,
                        'message'   => $this->lang->line('nothing_updated')
                    ];
                } else {
                    $this->session->set_notification('success', $this->lang->line('success_updated'));
                    $response = [
                        'status'    => 'success',
                        'data'      => NULL,
                        'message'   => 'success'
                    ];
                }
            }

        } else {
            // load form
            self::$viewData['edit'] = $this->qna->getById($id);
            $form = $this->load->view('qna/form', self::$viewData, true);
            $response = [
                'status'    => 'success',
                'data'      => null,
                'message'   => $form
            ];
        }

        $this->output->json($response);
    }

      public function status()
    {
        $id = Utility::getIdFromUri(4);

        if ($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if( ! $this->qna->status($id) ) {
                $this->session->set_notification('error', $this->lang->line('nothing_updated'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_updated'));
            }
        }
        admin_redirect('qna', 'refresh');
    }
}