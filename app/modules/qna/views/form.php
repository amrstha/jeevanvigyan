<?php
/**
 * Created by PhpStorm.
 * User: prabesh
 * Date: 1/17/17
 * Time: 1:10 AM
 */
?>
<div class="row">
    <div class="col-sm-12">

        <?php
        $attributes = [
            'class' => 'form-horizontal',
            'role'  => 'form',
            'name'  => 'popup_form',
            'id'    => 'popup_form'
        ];
        $action = admin_url('qna/'.'edit/'.url_encrypt($edit->id));
        echo form_open($action, $attributes);
        ?>

        <div class="form-group has-feedback">
            <label class="col-sm-3 control-label" for="name">Question: <span class="text-red">*</span></label>
            <div class="col-sm-9">
                <input type="text" name="question" id="question" class="form-control required" placeholder="Question" value="<?=(isset($edit) ? $edit->question : '')?>" />
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-3 control-label" for="description">Answer:<span class="text-red">*</span></label>
            <div class="col-sm-9">
                <textarea class="form-control" name="answer" id="answer" placeholder="Write your answer"><?=(isset($edit) ? $edit->answer : '')?></textarea>
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>

        <?=form_close()?>

    </div>
</div>
