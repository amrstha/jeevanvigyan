.<?php
/**
 * Created by PhpStorm.
 * User: manjul
 * Date: 01/06/17
 * Time: 3:15 PM
 */
?>

<div class="row">
    <div class="col-sm-12">

        <?php
        $attributes = [
            'class' => 'form-horizontal',
            'role'  => 'form',
            'name'  => 'popup_form',
            'id'    => 'popup_form'
        ];
        $action = admin_url('quotations/'.(isset($edit) ? 'edit/'.url_encrypt($edit->id) : 'add'));
        echo form_open($action, $attributes);
        ?>
        
        <div class="form-group">
                <label class="col-sm-2 control-label" for="language">Languaage: <span class="text-red">*</span></label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-6">
                            <select class="form-control chosen-select" name="language" id="language" data-placeholder="SELECT Language" required>
                                <option value=""></option>
                                    <option value="np" <?=(isset($edit) && $edit->language == 'np' ? 'selected' : '')?>>Nepali</option>
                                    <option value="en" <?=(isset($edit) && $edit->language == 'en' ? 'selected' : '')?>>English</option>
                            </select>
                        </div>
                    </div>
                </div>
        </div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="name">Quotation: <span class="text-red">*</span></label>
            <div class="col-sm-10">
                <input type="text" name="quotation" id="quotation" class="form-control required" placeholder="Quotation" value="<?=(isset($edit) ? $edit->quotation : '')?>" />
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <?=form_close()?>

    </div>
</div>
