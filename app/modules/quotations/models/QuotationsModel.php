<?php

/**
 * Created by PhpStorm.
 * User: manjul
 * Date: 01/06/17
 * Time: 3:15 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class QuotationsModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function add($data)
    {
        $this->db->insert(TBL_QUOTATIONS, $data);
        return $this->db->affected_rows() > 0;
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id)->update(TBL_QUOTATIONS, $data);
        return $this->db->affected_rows() > 0;
    }

    public function changestatus($id)
    {
        $this->db->query("UPDATE {$this->db->dbprefix(TBL_QUOTATIONS)} SET status = 1 - status WHERE id = {$id}");
        return $this->db->affected_rows() > 0;
    }

    public function delete($id)
    {
        $this->db->where('id', $id)->delete(TBL_QUOTATIONS);
        return $this->db->affected_rows() > 0;
    }

    public function undodelete($id)
    {
        $this->db->where('id', $id)->update(TBL_QUOTATIONS, ['status' => 1]);
        return $this->db->affected_rows() > 0;
    }

    public function getAll($activeOnly = false, $limit = null, $offset = 0)
    {
        if ( $activeOnly ) {
            $this->db->where('status', 1);
        }

        if (!is_null($limit)) {
            $this->db->limit($limit, $offset);
        }

        return $this->db->order_by('created_at')->get(TBL_QUOTATIONS)->result();
    }

    public function getById($id)
    {
        $data = $this->db->where('id', $id)->get(TBL_QUOTATIONS);
        return $data->num_rows() > 0 ? $data->row() : false;
    }
    
}