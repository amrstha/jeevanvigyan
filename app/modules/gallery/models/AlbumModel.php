<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/30/17
 * Time: 4:03 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class AlbumModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add($data)
    {
        $data['slug'] = Utility::generateSlug($data['name'], TBL_GALLERY_ALBUM, 'slug');
        $this->db->insert(TBL_GALLERY_ALBUM, $data);
        return $this->db->affected_rows() > 0;
    }

    public function update($id, $data)
    {
        $date['updated_at'] = date('Y-m-d H:i:s');
        $this->db->where('id', $id)->update(TBL_GALLERY_ALBUM, $data);
        return $this->db->affected_rows() > 0;
    }

    public function status($id)
    {
        $this->db->query("UPDATE {$this->db->dbprefix(TBL_GALLERY_ALBUM)} SET status = 1 - status WHERE id = {$id}");
        return $this->db->affected_rows() > 0;
    }

    public function delete($id)
    {
        $prevData = $this->getById($id);

        $this->db->where('id', $id)->delete(TBL_GALLERY_ALBUM);
        if ($this->db->affected_rows() > 0) {
            $this->deleteImage($prevData->image);
            return true;
        }
        return false;
    }

    public function deleteImage($imageFile)
    {
        $imageFile = Utility::getUploadDir('gallery_album').$imageFile;
        if (file_exists($imageFile)) {
            @unlink($imageFile);
            return true;
        }
        return false;
    }

    public function undodelete($id)
    {
        $this->db->where('id', $id)->update(TBL_GALLERY_ALBUM, ['status' => 1, 'updated_at' => date("Y-m-d H:i:s")]);
        return $this->db->affected_rows() > 0;
    }

    public function getAll($activeOnly = false, $limit = null, $offset = 0,  $type = null)
    {
        if ($activeOnly) {
            $this->db->where('status', 1);
        }

        if (!is_null($limit)) {
            $this->db->limit($limit, $offset);
        }

        if(!is_null($type)){
            $this->db->where('type', $type);
        }

        return $this->db->order_by('name, created_at')->get(TBL_GALLERY_ALBUM)->result();
    }

    public function getById($id)
    {
        $data = $this->db->where('id', $id)->get(TBL_GALLERY_ALBUM);
        return $data->num_rows() > 0 ? $data->row() : false;
    }

    public function getBySlug($slug)
    {
        $data = $this->db->where('slug', $slug)->get(TBL_GALLERY_ALBUM);
        return $data->num_rows() > 0 ? $data->row() : false;
    }


}