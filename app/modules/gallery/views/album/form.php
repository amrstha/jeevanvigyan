<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 2/1/17
 * Time: 4:11 PM
 */
?>

<div class="row">
    <div class="col-sm-12">

        <?php
        $attributes = [
            'class' => 'form-horizontal',
            'role'  => 'form',
            'name'  => 'popup_form',
            'id'    => 'popup_form'
        ];
        $action = admin_url('gallery/album/'.(isset($edit) ? 'edit/'.url_encrypt($edit->id) : 'add'));
        echo form_open($action, $attributes);
        ?>

        <?php $typeArr = [
                '1' => 'Image',
                '2' => 'Video',
                '3' => 'Audio'
        ] ?>
        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="type">Type: <span class="text-red">*</span></label>
            <div class="col-sm-10">
                <?php if(isset($edit)): ?>
                    <input type="hidden" name="type" id="type" value="<?=$edit->type?>">
                    <input type="text" class="form-control" value="<?=$typeArr[$edit->type]?>" disabled>
                <?php else: ?>
                    <select class="form-control chosen-select" name="type" id="type" data-placeholder="SELECT">
                        <option value=""></option>
                        <option value="1">Image</option>
                        <option value="2">Video</option>
                        <option value="3">Audio</option>
                    </select>
                <?php endif ?>
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>


        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="name">Name: <span class="text-red">*</span></label>
            <div class="col-sm-10">
                <input type="text" name="name" id="name" class="form-control required" placeholder="Name" value="<?=(isset($edit) ? $edit->name : '')?>" />
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="name_np">Name (in Nepali): <span class="text-red">*</span></label>
            <div class="col-sm-10">
                <input type="text" name="name_np" id="name_np" class="form-control required" placeholder="Name (in Nepali)" value="<?=(isset($edit) ? $edit->name_np : '')?>" />
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="description">Description:</label>
            <div class="col-sm-10">
                <textarea class="form-control" name="description" id="description" placeholder="Description"><?=(isset($edit) ? $edit->description : '')?></textarea>
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="description_np">Description (in Nepali):</label>
            <div class="col-sm-10">
                <textarea class="form-control" name="description_np" id="description_np" placeholder="Description (in Nepali)"><?=(isset($edit) ? $edit->description_np : '')?></textarea>
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-12 pull-left" for="image">Album Image (JPG,PNG format only) - Maximum: 2MB [780px * 405px]:</label>
            <?php if(isset($edit)): ?>
                <div class="field-group col-sm-4">
                    <?php if (!empty($edit->image)): ?>
                        <img src="<?php echo base_url(Utility::getUploadDir('gallery_album').$edit->image) ?>" alt="<?=$edit->name?>" class="img-responsive" />
                    <?php endif ?>
                    <input type="hidden" name="prevImageName" id="prevImageName" value="<?php echo $edit->image ?>">
                    <br/><br/>
                </div>
            <?php endif ?>
            <div class="col-sm-<?php echo isset($edit) ? '8' : '10 col-sm-offset-2' ?>">
                <input type="file" class="form-control" name="image" accept="image/*" id="image">
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>

        <?=form_close()?>

    </div>
</div>

<script>
    $(".chosen-select").chosen();
    $(document).find('.chosen-container').each(function(){
        $(this).find('.chosen-choices, .chosen-single').parent().css('width' , '100%');
    });
</script>