<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/30/17
 * Time: 3:15 PM
 */
?>

<div class="album">
    <?php $cnt = 0;
    foreach ($albums as $album): if ($album->type !== '1') continue; ?>

        <div class="col-xs-6 col-sm-4 col-md-3">
            <div class="thumbnail">
                <div class="caption">
                    <h4><?= word_limiter($album->name,7) ?></h4>
                    <p>
                        <a href="<?= admin_url('gallery/album/edit/' . url_encrypt($album->id)) ?>"
                           class="label label-success loadForm" data-successbtn="Update"
                           data-addclass="bootbox-modal-sm" title="Edit Album">edit</a>

                        <a href="<?= admin_url('gallery/album/delete/' . url_encrypt($album->id)) ?>"
                           class="label label-danger confirmDiag" data-confirmbtn="Yes, delete this album!"
                           data-msg="Are you sure? Once confirmed it cannot be undone." title="delete">delete</a>

                        <a href="<?= admin_url('gallery/images/lists/' . url_encrypt($album->id)) ?>"
                           class="label label-info">View</a>
                    </p>
                </div>
                <?php if (!empty($album->image)): ?>
                    <img
                            src="<?= base_url(Utility::getUploadDir('gallery_album') . $album->image) ?>"
                            alt="<?= $album->name ?>"
                    >
                <?php endif ?>
            </div>
        </div>

        <?php $cnt++; endforeach;
    if ($cnt === 0): ?>
        <div class="alert alert-info" role="alert">No image albums created.
            <a href="<?= admin_url('gallery/album/add') ?>" class="btn btn-success btn-xs tooltip-success loadForm"
               data-toggle="tooltip" data-successbtn="Add" data-addclass="bootbox-modal-sm" title="Add New Album">
                Add New
            </a>
        </div>
    <?php endif ?>
</div>
