<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 2/1/17
 * Time: 3:55 PM
 */
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="pull-right">
            <a href="<?=admin_url('gallery/album/add')?>" class="btn btn-success btn-xs tooltip-success loadForm" data-toggle="tooltip" data-successbtn="Add" data-addclass="bootbox-modal-sm" title="Add New Album">
                <i class="fa fa-plus-circle"></i>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#images" id="images-tab-btn" role="tab" data-toggle="tab" aria-expanded="true">
                        Images
                    </a>
                </li>
                <li role="presentation" class="">
                    <a href="#video" id="images-tab-btn" role="tab" data-toggle="tab" aria-expanded="true">
                        Video
                    </a>
                </li>
                <li role="presentation" class="">
                    <a href="#audio" id="images-tab-btn" role="tab" data-toggle="tab" aria-expanded="true">
                        Audio
                    </a>
                </li>
            </ul>

            <div class="tab-content">

                <div role="tabpanel" class="tab-pane fade active in" id="images" aria-labelledby="images-tab-btn">

                    <?php $this->load->view('album/images') ?>

                </div>

                <div role="tabpanel" class="tab-pane fade" id="video" aria-labelledby="video-tab-btn">

                    <?php $this->load->view('album/video') ?>

                </div>

                <div role="tabpanel" class="tab-pane fade" id="audio" aria-labelledby="audio-tab-btn">

                    <?php $this->load->view('album/audio') ?>

                </div>

            </div>
        </div>

    </div>
</div>