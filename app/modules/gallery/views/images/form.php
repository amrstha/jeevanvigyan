<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/30/17
 * Time: 3:32 PM
 */
?>

<div class="row">
    <div class="col-sm-12">

        <?php
        $attributes = [
            'class' => 'form-horizontal',
            'role'  => 'form',
            'name'  => 'popup_form',
            'id'    => 'popup_form'
        ];
        $action = admin_url('gallery/images/'.(isset($edit) ? 'edit/'.url_encrypt($album->id).'/'.url_encrypt($edit->id) : 'add/'.url_encrypt($album->id)));
        echo form_open($action, $attributes);
        ?>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="title">Title: <span class="text-red">*</span></label>
            <div class="col-sm-10">
                <input type="text" name="title" id="title" class="form-control required" placeholder="Title" value="<?=(isset($edit) ? $edit->title : '')?>" />
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="title_np">Title (in Nepali): <span class="text-red">*</span></label>
            <div class="col-sm-10">
                <input type="text" name="title_np" id="title_np" class="form-control required" placeholder="Title (in Nepali)" value="<?=(isset($edit) ? $edit->title_np : '')?>" />
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="caption">Caption:</label>
            <div class="col-sm-10">
                <textarea class="form-control" name="caption" id="caption" placeholder="Caption"><?=(isset($edit) ? $edit->caption : '')?></textarea>
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="caption_np">Caption (in Nepali):</label>
            <div class="col-sm-10">
                <textarea class="form-control" name="caption_np" id="caption_np" placeholder="Caption (in Nepali)"><?=(isset($edit) ? $edit->caption_np : '')?></textarea>
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <?php if (!isset($edit)): ?>
        <div class="form-group has-feedback">
            <label class="col-sm-12 pull-left" for="image">Image (JPG,PNG format only) - Maximum: 2MB: <span class="text-red">*</span></label>
            <div class="col-sm-12">
                <input type="file" class="form-control required" accept="image/*" name="file" id="file">
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <?php endif ?>

        <?=form_close()?>

    </div>
</div>
