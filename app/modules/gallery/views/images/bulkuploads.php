<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 2/26/17
 * Time: 5:48 PM
 */
?>

<section>
    <div id="dropzone">

        <form class="dropzone needsclick" id="upload" method="post" action="<?=admin_url('gallery/images/bulkuploads/'.url_encrypt($album->id))?>">
            <div class="dz-message needsclick">
                Drop files here or click to upload.
            </div>
        </form>

    </div>
</section>

<script type="application/javascript">
    $(".dropzone").dropzone({
        acceptedFiles: '.jpg,.jpeg,.png'
    });
</script>