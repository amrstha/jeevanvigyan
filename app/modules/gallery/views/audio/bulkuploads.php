<?php
/**
 * Created by PhpStorm.
 * User: yipl
 * Date: 2/26/17
 * Time: 5:48 PM
 */
?>

<section>
    <div id="dropzone">

        <form class="dropzone needsclick" id="upload" action="<?=admin_url('gallery/audio/bulkuploads/'.url_encrypt($albumId))?>">
            <div class="dz-message needsclick">
                Drop files here or click to upload.
            </div>
        </form>

    </div>
</section>

<script type="application/javascript">
    $(".dropzone").dropzone({
        acceptedFiles: '.mp3'
    });
</script>