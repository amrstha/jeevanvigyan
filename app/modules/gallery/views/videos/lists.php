<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 2/1/17
 * Time: 11:19 PM
 */
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="pull-right">
            <a href="<?=admin_url('gallery/album')?>#videos" class="btn btn-info btn-xs tooltip-info" data-toggle="tooltip" title="Back to album">
                <i class="fa fa-reply"></i>
            </a>

            <a href="<?=admin_url('gallery/videos/add/'.url_encrypt($album->id))?>" class="btn btn-success btn-xs tooltip-success loadForm" data-toggle="tooltip" data-successbtn="Add" data-addclass="bootbox-modal-sm" title="Add New Videos">
                <i class="fa fa-upload"></i>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="row">

            <?php foreach($videos as $video): ?>
                <div class="col-md-55">
                    <div class="thumbnail">
                        <div class="image view view-first">
                            <img style="width: 100%; height: 100%; display: block;" src="<?=assets('custom/img/video-placeholder.jpg', 'admin')?>" alt="<?=$video->title?>" />
                            <div class="mask">
                                <p><?=$video->caption?></p>
                                <div class="tools tools-bottom">
                                    <a href="<?=admin_url('gallery/videos/edit/'.url_encrypt($album->id).'/'.url_encrypt($video->id))?>" class="loadForm" data-successbtn="Update" data-addclass="bootbox-modal-sm" title="Edit Video">
                                        <i class="fa fa-pencil"></i>
                                    </a>

                                    <a href="<?=admin_url('gallery/videos/delete/'.url_encrypt($album->id).'/'.url_encrypt($video->id))?>" class="confirmDiag" data-confirmbtn="Yes, delete this video!" data-msg="Are you sure? Once confirmed it cannot be undone." title="delete">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="caption">
                            <p><?=$video->title?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach;
            if (count($videos) == 0): ?>
                <div class="alert alert-info" role="alert">No video added in this album yet.
                    <a href="<?=admin_url('gallery/videos/add/'.url_encrypt($album->id))?>" class="btn btn-success btn-xs tooltip-success loadForm" data-toggle="tooltip" data-successbtn="Add" data-addclass="bootbox-modal-sm" title="Add New Video">
                        Add
                    </a>
                </div>
            <?php endif ?>

        </div>

    </div>
</div>
