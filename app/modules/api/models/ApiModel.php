<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/18/17
 * Time: 5:31 PM
 */
class ApiModel extends CI_Model
{
    private $CONSUMER_KEY;
    private $CONSUMER_SECRET;
    private $CONSUMER_TTL;

    public function __construct()
    {
        parent::__construct();

        $this->CONSUMER_SECRET = 'Zw0c3Z2d0OW2doUTXNoUwQ0cNYUU3ZUT09SU5HSUUwQUaG9NYaGNHX5wZSZSUZOWZw0c3Z2d5wZSZSUZOWoUwQ0cN0OWYUU3ZUT09SU5HSUUwQUaG2doUTXN9NYaGNHX';
        $this->CONSUMER_KEY = 'ZwZSUU50OW2doUTXNHSUNYaGZOWUwQ0c3Z2doUwQ0cNYUU3ZUT09SNHX5wZSUaG9';
        $this->CONSUMER_TTL = 86400;
    }

    public function getUserProfile($id = null)
    {
        $user = $this->authentication->getUser($id);

        if ($user === false) return false;

        return [
            'id'            => $user->id,
            'username'      => $user->username,
            'email'         => $user->email,
            'last_login'    => $user->last_login,
            'first_name'    => $user->first_name,
            'last_name'     => $user->last_name,
            'full_name'     => $user->first_name.' '.$user->last_name,
            'profile_pic'   => $user->profile_pic
        ];
    }

    public function getUserToken($id)
    {
        $token = $this->generateToken($id);
        $data = [
            'user'   => $id,
            'token'  => $token,
            'ttl'    => now() + $this->CONSUMER_TTL
        ];
        if ($this->checkUser($id)) {
            // update
            $this->db->where('user', $id)->update(TBL_API_USERS_TOKEN, $data);
        } else {
            // insert
            $this->db->insert(TBL_API_USERS_TOKEN, $data);
        }

        return $token;
    }
    private function generateToken($user_id)
    {
        $this->load->library("JWT");
        return $this->jwt->encode([
            'consumerKey'   => $this->CONSUMER_KEY,
            'userId'        => $user_id,
            'issuedAt'      => date(DATE_ISO8601, strtotime("now")),
            'ttl'           => $this->CONSUMER_TTL
        ], $this->CONSUMER_SECRET);
    }
    private function checkUser($id)
    {
        $user = $this->db->where([
            'user'   => $id
        ])->get(TBL_API_USERS_TOKEN);
        return $user->num_rows() > 0;
    }

    public function checkUserToken($token)
    {
        $user = $this->db->where([
            'token'     => $token,
            //'ttl >'     => time()
        ])->get(TBL_API_USERS_TOKEN);
        return $user->num_rows() > 0 ? $user->row()->user : false;
    }
}