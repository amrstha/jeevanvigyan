<?php

/**
 * Created by PhpStorm.
 * User: manjul
 * Date: 03/06/17
 * Time: 05:32 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class QuotationsController extends Api_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('quotations/QuotationsModel', 'quotations');
    }

    public function index_get()
    {
        try {

            $quotations = $this->quotations->getAll(true);
            $quotResp = [];
            foreach ($quotations as $key => $q) {
                $quotResp[$key] = [
                    'id'            => $q->id,
                    'language'      => $q->language,
                    'quotation'     => $q->quotation
                ];
            }

            $this->response([
                'status'    => 'success',
                'message'   => null,
                'data'      => $quotResp
            ], self::HTTP_OK);

        } catch (Exception $e) {
            $this->error($e);
        }
    }

    public function id_get()
    {
        try {

            $id = $this->uri->segment(5);
            if (empty($id) || !is_numeric($id)) {
                throw new Exception("Invalid Request", self::HTTP_BAD_REQUEST);
            }

            $n = $this->quotations->getById($id);
            if ($n === false) {
                throw new Exception("Quotations Not Found", self::HTTP_NOT_FOUND);
            }

            $quotResp = [
                'id'            => $q->id,
                'language'      => $q->language,
                'quotation'     => $q->quotation
            ];

            $this->response([
                'status'    => 'success',
                'message'   => null,
                'data'      => $quotResp
            ], self::HTTP_OK);

        } catch (Exception $e) {
            $this->error($e);
        }
    }
    
}