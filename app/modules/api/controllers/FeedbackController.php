<?php

/**
 * Created by PhpStorm.
* User: puncoz
* Date: 1/23/17
* Time: 12:20 AM
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class FeedbackController extends Api_Controller
{
	private $user;

	public function __construct()
	{
		parent::__construct();

		$this->user = $this->auth();

		$this->load->model('feedback/FeedbackModel', 'feedback');
	}

	public function index_get()
	{
		try {

			$feedbacks = $this->feedback->getAllByUser($this->user, true);
            $feedbackResp = [];
			foreach ($feedbacks as $feedback) {
			    $feedbackResp[] = [
                    'id'        => $feedback->id,
                    'message'   => $feedback->message,
                    'created_at'=> $feedback->created_at
                ];
            }

			$this->response([
					'status'    => 'success',
					'message'   => null,
					'data'      => $feedbackResp
					], self::HTTP_OK);

		} catch (Exception $e) {
			$this->error($e);
		}
	}

    public function add_post()
    {
	   	try {
	   		$this->form_validation->set_rules('feedback', 'Feedback', 'required');
	   	
	   		if ($this->form_validation->run() === FALSE) {
	   			throw new Exception(validation_errors(), self::HTTP_NOT_ACCEPTABLE);
	   		}

	   		$userData = [
                'message'  		=> $this->post('feedback'),
                'customer_id'  	=> $this->user,
                'status'		=> 1
	   		];
	   			
	   		if ($this->feedback->add($userData) === false) {
	   			throw new Exception('Feedback failed, please try again', self::HTTP_INTERNAL_SERVER_ERROR);
	   		}
	   	
	   		$this->response([
                'status'    => 'success',
                'message'   => 'Feedback added  successfully.',
                'data'      => null
            ], self::HTTP_OK);
	   	} catch (Exception $e) {
	   		$this->error($e);
	   	}	
    }

    public function id_get()
	{
		try {

			$id = $this->uri->segment(5);
			if (empty($id) || !is_numeric($id)) {
				throw new Exception("Invalid Request", self::HTTP_BAD_REQUEST);
			}

			$feedback = $this->feedback->getById($id);
			if (!$feedback) {
				throw new Exception("Feedback not found", self::HTTP_NOT_FOUND);
			}

            $feedbackResp = [
                'id'        => $feedback->id,
                'message'   => $feedback->message,
                'created_at'=> $feedback->created_at
            ];

			$this->response([
                'status'    => 'success',
                'message'   => null,
                'data'      => $feedbackResp
            ], self::HTTP_OK);

		} catch (Exception $e) {
			$this->error($e);
		}
	}
}