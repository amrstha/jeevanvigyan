<?php

/**
 * Created by PhpStorm.
 * User: manjul
 * Date: 06/06/17
 * Time: 10:07 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class ContactController extends Api_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('contact/ContactModel', 'contact');
    }

    public function add_post()
    {
	   	try {
	   		$this->form_validation->set_rules('name', 'Name', 'required');
	   		$this->form_validation->set_rules('email', 'Email', 'required');
	   		$this->form_validation->set_rules('subject', 'Subject', 'required');
	   		$this->form_validation->set_rules('message', 'Message', 'required');
	   	
	   		if ($this->form_validation->run() === FALSE) {
	   			throw new Exception(validation_errors(), self::HTTP_NOT_ACCEPTABLE);
	   		}

	   		$contactData = [
                'name'  		=> $this->post('name'),
                'email'			=> $this->post('email'),
                'subject'		=> $this->post('subject'),
                'message'		=> $this->post('message')
	   		];
	   			
	   		if ($this->contact->add($contactData) === false) {
	   			throw new Exception('Message sending failed, please try again', self::HTTP_INTERNAL_SERVER_ERROR);
	   		}
	   	
	   		$this->response([
                'status'    => 'success',
                'message'   => 'Message sent successfully. Thank you for contacting us. We will keep in touch.',
                'data'      => null
            ], self::HTTP_OK);
	   	} catch (Exception $e) {
	   		$this->error($e);
	   	}	
    }

}