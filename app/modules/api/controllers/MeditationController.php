<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MeditationController extends Api_Controller{
	
	private $user;
	
	public function __construct()
	{
		parent::__construct();
	
		$this->user = $this->auth();
	
		$this->load->model('meditation/MeditationModel', 'meditation');
		$this->load->model('meditation/MeditationDetailModel', 'meditationDetail');
	}

	
	public function index_get()
	{
		try {

		    $meditations = $this->meditation->getAll(true);
		    $meditationResponse = [];
		    foreach ($meditations as $key => $meditation) {
		        $meditationResponse[$key] = [
                    'id'            => $meditation->id,
                    'image'         => base_url($this->config->item('file_upload_dir_user')['meditation'].$meditation->image),
                    'title'         => ($this->language == 'np') ? $meditation->title_np : $meditation->title,
                    'description'   => ($this->language == 'np') ? $meditation->description_np : $meditation->description
                ];
                $details = $this->meditationDetail->getAll($meditation->id);
                $detailArr = [];
                $detailArr['audio'] = [];
                $detailArr['video'] = [];
                $detailArr['image'] = [];
                foreach ($details as $detail) {
                    $detailArr[$detail->type][] = [
                        'id'            => $detail->id,
                        'title'         => 'seme title',
                        'caption'       => 'some caption',
                        'value'         => ($detail->type == 'video') ? $detail->value : base_url($this->config->item('file_upload_dir_user')['meditation'].$detail->value),
                        'created_at'    => date('Y-m-d', strtotime($detail->created_at))
                    ];
                }
                $meditationResponse[$key]['details'] = $detailArr;
            }

			$this->response([
					'status'    => 'success',
					'message'   => null,
					'data'      => $meditationResponse
					], self::HTTP_OK);
	
		} catch (Exception $e) {
			$this->error($e);
		}
	}

	public function id_get()
    {
		try{
            $id = $this->uri->segment(5);
            if (empty($id) || !is_numeric($id)) {
                throw new Exception("Invalid Request", self::HTTP_BAD_REQUEST);
            }

            $meditations = $this->meditation->getById($id);
            if ($meditations === false) {
                throw new Exception("Meditation not found.", self::HTTP_NOT_FOUND);
            }

            $meditationResponse = [
                'id'            => $meditations->id,
                'image'         => base_url($this->config->item('file_upload_dir_user')['meditation'].$meditations->image),
                'title'         => ($this->language == 'np') ? $meditations->title_np : $meditations->title,
                'description'   => ($this->language == 'np') ? $meditations->description_np : $meditations->description
            ];
            $details = $this->meditationDetail->getAll($meditations->id);
            $detailArr = [];
            foreach ($details as $detail) {
                $detailArr[$detail->type][] = [
                    'id'            => $detail->id,
                    'title'         => 'seme title',
                    'caption'       => 'some caption',
                    'value'         => ($detail->type == 'video') ? $detail->value : base_url($this->config->item('file_upload_dir_user')['meditation'].$detail->value),
                    'created_at'    => date('Y-m-d', strtotime($detail->created_at))
                ];
            }
            $meditationResponse['details'] = $detailArr;

			$this->response([
                'status'    => 'success',
                'message'   => null,
                'data'      => $meditationResponse,
            ], self::HTTP_OK);
			
		}catch(Exception $e){
			$this->error($e);
		}
	}
}