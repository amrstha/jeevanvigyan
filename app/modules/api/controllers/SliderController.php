<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/20/17
 * Time: 10:12 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class SliderController extends Api_Controller
{
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('slider/sliderModel', 'slider');

        $this->user = $this->auth();
    }

    public function index_get()
    {
        try {

            $sliders = $this->slider->getAll(true);
            $slidersResp = [];
            foreach ($sliders as $key => $slider) {
                $slidersResp[$key] = [
                    'id'            => $slider->id,
                    'title'         => ($this->language == 'np') ? $slider->title_np : $slider->title,
                    'caption'       => ($this->language == 'np') ? $slider->caption_np : $slider->caption,
                    'image'         => base_url($this->config->item('file_upload_dir_user')['slider'].$slider->image),
                    'created_at'    => $slider->created_at
                ];
            }

            $this->response([
                'status'    => 'success',
                'message'   => null,
                'data'      => $slidersResp
            ], self::HTTP_OK);

        } catch (Exception $e) {
            $this->error($e);
        }
    }

    public function id_get()
    {
        try {

            $id = $this->uri->segment(5);
            if (empty($id) || !is_numeric($id)) {
                throw new Exception("Invalid Request", self::HTTP_BAD_REQUEST);
            }

            $slider = $this->slider->getById($id);
            if (!$slider) {
                throw new Exception("Slider not found", self::HTTP_NOT_FOUND);
            }

            $sliderResp = [
                'id'            => $slider->id,
                'title'         => ($this->language == 'np') ? $slider->title_np : $slider->title,
                'caption'       => ($this->language == 'np') ? $slider->caption_np : $slider->caption,
                'image'         => base_url($this->config->item('file_upload_dir_user')['slider'].$slider->image),
                'created_at'    => $slider->created_at
            ];

            $this->response([
                'status'    => 'success',
                'message'   => null,
                'data'      => $sliderResp
            ], self::HTTP_OK);

        } catch (Exception $e) {
            $this->error($e);
        }
    }

}