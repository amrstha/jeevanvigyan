<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 15/08/2017
 * Time: 15:54
 */

(defined('BASEPATH')) || exit('No direct script access allowed');

class RecentController extends Api_Controller
{
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->user = $this->auth();

        $this->load->model('recent/recentModel', 'recent');
        $this->load->model('events/eventsModel', 'events');
        $this->load->model('events/EventsBookModel', 'eventsbook');
        $this->load->model('pages/newsModel', 'news');
        $this->load->model('pages/blogModel', 'blog');
        $this->load->model('gallery/albumModel', 'album');
        $this->load->model('gallery/filesModel', 'files');
        $this->load->model('meditation/MeditationModel', 'meditation');
        $this->load->model('meditation/MeditationDetailModel', 'meditationDetail');
    }

    public function index_get()
    {
        try {

            $recentData = $this->recent->getAll();
            $recentDataResp = [];
            foreach ($recentData as $key => $recent) {
                $booked = null;
                if ($recent->type == 'video') continue;

                if ($recent->type == 'article') {
                    if ($recent->fk_id == '2') {
                        $type = 'blog';
                    } else {
                        $type = 'news';
                    }
                    $file = base_url($this->config->item('file_upload_dir_user')['pages'] . $recent->file);
                }

                if ($recent->type == 'meditation') {
                    $type = $recent->type;
                    $file = base_url($this->config->item('file_upload_dir_user')['meditation'] . $recent->file);
                }

                if ($recent->type == 'event') {
                    $type = $recent->type;
                    $file = base_url($this->config->item('file_upload_dir_user')['events'] . $recent->file);
                    $booked = $this->eventsbook->checkbooking($recent->id, $this->user) ? 1 : 0;
                }
                if ($recent->type == 'video') {
                    $type = $recent->type;
                    $file = $recent->file;
                }

                $recentDataResp[] = [
                    'id' => $recent->id,
                    'title' => ($this->language == 'np') ? $recent->title_np : $recent->title,
                    'description' => ($this->language == 'np') ? $recent->description_np : $recent->description,
                    'file' => $file,
                    'date_start' => $recent->date_start,
                    'date_end' => $recent->date_end,
                    'created_at' => $recent->created_at,
                    'booked' => isset($booked) ? $booked : null,
                    'type' => $type
                ];
            }
            $this->response([
                'status' => 'success',
                'message' => null,
                'data' => $recentDataResp
            ], self::HTTP_OK);

        } catch (Exception $e) {
            $this->error($e);
        }
    }

    public function idtype_get()
    {
        try {

            $id = $this->uri->segment(5);
            $type = $this->uri->segment(6);
            if (empty($id) || !is_numeric($id) || empty($type)) {
                throw new Exception("Invalid Request", self::HTTP_BAD_REQUEST);
            }

            if ($type == 'event') {
                $event = $this->events->getById($id);
                if (!$event) {
                    throw new Exception("Program not found", self::HTTP_NOT_FOUND);
                }

                $allResp = [
                    'id' => $event->id,
                    'title' => ($this->language == 'np') ? $event->title_np : $event->title,
                    'description' => ($this->language == 'np') ? $event->description_np : $event->description,
                    'cover_pics' => base_url($this->config->item('file_upload_dir_user')['events'] . $event->cover_pics),
                    'date_start' => $event->date_start,
                    'date_end' => $event->date_end,
                    'booked' => $this->eventsbook->checkbooking($event->id, $this->user) ? 1 : 0,
                    'status' => $event->status
                ];

            } elseif ($type == 'meditation') {
                $meditations = $this->meditation->getById($id);
                if ($meditations === false) {
                    throw new Exception("Meditation not found.", self::HTTP_NOT_FOUND);
                }

                $allResp = [
                    'id' => $meditations->id,
                    'image' => base_url($this->config->item('file_upload_dir_user')['meditation'] . $meditations->image),
                    'title' => ($this->language == 'np') ? $meditations->title_np : $meditations->title,
                    'description' => ($this->language == 'np') ? $meditations->description_np : $meditations->description
                ];
                $details = $this->meditationDetail->getAll($meditations->id);
                $detailArr = [];
                foreach ($details as $detail) {
                    $detailArr[$detail->type][] = [
                        'id' => $detail->id,
                        'value' => ($detail->type == 'video') ? $detail->value : base_url($this->config->item('file_upload_dir_user')['meditation'] . $detail->value),
                        'created_at' => date('Y-m-d', strtotime($detail->created_at))
                    ];
                }
                $allResp['details'] = $detailArr;
            }

            if ($type == 'blog') {
                $n = $this->blog->getById($id);
                if ($n === false) {
                    throw new Exception("Blog Not Found", self::HTTP_NOT_FOUND);
                }

                $allResp = [
                    'id' => $n->id,
                    'title' => ($this->language == 'np') ? $n->title_np : $n->title,
                    'content' => ($this->language == 'np') ? $n->content_np : $n->content,
                    'image' => base_url($this->config->item('file_upload_dir_user')['pages'] . $n->image),
                    'pub_date' => date('Y-m-d', strtotime($n->pub_date))
                ];
            }

            if ($type == 'news') {
                $n = $this->news->getById($id);
                if ($n === false) {
                    throw new Exception("News Not Found", self::HTTP_NOT_FOUND);
                }

                $allResp = [
                    'id' => $n->id,
                    'title' => ($this->language == 'np') ? $n->title_np : $n->title,
                    'content' => ($this->language == 'np') ? $n->content_np : $n->content,
                    'image' => base_url($this->config->item('file_upload_dir_user')['pages'] . $n->image),
                    'pub_date' => date('Y-m-d', strtotime($n->pub_date))
                ];
            }

            if ($type == 'video') {
                $file = $this->files->getById($id);
                if ($file === false) {
                    throw new Exception("Video Not Found", self::HTTP_NOT_FOUND);
                }
                if (strlen($file->file) > 15) {
                    throw new Exception("Video Not Found", self::HTTP_NOT_FOUND);
                }
                $allResp = [
                    'id' => $file->id,
                    'title' => ($this->language == 'np') ? $file->title_np : $file->title,
                    'caption' => ($this->language == 'np') ? $file->caption_np : $file->caption,
                    'file' => $file->file,
                    'thumbnail' => $file->thumbnail,
                    'created_at' => $file->created_at
                ];
            }

            $this->response([
                'status' => 'success',
                'message' => null,
                'data' => $allResp
            ], self::HTTP_OK);

        } catch (Exception $e) {
            $this->error($e);
        }
    }


}