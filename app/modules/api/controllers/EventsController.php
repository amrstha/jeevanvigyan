<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/23/17
 * Time: 12:20 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class EventsController extends Api_Controller
{
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->user = $this->auth();

        $this->load->model('events/eventsModel', 'events');
        $this->load->model('events/EventsBookModel', 'eventsbook');
    }

    public function index_get()
    {
        try {

            $events = $this->events->getAll(true);
            $eventsResp = [];
            $booked = 0;
            foreach ($events as $key => $event) {

                $eventsResp[$key] = [
                    'id'            => $event->id,
                    'title'         => ($this->language == 'np') ? $event->title_np : $event->title,
                    'description'   => ($this->language == 'np') ? $event->description_np : $event->description,
                    'cover_pics'    => base_url($this->config->item('file_upload_dir_user')['events'].$event->cover_pics),
                    'date_start'    => $event->date_start,
                    'date_end'      => $event->date_end,
                    'status'        => $event->status,
                    'booked'        => $this->eventsbook->checkbooking($event->id, $this->user) ? 1 : 0
                ];
            }

            $this->response([
                'status'    => 'success',
                'message'   => null,
                'data'      => $eventsResp
            ], self::HTTP_OK);

        } catch (Exception $e) {
            $this->error($e);
        }
    }

    public function id_get()
    {
        try {

            $id = $this->uri->segment(5);
            if (empty($id) || !is_numeric($id)) {
                throw new Exception("Invalid Request", self::HTTP_BAD_REQUEST);
            }

            $event = $this->events->getById($id);
            if ($event === false) {
                throw new Exception("Event Not Found", self::HTTP_NOT_FOUND);
            }

            $eventResp = [
                'id'            => $event->id,
                'title'         => ($this->language == 'np') ? $event->title_np : $event->title,
                'description'   => ($this->language == 'np') ? $event->description_np : $event->description,
                'cover_pics'    => base_url($this->config->item('file_upload_dir_user')['events'].$event->cover_pics),
                'date_start'    => $event->date_start,
                'date_end'      => $event->date_end,
                'booked'        => $this->eventsbook->checkbooking($event->id, $this->user) ? 1 : 0,
                'status'        => $event->status
            ];

            $this->response([
                'status'    => 'success',
                'message'   => null,
                'data'      => $eventResp
            ], self::HTTP_OK);

        } catch (Exception $e) {
            $this->error($e);
        }
    }

    public function book_post()
    {
        try {
            $eventId = $this->post('event_id');
            if (empty($eventId) || !is_numeric($eventId)) {
                throw new Exception("Invalid Request", self::HTTP_BAD_REQUEST);
            }
            // check for event existance
            $event = $this->events->getById($eventId);
            if (!$event) {
                throw new Exception("Event not found!", self::HTTP_NOT_FOUND);
            }

            $status = $this->eventsbook->checkbooking($eventId, $this->user);
            $result = false;

            if ($status === false) {
                $result = $this->eventsbook->book($eventId, $this->user);
            } else if ($status === '1') {
                throw new Exception("Event already booked", self::HTTP_ACCEPTED);
            } else if ($status === '0') {
                $result = $this->eventsbook->rebook($eventId, $this->user);
            }

            if ($result === false) {
                throw new Exception("Failed to book event, please try again later.", self::HTTP_INTERNAL_SERVER_ERROR);
            }

            $this->response([
                'status'    => 'success',
                'message'   => 'Event booked successfully.',
                'data'      => null
            ], self::HTTP_OK);
        } catch (Exception $e) {
            $this->error($e);
        }
    }

    public function unbook_post()
    {
        try {
            $eventId = $this->post('event_id');
            if (empty($eventId) || !is_numeric($eventId)) {
                throw new Exception("Invalid Request", self::HTTP_BAD_REQUEST);
            }

            $status = $this->eventsbook->checkbooking($eventId, $this->user);
            $result = false;

            if ($status === false) {
                throw new Exception("You have not registered to this event yet", self::HTTP_ACCEPTED);
            } else if ($status == '1') {
                $result = $this->eventsbook->unbook($eventId, $this->user);
            } else if ($status == '0') {
                throw new Exception("Event already unbooked", self::HTTP_ACCEPTED);
            }

            if ($result === false) {
                throw new Exception("Failed to unbook event, please try again later.", self::HTTP_INTERNAL_SERVER_ERROR);
            }

            $this->response([
                'status'    => 'success',
                'message'   => 'Event unbooked successfully.',
                'data'      => null
            ], self::HTTP_OK);
        } catch (Exception $e) {
            $this->error($e);
        }
    }
    
    public function booking_get()
    {
    	try{
            $events = $this->events->userBooked($this->user);
            $eventsResp = [];
            foreach ($events as $key => $event) {
                $eventsResp[$key] = [
                    'id'            => $event->id,
                    'title'         => ($this->language == 'np') ? $event->title_np : $event->title,
                    'description'   => ($this->language == 'np') ? $event->description_np : $event->description,
                    'cover_pics'    => base_url($this->config->item('file_upload_dir_user')['events'].$event->cover_pics),
                    'date_start'    => $event->date_start,
                    'date_end'      => $event->date_end,
                    'status'        => $event->status
                ];
            }

            $this->response([
                'status'    => 'success',
                'message'   => null,
                'data'      => $eventsResp
            ], self::HTTP_OK);
    	} catch (Exception $e){
    		$e->error($e);
    	}
    }
}