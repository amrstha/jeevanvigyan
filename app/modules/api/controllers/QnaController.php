<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 3/14/17
 * Time: 12:05 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class QnaController extends Api_Controller
{
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->user = $this->auth();

        $this->load->model('qna/qnaModel', 'qna');
    }

    public function index_get()
    {
        try {

            $qnas = $this->qna->getAll(true);
            $qnaResp = [];
            foreach ($qnas as $qna) {
                $qnaResp[] = [
                    'id'        => $qna->id,
                    'question'  => $qna->question,
                    'answer'    => $qna->answer,
                    'created_at'=> $qna->created_at
                ];
            }

            $this->response([
                'status'    => 'success',
                'message'   => null,
                'data'      => $qnaResp
            ], self::HTTP_OK);

        } catch (Exception $e) {
            $this->error($e);
        }
    }

    public function ask_post()
    {
        try {
            $this->form_validation->set_rules('question', 'Question', 'required');

            if ($this->form_validation->run() === FALSE) {
                throw new Exception(validation_errors(), self::HTTP_NOT_ACCEPTABLE);
            }

            $userData = [
                'question'  	=> $this->post('question'),
                'asked_by'  	=> $this->user,
                'status'		=> 0
            ];

            if ($this->qna->add($userData) === false) {
                throw new Exception('Question ask failed, please try again', self::HTTP_INTERNAL_SERVER_ERROR);
            }

            $this->response([
                'status'    => 'success',
                'message'   => 'Question asked successfully.',
                'data'      => null
            ], self::HTTP_OK);
        } catch (Exception $e) {
            $this->error($e);
        }
    }

    public function id_get()
    {
        try {

            $id = $this->uri->segment(5);
            if (empty($id) || !is_numeric($id)) {
                throw new Exception("Invalid Request", self::HTTP_BAD_REQUEST);
            }

            $qna = $this->qna->getById($id);
            if (!$qna) {
                throw new Exception("Q&A not found", self::HTTP_NOT_FOUND);
            }

            $qnaResp = [
                'id'        => $qna->id,
                'question'  => $qna->question,
                'answer'    => $qna->answer,
                'created_at'=> $qna->created_at
            ];

            $this->response([
                'status'    => 'success',
                'message'   => null,
                'data'      => $qnaResp
            ], self::HTTP_OK);

        } catch (Exception $e) {
            $this->error($e);
        }
    }
}