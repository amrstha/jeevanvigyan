<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 2/7/17
 * Time: 3:05 PM
 */
class GalleryController extends Api_Controller
{
    private $user;
    private $albumType = ['1' => 'Image', '2' => 'Videos', '3' => 'Audio'];

    public function __construct()
    {
        parent::__construct();

        $this->user = $this->auth();

        $this->load->model('gallery/albumModel', 'album');
        $this->load->model('gallery/filesModel', 'files');
    }

    public function index_get()
    {
        try {

            $albums = $this->album->getAll(true);
            $galleries = [];
            foreach ($albums as $key => $album) {
                $galleries[$key] = [
                    'id'            => $album->id,
                    'name'          => ($this->language == 'np') ? $album->name_np : $album->name,
                    'description'   => ($this->language == 'np') ? $album->description_np : $album->description,
                    'image'         => base_url($this->config->item('file_upload_dir_user')['gallery_album'].$album->image),
                    'type'          => $this->albumType[$album->type],
                    'created_at'    => $album->created_at,
                    'updated_at'    => $album->updated_at
                ];
                $files = $this->files->getAll($album->id);
                $filesArr = [];
                foreach ($files as $file) {
                    $filesArr[] = [
                        'id'            => $file->id,
                        'title'         => ($this->language == 'np') ? $file->title_np : $file->title,
                        'caption'       => ($this->language == 'np') ? $file->caption_np : $file->caption,
                        'file'          => ($album->type == '2') ? $file->file : base_url($this->config->item('file_upload_dir_user')['gallery_images'].$file->file),
                        'created_at'    => date('Y-m-d', strtotime($file->created_at))
                    ];
                }
                $galleries[$key]['files'] = $filesArr;
            }

            $this->response([
                'status'    => 'success',
                'message'   => null,
                'data'      => $galleries
            ], self::HTTP_OK);

        } catch (Exception $e) {
            $this->error($e);
        }
    }

    public function images_get()
    {
        try {

            $this->db->where('type', 1);
            $albums = $this->album->getAll(true);
            $galleries = [];
            foreach ($albums as $key => $album) {
                $galleries[$key] = [
                    'id'            => $album->id,
                    'name'          => ($this->language == 'np') ? $album->name_np : $album->name,
                    'description'   => ($this->language == 'np') ? $album->description_np : $album->description,
                    'image'         => base_url($this->config->item('file_upload_dir_user')['gallery_album'].$album->image),
                    'type'          => $this->albumType[$album->type],
                    'created_at'    => $album->created_at,
                    'updated_at'    => $album->updated_at
                ];
                $files = $this->files->getAll($album->id);
                $filesArr = [];
                foreach ($files as $file) {
                    $filesArr[] = [
                        'id'            => $file->id,
                        'title'         => ($this->language == 'np') ? $file->title_np : $file->title,
                        'caption'       => ($this->language == 'np') ? $file->caption_np : $file->caption,
                        'file'          => ($album->type == '2') ? $file->file : base_url($this->config->item('file_upload_dir_user')['gallery_images'].$file->file),
                        'created_at'    => date('Y-m-d', strtotime($file->created_at))
                    ];
                }
                $galleries[$key]['files'] = $filesArr;
            }

            $this->response([
                'status'    => 'success',
                'message'   => null,
                'data'      => $galleries
            ], self::HTTP_OK);

        } catch (Exception $e) {
            $this->error($e);
        }
    }

    public function videos_get()
    {
        try {

            $this->db->where('type', 2);
            $albums = $this->album->getAll(true);
            $galleries = [];
            foreach ($albums as $key => $album) {
                $galleries[$key] = [
                    'id'            => $album->id,
                    'name'          => ($this->language == 'np') ? $album->name_np : $album->name,
                    'description'   => ($this->language == 'np') ? $album->description_np : $album->description,
                    'image'         => base_url($this->config->item('file_upload_dir_user')['gallery_album'].$album->image),
                    'type'          => $this->albumType[$album->type],
                    'created_at'    => $album->created_at,
                    'updated_at'    => $album->updated_at
                ];
                $files = $this->files->getAll($album->id);
                $filesArr = [];
                foreach ($files as $file) {
                    $filesArr[] = [
                        'id'            => $file->id,
                        'title'         => ($this->language == 'np') ? $file->title_np : $file->title,
                        'caption'       => ($this->language == 'np') ? $file->caption_np : $file->caption,
                        'file'          => ($album->type == '2') ? $file->file : base_url($this->config->item('file_upload_dir_user')['gallery_images'].$file->file),
                        'created_at'    => date('Y-m-d', strtotime($file->created_at))
                    ];
                }
                $galleries[$key]['files'] = $filesArr;
            }

            $this->response([
                'status'    => 'success',
                'message'   => null,
                'data'      => $galleries
            ], self::HTTP_OK);

        } catch (Exception $e) {
            $this->error($e);
        }
    }

    public function audios_get()
    {
        try {

            $this->db->where('type', 3);
            $albums = $this->album->getAll(true);
            $galleries = [];
            foreach ($albums as $key => $album) {
                $galleries[$key] = [
                    'id'            => $album->id,
                    'name'          => ($this->language == 'np') ? $album->name_np : $album->name,
                    'description'   => ($this->language == 'np') ? $album->description_np : $album->description,
                    'image'         => base_url($this->config->item('file_upload_dir_user')['gallery_album'].$album->image),
                    'type'          => $this->albumType[$album->type],
                    'created_at'    => $album->created_at,
                    'updated_at'    => $album->updated_at
                ];
                $files = $this->files->getAll($album->id);
                $filesArr = [];
                foreach ($files as $file) {
                    $filesArr[] = [
                        'id'            => $file->id,
                        'title'         => ($this->language == 'np') ? $file->title_np : $file->title,
                        'caption'       => ($this->language == 'np') ? $file->caption_np : $file->caption,
                        'file'          => base_url($this->config->item('file_upload_dir_user')['gallery_audio'].$file->file),
                        'created_at'    => date('Y-m-d', strtotime($file->created_at))
                    ];
                }
                $galleries[$key]['files'] = $filesArr;
            }

            $this->response([
                'status'    => 'success',
                'message'   => null,
                'data'      => $galleries
            ], self::HTTP_OK);

        } catch (Exception $e) {
            $this->error($e);
        }
    }
}