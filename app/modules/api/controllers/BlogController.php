<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 2/24/17
 * Time: 10:48 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class BlogController extends Api_Controller
{
    private $user;

    public function __construct()
    {
        parent::__construct();

        $this->user = $this->auth();

        $this->load->model('pages/blogModel', 'blog');
    }

    public function index_get()
    {
        try {

            $news = $this->blog->getAll(true);
            $newsResp = [];
            foreach ($news as $key => $n) {
                $newsResp[$key] = [
                    'id'            => $n->id,
                    'title'         => ($this->language == 'np') ? $n->title_np : $n->title,
                    'content'       => ($this->language == 'np') ? $n->content_np : $n->content,
                    'image'         => base_url($this->config->item('file_upload_dir_user')['pages'].$n->image),
                    'pub_date'      => date('Y-m-d', strtotime($n->pub_date))
                ];
            }

            $this->response([
                'status'    => 'success',
                'message'   => null,
                'data'      => $newsResp
            ], self::HTTP_OK);

        } catch (Exception $e) {
            $this->error($e);
        }
    }

    public function id_get()
    {
        try {

            $id = $this->uri->segment(5);
            if (empty($id) || !is_numeric($id)) {
                throw new Exception("Invalid Request", self::HTTP_BAD_REQUEST);
            }

            $n = $this->blog->getById($id);
            if ($n === false) {
                throw new Exception("Blog Not Found", self::HTTP_NOT_FOUND);
            }

            $newsResp = [
                'id'            => $n->id,
                'title'         => ($this->language == 'np') ? $n->title_np : $n->title,
                'content'       => ($this->language == 'np') ? $n->content_np : $n->content,
                'image'         => base_url($this->config->item('file_upload_dir_user')['pages'].$n->image),
                'pub_date'      => date('Y-m-d', strtotime($n->pub_date))
            ];

            $this->response([
                'status'    => 'success',
                'message'   => null,
                'data'      => $newsResp
            ], self::HTTP_OK);

        } catch (Exception $e) {
            $this->error($e);
        }
    }
}