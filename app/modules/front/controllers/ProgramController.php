<?php

/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 02/08/2017
 * Time: 11:30
 */
class ProgramController extends Front_Controller
{

    /**
     * ProgramController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('program/programModel', 'program');
    }

    public function index($offset = 0)
    {

        // Pagination
        $config = pagination(base_url('program/index'), count($this->program->getAll(true)), 6);
        if ($config != NULL) {
            $this->pagination->initialize($config);
            self::$viewData['programPagination'] = $this->pagination->create_links();
        }

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('program_front_module'),
            'keywords' => $this->lang->line('program_front_module'),
            'description' => $this->lang->line('program_front_module'),
        ];

        self::$viewData['programmes'] = $this->program->getAll(true, 6, $offset);

        $this->load->fronttheme('program/lists', self::$viewData);
    }

    public function detail()
    {
        $slug = $this->uri->segment(2);
        if (empty($slug) || !$program = $this->program->getBySlug($slug)) {
            show_404();
            exit();
        }

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('program_front_module'),
            'keywords' => $this->lang->line('program_front_module'),
            'description' => $this->lang->line('program_front_module'),
        ];

        self::$viewData['program'] = $program;

        $this->load->fronttheme('program/detail', self::$viewData);
    }
}
