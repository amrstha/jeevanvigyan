<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 5/28/17
 * Time: 8:31 AM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

/**
 * Class PagesController
 */
class PagesController extends Front_Controller
{

    /**
     * PagesController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('pages/articleModel', 'article');
    }

    /**
     *
     */
    public function index()
    {
        echo 'hello';
    }

    /**
     * Get Page by slug
     */
    public function pageBySlug()
    {
        $slug = $this->uri->segment(2);
        if (empty($slug) || !$article = $this->article->getBySlug($slug)) {
            show_404();
            exit();
        }

        self::$viewData['pageInfo'] = (object) [
            'title'       => $this->lang->line('pages_front_module'),
            'keywords'    => $this->lang->line('pages_front_module'),
            'description' => $this->lang->line('pages_front_module'),
        ];

        self::$viewData['article'] = $article;

        $this->load->fronttheme('article', self::$viewData);
    }
}
