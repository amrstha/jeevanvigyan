<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 08/01/17
 * Time: 03:49 PM
 */
class BlogController extends Front_Controller
{

    /**
     * BlogController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('pages/blogModel', 'blog');
    }

    public function index($offset = 0)
    {
//         Pagination
        $config = pagination(base_url('blog/index'), count($this->blog->getAll(true)), 5);

        if ($config != NULL) {
            $this->pagination->initialize($config);
            self::$viewData['blogPagination'] = $this->pagination->create_links();
        }

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('blog_front_module'),
            'keywords' => $this->lang->line('blog_front_module'),
            'description' => $this->lang->line('blog_front_module'),
        ];

        self::$viewData['blog'] = $this->blog->getAll(true, 5, $offset);

        $this->load->fronttheme('blog/lists', self::$viewData);
    }

    public function detail()
    {
        $slug = $this->uri->segment(2);
        if (empty($slug) || !$blog = $this->blog->getBySlug($slug)) {
            show_404();
            exit();
        }

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('blog_front_module'),
            'keywords' => $this->lang->line('blog_front_module'),
            'description' => $this->lang->line('blog_front_module'),
        ];

        self::$viewData['blog'] = $blog;

        $this->load->fronttheme('blog/detail', self::$viewData);
    }
}
