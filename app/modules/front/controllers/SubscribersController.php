<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 17/06/2017
 * Time: 11:22
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

/**
 * Class SubscribersController
 */
class SubscribersController extends Front_Controller
{
    /**
     * AboutController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('newsletter/subscribersModel', 'subscribers');
    }

    public function add()
    {
        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            //form validation

            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $currentUrl = $this->input->post('current_url');

            if ($this->form_validation->run() === false) {
                // validation failed
                $this->session->set_alert('error', 'Sorry! You will have to enter a valid email address. ');
                redirect($currentUrl.'/#c-subscribe');

            } else {
                // validation success

                // Update Information in database
                $email = $this->input->post('email');
                $insertData = [
                    'email' => $email,
                    'status' => 1
                ];
                if($this->subscribers->add($insertData)) {
                    $this->send_email($email);
                    $this->session->set_alert('success', 'Thank you for subscribing our newsletter. We will keep in touch.');
                } else {
                    $this->session->set_alert('error', 'Sorry! You will have to subscribe later. ');
                }
                redirect($currentUrl.'/#c-subscribe');
            }
        }
    }

    public function send_email($receiver)
    {
        $ci = get_instance();
        $ci->load->library('email');
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://smtp.gmail.com";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = "tomandjerry6636@gmail.com";
        $config['smtp_pass'] = "halamadrid123";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";

        $ci->email->initialize($config);

        $ci->email->from('tomandjerry6636@gmail.com', 'manjul');
        $ci->email->to($receiver);
        $ci->email->subject('NewsLetter Subscription');
        $ci->email->message('Thank you for subscribing NewsLetter of Swyet Swarna Investment. You will frequently get new updates.');

        if($ci->email->send()){
            echo "your email was sent";
        }else{
            show_error($this->email->print_debugger());
        }
    }
}