<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 02/08/2017
 * Time: 14:56
 */

(defined('BASEPATH')) || exit('No direct script access allowed');

/**
 * Class VideosController
 */
class VideosController extends Front_Controller
{
    /**
     * VideosController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('gallery/albumModel', 'album');

        $this->load->model('gallery/filesModel', 'video');
    }

    /**
     * Video page
     */

    public function index(){
        $this->album();
    }

    public function album()
    {
        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('video_front_module'),
            'keywords' => $this->lang->line('video_front_module'),
            'description' => $this->lang->line('video_front_module'),
        ];

        self::$viewData['albums'] = $this->album->getAll(true, null, 0, 2);

        $this->load->fronttheme('videos/albumlist', self::$viewData);
    }

    public function lists()
    {
        $slug = $this->uri->segment(3);
        if (empty($slug) || !$album = $this->album->getBySlug($slug)) {
            show_404();
            exit();
        }
        self::$viewData['pageInfo'] = (object) [
            'title'       => $this->lang->line('video_gallery_front_module'),
            'keywords'    => $this->lang->line('video_gallery_front_module'),
            'description' => $this->lang->line('video_gallery_front_module'),
        ];


        self::$viewData['album'] = $album;
        $albumId = $album->id;

        self::$viewData['latestVideo'] = $this->video->getLatestFile($albumId, true, 1 );
        self::$viewData['videos'] = $this->video->getAll($albumId, true);

        $this->load->fronttheme('videos/videolist', self::$viewData);
    }

    public function detail()
    {
        $albumSlug = $this->uri->segment(3);
        $videoSlug = $this->uri->segment(4);
        if (empty($albumSlug) || !$album = $this->album->getBySlug($albumSlug)) {
            show_404();
            exit();
        }
        if (empty($videoSlug) || !$video = $this->video->getBySlug($videoSlug)) {
            show_404();
            exit();
        }

        self::$viewData['pageInfo'] = (object) [
            'title'       => $this->lang->line('video_gallery_front_module'),
            'keywords'    => $this->lang->line('video_gallery_front_module'),
            'description' => $this->lang->line('video_gallery_front_module'),
        ];



        self::$viewData['album'] = $album;
        $albumId = $album->id;
        self::$viewData['videoSlug'] = $videoSlug;
        self::$viewData['videos'] = $this->video->getAll($albumId, true);
        self::$viewData['video'] = $video;
        $this->load->fronttheme('videos/video', self::$viewData);
    }






}