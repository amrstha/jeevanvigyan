<?php


(defined('BASEPATH')) || exit('No direct script access allowed');

/**
 * Class ContactController
 */
class FaqController extends Front_Controller
{
    /**
     * FAQController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('faq_front_module'),
            'keywords' => $this->lang->line('faq_front_module'),
            'description' => $this->lang->line('faq_front_module'),
        ];
        $this->load->fronttheme('faq/lists', self::$viewData);
    }

}
