<?php

/**
 * Created by PhpStorm.
 * User: prabesh
 * Date: 5/20/17
 * Time: 11:35 AM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

/**
 * Class MeditationController
 */
class MeditationController extends Front_Controller
{
    /**
     * ContactController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('meditation/MeditationModel', 'meditation');
        $this->load->model('meditation/MeditationDetailModel', 'meditationDetail');
    }

    public function index($offset = 0)
    {

        // Pagination
        $config = pagination(base_url('meditation/index'), count($this->meditation->getAll(true)), 10);
        if($config != NULL) {
            $this->pagination->initialize($config);
            self::$viewData['meditationPagination'] = $this->pagination->create_links();
        }

        self::$viewData['pageInfo'] = (object) [
            'title'       => $this->lang->line('meditation_front_module'),
            'keywords'    => $this->lang->line('meditation_front_module'),
            'description' => $this->lang->line('meditation_front_module'),
        ];

         
        self::$viewData['meditations'] = $this->meditation->getAll(true, 10, $offset);
        $this->load->fronttheme('meditation/lists', self::$viewData);

    }

    public function view()
    {
        $id = Utility::getIdFromUri(3);
        if ( $id === false ) {
            redirect(front_url('meditation'));
        }

        self::$viewData['pageInfo'] = (object) [
            'title'       => $this->lang->line('meditation_details_front_module'),
            'keywords'    => $this->lang->line('meditation_details_front_module'),
            'description' => $this->lang->line('meditation_details_front_module'),
        ];

        self::$viewData['meditation'] = $this->meditation->getById($id);
        self::$viewData['meditationDetails'] = $this->meditationDetail->getAll($id);
        $this->load->fronttheme('meditation/view', self::$viewData);
    }

}
