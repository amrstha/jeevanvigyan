<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/25/17
 * Time: 4:01 PM
 */
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php echo validation_errors(); ?>
        <?php
        $attributes = [
            'class' => 'form-horizontal',
            'role'  => 'form',
            'name'  => 'article_form',
            'id'    => 'article_form',
            'enctype'=>'multipart/form-data'
        ];
        $action = admin_url('about/save');
        echo form_open($action, $attributes);
        ?>
        <div class="col-sm-12">

            <div class="form-group">
                <label class="col-sm-2 control-label" for="content">Introduction In Home Page: <span class="text-red">*</span></label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-12">
                            <textarea class="form-control" name="home_intro" id="home_intro" placeholder="write an small introduction of organization in home page"><?=set_value('home_intro', ($aboutus) ? $aboutus->home_intro : '')?></textarea>
                        </div>
                    </div>
                    <?=form_error('content', '<span class="form-error">', '</span>')?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="content">Introduction In Home Page (In Nepali): <span class="text-red">*</span></label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-12">
                            <textarea class="form-control" name="home_intro_np" id="home_intro_np" placeholder="write an small introduction of organization in home page (In Nepali)"><?=set_value('home_intro_np', ($aboutus) ? $aboutus->home_intro_np : '')?></textarea>
                        </div>
                    </div>
                    <?=form_error('content', '<span class="form-error">', '</span>')?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="content">About Us Page Description: <span class="text-red">*</span></label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-12">
                            <textarea class="form-control" name="about_intro" id="about_intro" placeholder="write full description of organization in about us page"><?=set_value('about_intro', ($aboutus) ? $aboutus->about_intro : '')?></textarea>
                        </div>
                    </div>
                    <?=form_error('content', '<span class="form-error">', '</span>')?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="content">About Us Page Description (In Nepali): <span class="text-red">*</span></label>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-12">
                            <textarea class="form-control" name="about_intro_np" id="about_intro_np" placeholder="write full description of organization in about us page (In Nepali)"><?=set_value('about_intro_np', ($aboutus) ? $aboutus->about_intro_np : '')?></textarea>
                        </div>
                    </div>
                    <?=form_error('content', '<span class="form-error">', '</span>')?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label" for="content">About Us Image: <span class="text-red">*</span> </label>

                <div class="col-sm-6">
                    <div class="row">
                        <input type="file" name="image" accept="image/*">
                    </div>
                </div>
                <?php if($aboutus): ?>
                    <div class="field-group col-sm-4">
                        <img src="<?php echo base_url(Utility::getUploadDir('about').$aboutus->image) ?>" alt="" class="img-responsive" height="100px" width="100px" />
                        <input type="hidden" name="prevImageName" id="prevImageName" value="<?php echo $aboutus->image ?>">
                        <br/><br/>
                    </div>
                <?php endif ?>
            </div>

            <div class="form-group has-feedback">
                <div class="col-sm-10 col-sm-offset-2">
                    <input type="submit" class="btn btn-primary" value="Update" id="submit_btn">
                    <a href="<?=admin_url('about')?>" class="btn btn-default">Cancel</a>
                </div>
            </div>

        </div>

        <?php echo form_close() ?>

    </div>
</div>

