<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/23/17
 * Time: 4:04 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class AboutModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function save($insertData)
    {
        $data = $this->db->from(TBL_ABOUT_US)->count_all_results();
        if ($data > 0) {
            // update
            $query = $this->db->get(TBL_ABOUT_US);
            $row = $query->row();
            $id = $row->id;
            return $this->db->where('id', $id)->update(TBL_ABOUT_US, $insertData);
        }

        // create
        return $this->db->insert(TBL_ABOUT_US, $insertData);

    }
    public function getFirstRow()
    {
        $data = $this->db->from(TBL_ABOUT_US)->count_all_results();
        if ($data > 0) {
            $query = $this->db->get(TBL_ABOUT_US);
            $row = $query->row();
            $id = $row->id;
            $res = $this->db->where('id', $id)->get(TBL_ABOUT_US);
            return $res->row();
        } else {
            return false;
        }
    }
}