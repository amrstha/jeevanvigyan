<?php

(defined('BASEPATH')) || exit('No direct script access allowed');

class AboutController extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('about_module')
        ];
        $this->breadcrumb->append('About', admin_url('about'));

        // load model
        $this->load->model('aboutModel', 'about');
    }

    public function index()
    {
        $this->breadcrumb->append('Save', admin_url('about/save'));

        self::$viewData['pageDetail'] = (object)[
            'title' => 'About: Save About in home page and about page',
            'subTitle' => 'About Us Form'
        ];
        self::$viewData['aboutus'] = $this->about->getFirstRow();

        $this->load->admintheme('form', self::$viewData);
    }

    public function save()
    {
        $this->form_validation->set_rules('home_intro', 'Home Introduction', 'required');
        $this->form_validation->set_rules('home_intro_np', 'Home Introduction (In Nepali)', 'required');
        $this->form_validation->set_rules('about_intro', 'About Us Introduction', 'required');
        $this->form_validation->set_rules('about_intro_np', 'About Us Introduction (In Nepali)', 'required');

        if ($this->form_validation->run() === false) {
            // load form

            self::$viewData['pageDetail'] = (object)[
                'title' => 'About: Save About in home page and about page',
                'subTitle' => 'About Us Form'
            ];
            self::$viewData['aboutus'] = $this->about->getFirstRow();

            $this->load->admintheme('form', self::$viewData);

        } else {
            // validation success

            $image = null;

            $prevImageName = $this->input->post('prevImageName');
            if ( !empty($_FILES['image']['tmp_name']) ) {
                $image = $image = Utility::upload_image("about", "image");
                // delete previous image
                $imageFile = Utility::getUploadDir('about') . $prevImageName;
                if (file_exists($imageFile)) {
                    @unlink($imageFile);
                }
            } else {
                $image = $prevImageName;
            }

            // Update Information in database
            $insertData = [
                'home_intro' => $this->input->post('home_intro'),
                'home_intro_np' => $this->input->post('home_intro_np'),
                'about_intro' => $this->input->post('about_intro'),
                'about_intro_np' => $this->input->post('about_intro_np'),
                'image' => $image
            ];


            if ($this->about->save($insertData)) {
                $this->session->set_notification('success', $this->lang->line('success_added'));
            } else {
                $this->session->set_notification('error', $this->lang->line('error_added'));
            }


            admin_redirect('about');
        }
//        }
    }
}