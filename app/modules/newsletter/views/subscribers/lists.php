<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 17/06/2017
 * Time: 10:36
 */
?>
<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#active-tab" id="active-tab-btn" role="tab" data-toggle="tab" aria-expanded="true">
                        Active
                    </a>
                </li>
                <li role="presentation" class="">
                    <a href="#inactive-tab" role="tab" id="inactive-tab-btn" data-toggle="tab" aria-expanded="false">
                        Inactive
                    </a>
                </li>
            </ul>

            <div id="userListTab" class="tab-content">
                <!-- Active Users -->
                <div role="tabpanel" class="tab-pane fade active in" id="active-tab" aria-labelledby="active-tab-btn">
                    <div class="table-responsive">
                        <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped data-table">
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Email</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $cnt = 1;
                            foreach ($subscribers as $subscriber): if($subscriber->status != 1) continue; ?>
                                <tr>
                                    <td><?=$cnt++.'.'?></td>
                                    <td><?=$subscriber->email?></td>
                                    <td nowrap="">
                                            <a href="<?=admin_url('newsletter/subscribers/changestatus/'.url_encrypt($subscriber->id))?>" class="btn btn-xs btn-danger tooltip-danger confirmDiag" data-toggle="tooltip" data-confirmbtn="Yes, deactivate this user!" data-msg="Are you sure?" title="de-activate">
                                                <span class="fa fa-remove"></span>
                                            </a>
                                    </td>
                                </tr>
                            <?php endforeach;
                            if ($cnt == 1): ?>
                                <tr>
                                    <td colspan="8">No record found!</td>
                                </tr>
                            <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /Active Users -->

                <!-- In-active Users -->
                <div role="tabpanel" class="tab-pane fade" id="inactive-tab" aria-labelledby="inactive-tab-btn">
                    <div class="table-responsive">
                        <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped <?=($cnt == count($subscribers)+1 ? '' : 'data-table')?>">
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Email</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $cnt = 1;
                            foreach ($subscribers as $subscriber): if($subscriber->status == 1) continue; ?>
                                <tr>
                                    <td><?=$cnt++.'.'?></td>
                                    <td><?=$subscriber->email?></td>
                                    <td nowrap="">
                                        <a href="<?=admin_url('newsletter/subscribers/changestatus/'.url_encrypt($subscriber->id))?>" class="btn btn-xs btn-danger tooltip-danger confirmDiag" data-toggle="tooltip" data-confirmbtn="Yes, re-activate this user!" data-msg="Are you sure?" title="re-activate">
                                            <span class="fa fa-refresh"></span>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach;
                            if ($cnt == 1): ?>
                                <tr>
                                    <td colspan="8">No record found!</td>
                                </tr>
                            <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /In-active Users -->
            </div>
        </div>

    </div>
</div>
