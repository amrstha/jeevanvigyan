<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 17/06/2017
 * Time: 10:49
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class SubscribersModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add($data)
    {
        $this->db->insert(TBL_SUBSCRIBERS, $data);
        return $this->db->affected_rows() > 0;
    }

    public function changestatus($id)
    {
        $this->db->query("UPDATE {$this->db->dbprefix(TBL_SUBSCRIBERS)} SET status = 1 - status WHERE id = {$id}");
        return $this->db->affected_rows() > 0;
    }

    public function delete($id)
    {
        $this->db->where('id', $id)->delete(TBL_SUBSCRIBERS);
        return $this->db->affected_rows() > 0;
    }

    public function getAll($activeOnly = false, $limit = null, $offset = 0)
    {
        if ( $activeOnly ) {
            $this->db->where('status', 1);
        }

        if (!is_null($limit)) {
            $this->db->limit($limit, $offset);
        }

        return $this->db->order_by('created_at')->get(TBL_SUBSCRIBERS)->result();
    }

    public function getById($id)
    {
        $data = $this->db->where('id', $id)->get(TBL_SUBSCRIBERS);
        return $data->num_rows() > 0 ? $data->row() : false;
    }
}