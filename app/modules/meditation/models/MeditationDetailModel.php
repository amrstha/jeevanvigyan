<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MeditationDetailModel extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	//function to add meditation
	public function add($data)
	{
		$this->db->insert(TBL_MEDITATION_DETAILS, $data);
		return $this->db->affected_rows() > 0;
	}
	
	//function to update meditation
	
	
	public function getAll($med_id = null, $join = true, $limit = null, $offset = 0)
	{
		
		if (!is_null($med_id)) {
			$this->db->where('med_id', $med_id);
		}
	
		if (!is_null($limit)) {
			$this->db->limit($limit, $offset);
		}
		
		if($join == true){
			$this->db->select('MD.id,MD.med_id,MD.type,MD.value,MD.created_at,M.title,M.description');
			$this->db->from(TBL_MEDITATION_DETAILS.' AS MD');// I use aliasing make joins easier
			$this->db->join(TBL_MEDITATION.' AS M', 'MD.med_id = M.id', 'INNER');
			return $this->db->order_by('created_at','DESC')->get()->result();
		}else{
			return $this->db->order_by('created_at','DESC')->get(TBL_MEDITATION_DETAILS)->result();
		}
		
	}
	
	public function delete($id)
	{
		$prevData = $this->getById($id);
		$this->deleteImage($prevData->value);
		$this->db->where('id', $id)->delete(TBL_MEDITATION_DETAILS);
		return $this->db->affected_rows() > 0;
	}
	
	public function deleteImage($imageFile)
	{
		$imageFile = Utility::getUploadDir('meditation').$imageFile;
		if (file_exists($imageFile)) {
			@unlink($imageFile);
			return true;
		}
		return false;
	}
	//function to get meditation by id
	public function getById($id){
		
		$data = $this->db->where('id',$id)->get(TBL_MEDITATION_DETAILS);
		return $data->num_rows() > 0 ? $data->row() : false;
	}
	//function to change status of meditation
	
}