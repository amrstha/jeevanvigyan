<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 02/08/2017
 * Time: 10:50
 */

(defined('BASEPATH')) || exit('No direct script access allowed');

class ProgramModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add($data)
    {
        $data['slug'] = Utility::generateSlug($data['title'], TBL_PROGRAM, 'slug');
        $this->db->insert(TBL_PROGRAM, $data);
        return $this->db->affected_rows() > 0;
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id)->update(TBL_PROGRAM, $data);
        return $this->db->affected_rows() > 0;
    }

    public function status($id)
    {
        $this->db->query("UPDATE {$this->db->dbprefix(TBL_PROGRAM)} SET status = 1 - status WHERE id = {$id}");
        return $this->db->affected_rows() > 0;
    }

    public function delete($id)
    {
        $prevData = $this->getById($id);
        $this->deleteImage($prevData->image);
        $this->db->where('id', $id)->delete(TBL_PROGRAM);
        return $this->db->affected_rows() > 0;
    }

    public function deleteImage($imageFile)
    {
        $imageFile = Utility::getUploadDir('program') . $imageFile;
        if (file_exists($imageFile)) {
            @unlink($imageFile);
            return true;
        }
        return false;
    }

    public function getAll($activeOnly = false, $limit = null, $offset = 0)
    {
        if ($activeOnly) {
            $this->db->where('status', 1);
        }

        if (!is_null($limit)) {
            $this->db->limit($limit, $offset);
        }

        return $this->db->order_by('status, created_at')->get(TBL_PROGRAM)->result();
    }

    public function getById($id)
    {
        $data = $this->db->where('id', $id)->get(TBL_PROGRAM);
        return $data->num_rows() > 0 ? $data->row() : false;
    }

    public function getBySlug($slug)
    {
        $data = $this->db->where('slug', $slug)->get(TBL_PROGRAM);
        return $data->num_rows() > 0 ? $data->row() : false;
    }
}