<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 02/08/2017
 * Time: 10:56
 */
?>


<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="pull-right">
            <a href="<?=admin_url('program/add')?>" class="btn btn-success btn-xs tooltip-success loadForm" data-toggle="tooltip" data-successbtn="Add" data-addclass="bootbox-modal-sm" title="Add New Slider">
                <i class="fa fa-plus"></i>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>S.N.</th>
                    <th>Status</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Image</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php $cnt = 1; foreach($programmes as $program): ?>
                    <tr>
                        <td><?=$cnt++?></td>
                        <td>
                            <a href="<?=admin_url('program/status/'.url_encrypt($program->id))?>" class="tooltip-<?=($program->status == 1 ? 'success' : 'danger')?>" data-toggle="tooltip" title="<?=($program->status == 1 ? 'un-publish' : 'publish')?> it">
                                <span class="label label-<?=($program->status == 1 ? 'success' : 'danger')?>"><?=($program->status == 1 ? 'Published' : 'Not Published')?></span>
                            </a>
                        </td>
                        <td><?=$program->title?></td>
                        <td><?=word_limiter($program->description,10)?></td>
                        <td>
                            <img src="<?=base_url(Utility::getUploadDir('program').$program->image)?>" class="img-responsive" height="50px" width="50px">
                        </td>
                        <td nowrap="">
                            <a href="<?=admin_url('program/edit/'.url_encrypt($program->id))?>" class="btn btn-xs btn-info tooltip-info loadForm" data-toggle="tooltip" data-successbtn="Update" data-addclass="bootbox-modal-sm" title="edit">
                                <span class="fa fa-edit"></span>
                            </a>

                            <a href="<?=admin_url('program/delete/'.url_encrypt($program->id))?>" class="btn btn-xs btn-danger tooltip-danger confirmDiag" data-toggle="tooltip" data-confirmbtn="Yes, delete it!" data-msg="Are you sure? Once confirmed it cannot be undone." title="delete">
                                <span class="fa fa-remove"></span>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php if(count($programmes) == 0): ?>
                    <tr>
                        <td colspan="7">
                            No programmes has been added yet.
                        </td>
                    </tr>
                <?php endif ?>
                </tbody>
            </table>
        </div>

    </div>
</div>

