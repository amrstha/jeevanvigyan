<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 02/08/2017
 * Time: 10:35
 */

(defined('BASEPATH')) || exit('No direct script access allowed');

class ProgramController extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('program_module')
        ];
        $this->breadcrumb->append('Program', admin_url('program'));

        // load program model
        $this->load->model('programModel', 'program');
    }

    public function lists()
    {
        $this->breadcrumb->append('List', admin_url('program'));
        self::$viewData['pageDetail'] = (object)[
            'title' => 'Programmes: List',
            'subTitle' => 'List of all programmes.'
        ];

        self::$viewData['programmes'] = $this->program->getAll();

        $this->load->admintheme('lists', self::$viewData);
    }

    public function add()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $response = [];
        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('title_np', 'Title (in Nepali)', 'required');
            $this->form_validation->set_rules('description', 'Description');
            $this->form_validation->set_rules('description_np', 'Description (in Nepali)');

            if ($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Upload image first
                $image = $this->upload_image();

                if ($image !== false) {
                    // Update Information in database
                    $insertData = [
                        'title' => $this->input->post('title'),
                        'title_np' => $this->input->post('title_np'),
                        'description' => $this->input->post('description'),
                        'description_np' => $this->input->post('description_np'),
                        'video' => $this->input->post('video'),
                        'image' => $image
                    ];

                    if ($this->program->add($insertData)) {
                        $this->session->set_notification('success', $this->lang->line('success_added'));
                    } else {
                        $this->session->set_notification('error', $this->lang->line('error_added'));
                    }
                }

                $response = [
                    'status' => 'success',
                    'data' => NULL,
                    'message' => 'success'
                ];
            }

        } else {
            // load form
            $form = $this->load->view('form', self::$viewData, true);
            $response = [
                'status' => 'success',
                'data' => null,
                'message' => $form
            ];
        }

        $this->output->json($response);
    }

    private function upload_image()
    {
        $config['upload_path'] = Utility::getUploadDir('program');
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '10000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('image')) {
            $this->session->set_notification('error', $this->upload->display_errors());
            return false;
        } else {
            $data = ['upload_data' => $this->upload->data()];
            return $data['upload_data']['file_name'];
        }
    }

    public function edit()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $id = Utility::getIdFromUri(4);
        if ($id === false) {
            $this->output->json([
                'status' => 'error',
                'data' => NULL,
                'message' => $this->lang->line('invalid_request')
            ]);
        }

        $response = [];
        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('title_np', 'Title (in Nepali)', 'required');
            $this->form_validation->set_rules('description', 'Description');
            $this->form_validation->set_rules('description_np', 'Description (in Nepali)');

            if ($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Upload image first
                $prevImageName = $this->input->post('prevImageName');
                if (!empty($_FILES['image']['tmp_name'])) {
                    $image = $this->upload_image();
                    // delete previous image
                    $this->program->deleteImage($prevImageName);
                } else {
                    $image = $prevImageName;
                }

                if ($image !== false) {
                    // Update Information in database
                    $updateData = [
                        'title' => $this->input->post('title'),
                        'title_np' => $this->input->post('title_np'),
                        'description' => $this->input->post('description'),
                        'description_np' => $this->input->post('description_np'),
                        'video' => $this->input->post('video'),
                        'image' => $image
                    ];

                    if ($this->program->update($id, $updateData)) {
                        $this->session->set_notification('success', $this->lang->line('success_updated'));
                    } else {
                        $this->session->set_notification('error', $this->lang->line('nothing_updated'));
                    }
                }

                $response = [
                    'status' => 'success',
                    'data' => NULL,
                    'message' => 'success'
                ];
            }

        } else {
            // load form
            self::$viewData['edit'] = $this->program->getById($id);
            $form = $this->load->view('form', self::$viewData, true);
            $response = [
                'status' => 'success',
                'data' => null,
                'message' => $form
            ];
        }

        $this->output->json($response);
    }

    public function delete()
    {
        $id = Utility::getIdFromUri(4);

        if ($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if (!$this->program->delete($id)) {
                $this->session->set_notification('error', $this->lang->line('error_delete'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_delete'));
            }
        }
        admin_redirect('program/lists', 'refresh');
    }

    public function status()
    {
        $id = Utility::getIdFromUri(4);

        if ($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if (!$this->program->changestatus($id)) {
                $this->session->set_notification('error', $this->lang->line('nothing_updated'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_updated'));
            }
        }
        admin_redirect('program/lists', 'refresh');
    }
}