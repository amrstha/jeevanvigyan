<?php

/**
 * Created by PhpStorm.
 * User: manjul
 * Date: 04/06/17
 * Time: 1:36 PM
 */

(defined('BASEPATH')) || exit('No direct script access allowed');

class ContactController extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('contact_module')
        ];
        $this->breadcrumb->append('Contact', admin_url('messages'));

        //load contact model
        $this->load->model('ContactModel','contact');
    }

    public function index()
    {
        $this->messages();
    }

    // Listing all messages from contact us form
    public function messages()
    {
        $this->breadcrumb->append('Messages', admin_url('contact'));

        self::$viewData['pageDetail'] = (object)[
            'title'     => 'Messages',
            'subTitle'  => 'List of messages from contact us'
        ];

        self::$viewData['messages'] = $this->contact->getAll();        
        self::$viewData['unreadCount'] = $this->contact->getAll(true);
        $this->load->admintheme('messages',self::$viewData);
    }

    public function message_details()
    {   
        
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $id = Utility::getIdFromUri(4);
        if ($id === false) {
            $this->output->json([
                'status'    => 'error',
                'data'      => NULL,
                'message'   => $this->lang->line('invalid_request')
            ]);
        }
        
        $this->contact->isRead($id);
            // load form
            self::$viewData['message'] = $this->contact->getById($id);
            $form = $this->load->view('message_details', self::$viewData, true);
            $response = [
                'status'    => 'success',
                'data'      => null,
                'message'   => $form
            ];
        

        $this->output->json($response);

    }

    public function delete()
    {
        $id = Utility::getIdFromUri(4);

        if($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if(!$this->contact->delete($id)){
                $this->session->set_notification('error', $this->lang->line('error_delete'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_delete'));
            }
        }
        admin_redirect('contact/messages', 'refresh');
    }

    public function isRead()
    {
        $id = Utility::getIdFromUri(3);

        if($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if( !$this->contact->isRead($id)) {
                $this->session->set_notification('error', $this->lang->line('nothing_updated'));
            } else {
                $this->session->set_notification('sucesss', $this->lang->line('success_updated'));
            }
        }

        admin_redirect('contact/messages','refresh');
    }

}