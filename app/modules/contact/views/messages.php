<?php
/**
 * Created by PhpStorm.
 * User: manjul
 * Date: 04/06/17
 * Time: 4:40 PM
 */
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>S.N.</th>
                    <th>Subject</th>
                    <th>Message</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php $cnt = 1; foreach($messages as $message): ?>
                    <div>
                        <tr class="<?=($message->is_read == 0?'unread-message':'')?>">
                            <td><?=$cnt++?></td>
                            <td><?=$message->subject?></td>
                            <td><?=word_limiter($message->message,15)?></td>
                            <td nowrap="">
                                <a href="<?=admin_url('contact/delete/'.url_encrypt($message->id))?>" class="btn btn-xs btn-danger tooltip-danger confirmDiag" data-toggle="tooltip" data-confirmbtn="Yes, delete it!" data-msg="Are you sure? Once confirmed it cannot be undone." title="delete">
                                    <span class="fa fa-remove"></span>
                                </a>
                                <a href="<?=admin_url('contact/message_details/'.url_encrypt($message->id))?>" class="btn btn-xs btn-info tooltip-info loadForm" data-toggle="tooltip" data-successbtn="" data-addclass="bootbox-modal-sm" title="view">
                                        <span class="fa fa-eye"></span>
                                </a>
                            </td>
                        </tr>
                    </div>
                <?php endforeach; ?>
                <?php if(count($messages) == 0): ?>
                    <tr>
                        <td colspan="6">
                            No messages has received yet.
                        </td>
                    </tr>
                <?php endif ?>
                </tbody>
            </table>
        </div>

    </div>
</div>

