<?php
/**
 * Created by PhpStorm.
 * User: manjul
 * Date: 06/06/17
 * Time: 4:38 PM
 */
?>

<div class="row">
    <div class="col-sm-12">

        <?php
        $attributes = [
            'class' => 'form-horizontal',
            'role'  => 'form',
            'name'  => 'popup_form',
            'id'    => 'popup_form'
        ];
        $action = admin_url('menu/social/'.(isset($edit) ? 'edit/'.url_encrypt($edit->id) : 'add'));
        echo form_open($action, $attributes);
        ?>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="title">Name: <span class="text-red">*</span></label>
            <div class="col-sm-10">
                <input type="text" name="name" id="name" class="form-control required" placeholder="Name" value="<?=(isset($edit) ? $edit->name : '')?>" />
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="title_np">Title: <span class="text-red">*</span></label>
            <div class="col-sm-10">
                <input type="text" name="title" id="title" class="form-control required" placeholder="Title" value="<?=(isset($edit) ? $edit->title : '')?>" />
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="order_by">Position: <span class="text-red">*</span></label>
            <div class="col-sm-10">
                <input type="text" name="position" id="position" class="form-control required" placeholder="Postion" value="<?=(isset($edit) ? $edit->position : '0')?>" />
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="title_np">Link: <span class="text-red">*</span></label>
            <div class="col-sm-10">
                <input type="text" name="link" id="link" class="form-control required" placeholder="Link" value="<?=(isset($edit) ? $edit->link : '')?>" />
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-12 pull-left" for="image">Icon (JPG,PNG,ICO format only) - Maximum: 50KB [250px * 250px]: <span class="text-red">*</span></label>
            <?php if(isset($edit)): ?>
                <div class="field-group col-sm-4">
                    <img src="<?php echo base_url(Utility::getUploadDir('menu_social').$edit->icon) ?>" alt="$edit->title" class="img-responsive" />
                    <input type="hidden" name="prevIconName" id="prevIconName" value="<?php echo $edit->icon ?>">
                    <br/><br/>
                </div>
            <?php endif ?>
            <div class="col-sm-<?php echo isset($edit) ? '8' : '10 col-sm-offset-2' ?>">
                <input type="file" class="form-control <?php echo isset($edit) ? '' : 'required' ?>" name="icon" id="icon">
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>

        <?=form_close()?>

    </div>
</div>
