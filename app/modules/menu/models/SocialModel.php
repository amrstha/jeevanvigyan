<?php

/**
 * Created by PhpStorm.
 * User: manjul
 * Date: 06/06/17
 * Time: 3:12 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class SocialModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add($data)
    {
        $this->db->insert(TBL_MENU_SOCIAL, $data);
        return $this->db->affected_rows() > 0;
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id)->update(TBL_MENU_SOCIAL, $data);
        return $this->db->affected_rows() > 0;
    }

    public function changestatus($id)
    {
        $this->db->query("UPDATE {$this->db->dbprefix(TBL_MENU_SOCIAL)} SET status = 1 - status WHERE id = {$id}");
        return $this->db->affected_rows() > 0;
    }

    public function delete($id)
    {
        $prevData = $this->getById($id);
        $this->deleteImage($prevData->icon);
        $this->db->where('id', $id)->delete(TBL_MENU_SOCIAL);
        return $this->db->affected_rows() > 0;
    }

    public function deleteImage($iconFile)
    {
        $iconFile = Utility::getUploadDir('menu_social').$iconFile;
        if (file_exists($iconFile)) {
            @unlink($iconFile);
            return true;
        }
        return false;
    }

    public function getAll($activeOnly = false, $limit = null, $offset = 0)
    {
        if ($activeOnly) {
            $this->db->where('status', 1);
        }

        if (!is_null($limit)) {
            $this->db->limit($limit, $offset);
        }

        return $this->db->order_by('position')->get(TBL_MENU_SOCIAL)->result();
    }

    public function getById($id)
    {
        $data = $this->db->where('id', $id)->get(TBL_MENU_SOCIAL);
        return $data->num_rows() > 0 ? $data->row() : false;
    }
}