<?php

/**
 * Created by PhpStorm.
 * User: manjul
 * Date: 06/06/17
 * Time: 3:18 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class SocialController extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('menu_module')
        ];
        $this->breadcrumb->append('Menu Management', admin_url('menu/social'));

        // load slider model
        $this->load->model('socialModel', 'social');
    }

    public function index()
    {
        $this->breadcrumb->append('Social', admin_url('menu/social'));
        self::$viewData['pageDetail'] = (object)[
            'title'     => 'Social Menu: List',
            'subTitle'  => 'List of all social menu.'
        ];

        self::$viewData['socialmenus'] = $this->social->getAll();

        $this->load->admintheme('social/lists', self::$viewData);
    }

    public function add()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $response = [];
        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('link', 'Link', 'required');
            $this->form_validation->set_rules('position', 'Position', 'required|numeric');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Upload icon first
                $icon = $this->upload_icon();

                if($icon !== false) {
                    // Update Information in database
                    $insertData = [
                        'name'          => $this->input->post('name'),
                        'title' 		=> $this->input->post('title'),
                        'link'          => $this->input->post('link'),
                        'position' 	    => $this->input->post('position'),
                        'icon' 		    => $icon,
                        'status'        => 1
                    ];

                    if($this->social->add($insertData)) {
                        $this->session->set_notification('success', $this->lang->line('success_added'));
                    } else {
                        $this->session->set_notification('error', $this->lang->line('error_added'));
                    }
                }

                $response = [
                    'status' 	=> 'success',
                    'data'		=> NULL,
                    'message'	=> 'success'
                ];
            }

        } else {
            // load form
            $form = $this->load->view('social/form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function edit()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $id = Utility::getIdFromUri(5);
        if ($id === false) {
            $this->output->json([
                'status' 	=> 'error',
                'data'		=> NULL,
                'message'	=> $this->lang->line('invalid_request')
            ]);
        }

        $response = [];
        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('link', 'Link', 'required');
            $this->form_validation->set_rules('position', 'Position', 'required|numeric');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Upload icon first
                $prevIconName = $this->input->post('prevIconName');
                if( ! empty($_FILES['icon']['tmp_name']) ) {
                    $icon = $this->upload_icon();
                    // delete previous icon
                    $this->social->deleteImage($prevIconName);
                } else {
                    $icon = $prevIconName;
                }

                if($icon !== false) {
                    // Update Information in database
                    $updateData = [
                        'name'          => $this->input->post('name'),
                        'title'         => $this->input->post('title'),
                        'link'          => $this->input->post('link'),
                        'position'      => $this->input->post('position'),
                        'icon'          => $icon,
                        'status'        => 1
                    ];

                    if($this->social->update($id, $updateData)) {
                        $this->session->set_notification('success', $this->lang->line('success_updated'));
                    } else {
                        $this->session->set_notification('error', $this->lang->line('nothing_updated'));
                    }
                }

                $response = [
                    'status' 	=> 'success',
                    'data'		=> NULL,
                    'message'	=> 'success'
                ];
            }

        } else {
            // load form
            self::$viewData['edit'] = $this->social->getById($id);
            $form = $this->load->view('social/form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    private function upload_icon()
    {
        $config['upload_path'] = Utility::getUploadDir('menu_social');
        $config['allowed_types'] = 'jpg|png|jpeg|ico|icon';
        $config['max_size']	= '50';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if( ! $this->upload->do_upload('icon')) {
            $this->session->set_notification('error', $this->upload->display_errors());
            return false;
        } else {
            $data = ['upload_data' => $this->upload->data()];
            return $data['upload_data']['file_name'];
        }
    }

    public function delete()
    {
        $id = Utility::getIdFromUri(5);

        if ($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if( ! $this->social->delete($id) ) {
                $this->session->set_notification('error', $this->lang->line('error_delete'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_delete'));
            }
        }
        admin_redirect('menu/social', 'refresh');
    }

    public function changestatus()
    {
        $id = Utility::getIdFromUri(4);

        if ($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if( ! $this->social->changestatus($id) ) {
                $this->session->set_notification('error', $this->lang->line('nothing_updated'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_updated'));
            }
        }
        admin_redirect('menu/social', 'refresh');
    }
}