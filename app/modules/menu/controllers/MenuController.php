<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 5/13/17
 * Time: 10:06 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

/**
 * Class MenuController
 */
class MenuController extends Admin_Controller
{

    /**
     * MenuController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        self::$viewData['pageInfo'] = (object) [
            'title' => $this->lang->line('menu_module'),
        ];
        $this->breadcrumb->append('Menu Management', admin_url('menu'));

        // load model
        $this->load->model('MenuModel', 'menu');
    }

    /**
     *
     */
    public function index()
    {
        self::$viewData['pageDetail'] = (object)[
            'title'    => 'Menus: List',
            'subtitle' => ''
        ];

        // self::$viewData['menus'] = $this->menu->getAll();

        $this->load->admintheme('lists', self::$viewData);
    }
}
