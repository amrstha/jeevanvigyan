.<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/23/17
 * Time: 3:36 PM
 */
?>

<div class="row">
    <div class="col-sm-12">

        <?php
        $attributes = [
            'class' => 'form-horizontal',
            'role'  => 'form',
            'name'  => 'popup_form',
            'id'    => 'popup_form'
        ];
        $action = admin_url('pages/category/'.(isset($edit) ? 'edit/'.url_encrypt($edit->id) : 'add'));
        echo form_open($action, $attributes);
        ?>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="name">Name: <span class="text-red">*</span></label>
            <div class="col-sm-10">
                <input type="text" name="name" id="name" class="form-control required" placeholder="Name" value="<?=(isset($edit) ? $edit->name : '')?>" />
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="name_np">Name (in Nepali): <span class="text-red">*</span></label>
            <div class="col-sm-10">
                <input type="text" name="name_np" id="name_np" class="form-control required" placeholder="Name (in Nepali)" value="<?=(isset($edit) ? $edit->name_np : '')?>" />
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="description">Description:</label>
            <div class="col-sm-10">
                <textarea class="form-control" name="description" id="description" placeholder="Description"><?=(isset($edit) ? $edit->description : '')?></textarea>
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="description_np">Description (in Nepali):</label>
            <div class="col-sm-10">
                <textarea class="form-control" name="description_np" id="description_np" placeholder="Description (in Nepali)"><?=(isset($edit) ? $edit->description_np : '')?></textarea>
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <?=form_close()?>

    </div>
</div>
