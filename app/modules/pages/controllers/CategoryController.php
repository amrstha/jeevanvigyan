<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/23/17
 * Time: 3:14 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class CategoryController extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('pages_module')
        ];
        $this->breadcrumb->append('Pages Management', admin_url('pages'));

        // load model
        $this->load->model('categoryModel', 'category');
    }

    public function index()
    {
        $this->breadcrumb->append('Category', admin_url('pages/category'));
        self::$viewData['pageDetail'] = (object)[
            'title'     => 'Page Management: Category',
            'subTitle'  => 'List of all categories.'
        ];

        self::$viewData['categories'] = $this->category->getAll();

        $this->load->admintheme('category/lists', self::$viewData);
    }

    public function add()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $response = [];
        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('name_np', 'Name (in Nepali)', 'required');
            $this->form_validation->set_rules('description', 'Description', 'prep_for_form|strip_image_tags|encode_php_tags');
            $this->form_validation->set_rules('description_np', 'Description (in Nepali)', 'prep_for_form|strip_image_tags|encode_php_tags');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Update Information in database
                $insertData = [
                    'name' 		        => $this->input->post('name'),
                    'name_np' 	        => $this->input->post('name_np'),
                    'description' 		=> $this->input->post('description'),
                    'description_np' 	=> $this->input->post('description_np'),
                ];

                if($this->category->add($insertData)) {
                    $this->session->set_notification('success', $this->lang->line('success_added'));
                } else {
                    $this->session->set_notification('error', $this->lang->line('error_added'));
                }

                $response = [
                    'status' 	=> 'success',
                    'data'		=> NULL,
                    'message'	=> 'success'
                ];
            }

        } else {
            // load form
            $form = $this->load->view('category/form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function edit()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $id = Utility::getIdFromUri(5);
        if ($id === false) {
            $this->output->json([
                'status' 	=> 'error',
                'data'		=> NULL,
                'message'	=> $this->lang->line('invalid_request')
            ]);
        }

        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('name_np', 'Name (in Nepali)', 'required');
            $this->form_validation->set_rules('description', 'Description', 'prep_for_form|strip_image_tags|encode_php_tags');
            $this->form_validation->set_rules('description_np', 'Description (in Nepali)', 'prep_for_form|strip_image_tags|encode_php_tags');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Update Information in database
                $updateData = [
                    'name' 		        => $this->input->post('name'),
                    'name_np' 	        => $this->input->post('name_np'),
                    'description' 		=> $this->input->post('description'),
                    'description_np' 	=> $this->input->post('description_np'),
                ];

                if($this->category->update($id, $updateData)) {
                    $this->session->set_notification('success', $this->lang->line('success_updated'));
                } else {
                    $this->session->set_notification('error', $this->lang->line('nothing_updated'));
                }

                $response = [
                    'status' 	=> 'success',
                    'data'		=> NULL,
                    'message'	=> 'success'
                ];
            }

        } else {
            // load form
            self::$viewData['edit'] = $this->category->getById($id);
            $form = $this->load->view('category/form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function delete()
    {
        $id = Utility::getIdFromUri(5);

        if ($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if( ! $this->category->delete($id) ) {
                $this->session->set_notification('error', $this->lang->line('error_delete'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_delete'));
            }
        }
        admin_redirect('pages/category', 'refresh');
    }

    public function undodelete()
    {
        $id = Utility::getIdFromUri(5);

        if ($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if( ! $this->category->undodelete($id) ) {
                $this->session->set_notification('error', $this->lang->line('error_updated'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_updated'));
            }
        }
        admin_redirect('pages/category', 'refresh');
    }
}