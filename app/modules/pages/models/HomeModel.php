<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/23/17
 * Time: 12:22 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function save($data)
    {
    	$prevdata = $this->get();
    	if ($prevdata === false) {
    		// insert
    		$this->db->insert(TBL_HOME_PAGE, $data);
    	} else {
    		// update
    		$this->db->where('id', $prevdata->id)->update(TBL_HOME_PAGE, $data);
    	}        
        return $this->db->affected_rows() > 0;
    }

    public function get()
    {   	
    	$data = $this->db->get(TBL_HOME_PAGE);
    	return $data->num_rows() > 0 ? $data->row() : false;
    }   
}