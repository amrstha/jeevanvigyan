<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/23/17
 * Time: 4:04 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class ArticleModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add($data)
    {
        $data['slug'] = Utility::generateSlug($data['title'], TBL_PAGES_ARTICLE, 'slug');
        $this->db->insert(TBL_PAGES_ARTICLE, $data);
        return $this->db->affected_rows() > 0;
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id)->update(TBL_PAGES_ARTICLE, $data);
        return $this->db->affected_rows() > 0;
    }

    public function status($id)
    {
        $this->db->query("UPDATE {$this->db->dbprefix(TBL_PAGES_ARTICLE)} SET status = 1 - status WHERE id = {$id}");
        return $this->db->affected_rows() > 0;
    }

    public function delete($id)
    {
        $prevData = $this->getById($id);
        $this->db->where('id', $id)->delete(TBL_PAGES_ARTICLE);
        return $this->db->affected_rows() > 0 ? $this->deleteImage($prevData->image) : false;
    }

    public function deleteImage($image)
    {
        $image = Utility::getUploadDir('pages').$image;
        if (file_exists($image)) {
            @unlink($image);
            return true;
        }
        return false;
    }

    public function getAll($activeOnly = false, $limit = null, $offset = 0)
    {
        if ($activeOnly) {
            $this->db->where('status', 1);
        }

        if (!is_null($limit)) {
            $this->db->limit($limit, $offset);
        }

        return $this->db->order_by('cat_id, pub_date, title')->get(TBL_PAGES_ARTICLE)->result();
    }

    public function getById($id)
    {
        $data = $this->db->where('id', $id)->get(TBL_PAGES_ARTICLE);
        return $data->num_rows() > 0 ? $data->row() : false;
    }

    public function getBySlug($slug)
    {
        $data = $this->db->where('slug', $slug)->get(TBL_PAGES_ARTICLE);
        return $data->num_rows() > 0 ? $data->row() : false;
    }
}