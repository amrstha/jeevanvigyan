<?php
(defined('BASEPATH')) || exit('No direct script access allowed');

class EventsController extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        self::$viewData['pageInfo'] = (object) [
            'title' => $this->lang->line('slider_module'),
        ];
        $this->breadcrumb->append('Events', admin_url('events'));

        // load slider model
        $this->load->model('EventsModel', 'events');
    }

    //Listing all the events
    public function lists()
    {
        $this->breadcrumb->append('Lists', admin_url('lists'));

        self::$viewData['pageDetail'] = (object) [
            'title'    => 'Events',
            'subTitle' => 'List of Events',
        ];
        self::$viewData['events']     = $this->events->getAll();
        $this->load->admintheme('lists', self::$viewData);
    }

    //Adding new event
    public function add()
    {
        if ( !$this->input->is_ajax_request() ) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $response = [];
        if ( strtolower($this->input->server('REQUEST_METHOD')) == 'post' ) {

            //form validation

            $this->form_validation->set_rules('title', 'Title', 'required');
            //$this->form_validation->set_rules('title_np', 'Title (in Nepali)', 'required');
            $this->form_validation->set_rules('description', 'Description', 'required');
            $this->form_validation->set_rules('date_start', 'Start date', 'required');
            $this->form_validation->set_rules('date_end', 'End date', 'required');
            //$this->form_validation->set_rules('description_np', 'Description (in Nepali)', 'required');

            if ( $this->form_validation->run() === false ) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                //uploading image  (first param as folder name and second post name)
                $image = Utility::upload_image("events", "image");

                //saving in to database
                if ( $image !== false ) {
                    // Update Information in database
                    $insertData = [
                        'title'          => $this->input->post('title'),
                        'title_np'       => $this->input->post('title_np'),
                        'date_start'     => $this->input->post('date_start'),
                        'date_end'       => $this->input->post('date_end'),
                        'description'    => $this->input->post('description'),
                        'description_np' => $this->input->post('description_np'),
                        'cover_pics'     => $image,
                        'status'         => 1,
                    ];

                    if ( $this->events->add($insertData) ) {
                        $this->session->set_notification('success', $this->lang->line('success_added'));
                    } else {
                        $this->session->set_notification('error', $this->lang->line('error_added'));
                    }
                }

                $response = [
                    'status'  => 'success',
                    'data'    => null,
                    'message' => 'success',
                ];
            }

        } else {
            // load form
            $form     = $this->load->view('form', self::$viewData, true);
            $response = [
                'status'  => 'success',
                'data'    => null,
                'message' => $form,
            ];
        }

        $this->output->json($response);
    }

    //Editing events
    public function edit()
    {
        if ( !$this->input->is_ajax_request() ) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $id = Utility::getIdFromUri(4);
        if ( $id === false ) {
            $this->output->json(
                [
                    'status'  => 'error',
                    'data'    => null,
                    'message' => $this->lang->line('invalid_request'),
                ]
            );
        }

        $response = [];
        if ( strtolower($this->input->server('REQUEST_METHOD')) == 'post' ) {

            $this->form_validation->set_rules('title', 'Title', 'required');
            //$this->form_validation->set_rules('title_np', 'Title (in Nepali)', 'required');
            $this->form_validation->set_rules('description', 'Description', 'required');
            $this->form_validation->set_rules('date_start', 'Start date', 'required');
            $this->form_validation->set_rules('date_end', 'End date', 'required');
            //$this->form_validation->set_rules('description_np', 'Description (in Nepali)', 'required');

            if ( $this->form_validation->run() === false ) {
                // validation failed
                $response = Utility::getFormError();
            } else {

                //uploading image  (first param as folder name and second post name)
                $prevImageName = $this->input->post('prevImageName');
                if ( !empty($_FILES['image']['tmp_name']) ) {
                    $image = $image = Utility::upload_image("events", "image");
                    // delete previous image
                    $this->events->deleteImage($prevImageName);
                } else {
                    $image = $prevImageName;
                }


                //saving in to database
                if ( $image !== false ) {
                    // Update Information in database
                    $updateData = [
                        'title'          => $this->input->post('title'),
                        'title_np'       => $this->input->post('title_np'),
                        'date_start'     => $this->input->post('date_start'),
                        'date_end'       => $this->input->post('date_end'),
                        'description'    => $this->input->post('description'),
                        'description_np' => $this->input->post('description_np'),
                        'cover_pics'     => $image,
                        'status'         => 1,
                    ];

                    if ( $this->events->update($id, $updateData) ) {
                        $this->session->set_notification('success', $this->lang->line('success_added'));
                    } else {
                        $this->session->set_notification('error', $this->lang->line('error_added'));
                    }
                }

                $response = [
                    'status'  => 'success',
                    'data'    => null,
                    'message' => 'success',
                ];
            }

        } else {
            // load form
            self::$viewData['edit'] = $this->events->getById($id);
            $form                   = $this->load->view('form', self::$viewData, true);
            $response               = [
                'status'  => 'success',
                'data'    => null,
                'message' => $form,
            ];
        }

        $this->output->json($response);
    }

    public function delete()
    {
        $id = Utility::getIdFromUri(4);

        if ( $id === false ) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if ( !$this->events->delete($id) ) {
                $this->session->set_notification('error', $this->lang->line('error_delete'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_delete'));
            }
        }
        admin_redirect('events/lists', 'refresh');
    }

    public function changestatus()
    {
        $id = Utility::getIdFromUri(4);

        if ( $id === false ) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if ( !$this->events->changestatus($id) ) {
                $this->session->set_notification('error', $this->lang->line('nothing_updated'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_updated'));
            }
        }
        admin_redirect('events/lists', 'refresh');
    }
}