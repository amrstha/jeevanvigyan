<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/23/17
 * Time: 12:22 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class EventsBookModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function book($event, $user)
    {
        $data = [
            'user'      => $user,
            'event'     => $event
        ];
        $this->db->insert(TBL_EVENTS_BOOK, $data);
        return $this->db->affected_rows() > 0;
    }

    public function unbook($event, $user)
    {
        $data = [
            'user'      => $user,
            'event'     => $event
        ];
        $this->db->where($data)->update(TBL_EVENTS_BOOK, ['status' => 0]);
        return $this->db->affected_rows() > 0;
    }

    public function rebook($event, $user)
    {
        $data = [
            'user'      => $user,
            'event'     => $event
        ];
        $this->db->where($data)->update(TBL_EVENTS_BOOK, ['status' => 1]);
        return $this->db->affected_rows() > 0;
    }
    
    public function checkbooking($event, $user)
    {
        $data = [
            'user'      => $user,
            'event'     => $event
        ];
        $event = $this->db->where($data)->get(TBL_EVENTS_BOOK);
        return $event->num_rows() > 0 ? $event->row()->status : false;
    }
}