<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/18/17
 * Time: 3:23 PM
 */
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="pull-right">
            <a href="<?=admin_url('events/add')?>" class="btn btn-success btn-xs tooltip-success loadForm" data-toggle="tooltip" data-successbtn="Add" data-addclass="bootbox-modal-sm" title="Add New Event">
                <i class="fa fa-plus"></i>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>S.N.</th>
                        <th>Title</th>
                        <th>Date Start</th>
                        <th>Date End</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Image</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead><?php //print_r($events);?>
                <tbody>
                <?php $cnt = 1; foreach($events as $event): ?>
                    <tr>
                        <td><?=$cnt++?></td>
                        <td><?=$event->title ?></td>
                        <td><?=date('Y-m-d', strtotime($event->date_start))?></td>
                        <td><?=date('Y-m-d', strtotime($event->date_end))?></td>
                        <td><?=$event->description?></td>
                        <td>
                        	<a href="<?=admin_url('events/changestatus/'.url_encrypt($event->id))?>" class="tooltip-<?=($event->status == 1 ? 'success' : 'danger')?>" data-toggle="tooltip" title="<?=($event->status == 1 ? 'un-publish' : 'publish')?> it">
                                <span class="label label-<?=($event->status == 1 ? 'success' : 'danger')?>"><?=($event->status == 1 ? 'Published' : 'Not Published')?></span>
                            </a>
                        </td>
                        <td>
                            <img src="<?=base_url(Utility::getUploadDir('events').$event->cover_pics)?>" class="img-responsive thumbnail slider-thumbnail">
                        </td>
                        <td nowrap="">
                            <a href="<?=admin_url('events/edit/'.url_encrypt($event->id))?>" class="btn btn-xs btn-info tooltip-info loadForm" data-toggle="tooltip" data-successbtn="Update" data-addclass="bootbox-modal-sm" title="edit">
                                <span class="fa fa-edit"></span>
                            </a>

                            <a href="<?=admin_url('events/delete/'.url_encrypt($event->id))?>" class="btn btn-xs btn-danger tooltip-danger confirmDiag" data-toggle="tooltip" data-confirmbtn="Yes, delete it!" data-msg="Are you sure? Once confirmed it cannot be undone." title="delete">
                                <span class="fa fa-remove"></span>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php if(count($events) == 0): ?>
                    <tr>
                        <td colspan="8">
                            No events has been added yet.
                        </td>
                    </tr>
                <?php endif ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
