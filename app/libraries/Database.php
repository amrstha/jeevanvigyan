<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/18/17
 * Time: 1:49 AM
 */
class Database
{
    private  $conn_id;

    public function __construct()
    {
        $this->conn_id = get_instance()->db->conn_id;
    }

    /**
     * Read the next result
     *
     * @return  null
     */
    function next_result()
    {
        if (is_object($this->conn_id))
        {
            return mysqli_next_result($this->conn_id);
        }
    }
}