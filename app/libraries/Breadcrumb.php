<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/11/17
 * Time: 3:22 PM
 */

(defined('BASEPATH')) || exit('No direct script access allowed');

/**
 * BreadCrumb Class
 *
 * This class manages the breadcrumb object.
 */
class Breadcrumb
{
    /**
     *  BreadCrumb Stacks
     */
    private $breadcrumbs = [];

    public function __construct($params = [])
    {
        $this->initialize($params);
    }

    public function initialize($params = [])
    {
        log_message('info', "Breadcrumb Class Initialized");
    }

    public function append($title, $link)
    {
        if (empty($title) || empty($title)) {
            return;
        }

        $this->breadcrumbs[] = [
            'title'     => $title,
            'link'      => $link
        ];
    }

    public function output()
    {
        if (count($this->breadcrumbs) == 0) {
            return '';
        }

        $breadcrumbs = '';
        $breadCrumbNum = count($this->breadcrumbs) - 1;
        foreach ($this->breadcrumbs as $index => $breadcrumb) {
            $breadcrumbs .= '<li class="' . ($breadCrumbNum == $index ? 'active' : '') . '">'. ($breadCrumbNum != $index ? '<a href="' . $breadcrumb['link'] . '">' . $breadcrumb['title'] . '</a>' : $breadcrumb['title'])  . '</li>';
        }
        return $breadcrumbs;
    }
}