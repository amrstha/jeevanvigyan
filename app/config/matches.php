<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/10/17
 * Time: 4:42 PM
 */

/**
 * Configuration for Matches controller
 */


/**
 * Configure the templates directory
 */
$config['templates'] = 'views/matches_templates/';


/**
 * Tell the Matches what controller to extend
 */
$config['c_extends'] = 'CI';

/**
 * Tell the Matches what model to extend
 */
$config['mo_extends'] = 'CI';

/**
 * Tell the Matches what Migration to extend
 */
$config['mi_extends'] = 'CI';
