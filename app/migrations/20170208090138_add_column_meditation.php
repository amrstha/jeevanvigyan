<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Add_column_meditation extends CI_Migration
{
    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_meditation          = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        $fields = [
            'image' => [
                'type'          => 'VARCHAR',
                'constraint'    => 200,
                'after'         => 'description_np'
            ],
            
        ];
        $this->dbforge->add_column($this->tbl_meditation, $fields);
    }

    public function down()
    {
        $this->dbforge->drop_column($this->tbl_meditation, [
            'image'
        ]);
    }

    private function use_config()
    {
        /*
        * If you have the parameter set to use the config table and join names
        * this will change them for you
        */
        if ($this->use_config) {
            // table names
            $this->tbl_meditation            = TBL_MEDITATION;
        }
    }
}
/* End of file '20170208090138_add_column_meditation' */
/* Location: ./C:\xampp\htdocs\jeevan-vigyan-web\application\migrations/20170208090138_add_column_meditation.php */
