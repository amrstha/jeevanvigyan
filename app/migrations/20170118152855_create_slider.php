<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_slider extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_slider   = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        // Drop table if it exists
        if ($this->db->table_exists($this->tbl_slider)) {
            if ($this->drop_table === false) {
                return;
            }
            $this->dbforge->drop_table($this->tbl_slider, true);
        }

        // Table structure for table
        $this->dbforge->add_field([
            'id' => [
                'type'          => 'INT',
                'constraint'    => '11',
                'unsigned'      => TRUE,
                'auto_increment'=> TRUE
            ],
            'title' => [
                'type'          => 'VARCHAR',
                'constraint'    => '100',
            ],
            'title_np' => [
                'type'          => 'VARCHAR',
                'constraint'    => '100',
            ],
            'caption' => [
                'type'          => 'VARCHAR',
                'constraint'    => '255',
                'null'          => true
            ],
            'caption_np' => [
                'type'          => 'VARCHAR',
                'constraint'    => '255',
                'null'          => true
            ],
            'image' => [
                'type'          => 'VARCHAR',
                'constraint'    => '255',
                'null'          => true
            ],
            'status' => [
                'type'          => 'int',
                'constraint'    => 5,
                'default'       => 0,
            ],
            'order_by' => [
                'type'          => 'int',
                'constraint'    => 5,
                'null'          => true
            ],
            'created_at  timestamp default current_timestamp'
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->tbl_slider);

        log_message('info', 'Slider Table Created in Database.');
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl_slider)) {
            $this->dbforge->drop_table($this->tbl_slider);
        }
    }

    private function use_config()
    {
        if ($this->use_config) {
            $this->tbl_slider = TBL_SLIDER;
        }
    }
}
/* End of file '20170118152855_create_slider' */
/* Location: .//var/www/html/projects/jeevan-vigyan-web/application/migrations/20170118152855_create_slider.php */
