<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_access_modules extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table names
    private $modules_tbl   = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        // Drop table 'messages' if it exists
        if ($this->db->table_exists($this->modules_tbl)) {
            if ($this->drop_table === false) {
                return;
            }
            $this->dbforge->drop_table($this->modules_tbl, true);
        }

        // Table structure for table 'messages'
        $this->dbforge->add_field([
            'id' => [
                'type'          => 'INT',
                'constraint'    => '11',
                'unsigned'      => TRUE,
                'auto_increment'=> TRUE
            ],
            'name' => [
                'type'          => 'VARCHAR',
                'constraint'    => '100',
            ],
            'description' => [
                'type'          => 'VARCHAR',
                'constraint'    => '255',
                'null'          => true
            ],
            'status' => [
                'type'          => 'int',
                'constraint'    => 5,
                'default'       => 1,
            ],
            'created_at  timestamp default current_timestamp'
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->modules_tbl);

        log_message('info', 'Access Modules Table Created in Database.');
    }

    public function down()
    {
        if ($this->db->table_exists($this->modules_tbl)) {
            $this->dbforge->drop_table($this->modules_tbl);
        }
    }

    private function use_config()
    {
        if ($this->use_config) {
            $this->modules_tbl = TBL_ACCESS_MODULES;
        }
    }
}
/* End of file '20170117163931_create_access_modules' */
/* Location: .//var/www/html/projects/jeevan-vigyan-web/application/migrations/20170117163931_create_access_modules.php */
