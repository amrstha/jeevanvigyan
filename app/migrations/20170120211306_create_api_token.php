<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_api_token extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_token   = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        // Drop table if it exists
        if ($this->db->table_exists($this->tbl_token)) {
            if ($this->drop_table === false) {
                return;
            }
            $this->dbforge->drop_table($this->tbl_token, true);
        }

        // Table structure for table
        $this->dbforge->add_field([
            'id' => [
                'type'          => 'INT',
                'constraint'    => '11',
                'unsigned'      => TRUE,
                'auto_increment'=> TRUE
            ],
            'user' => [
                'type'          => 'MEDIUMINT',
                'constraint'    => '8',
                'unsigned' => TRUE,
            ],
            'token' => [
                'type'          => 'TEXT',
            ],
            'ttl' => [
                'type'          => 'VARCHAR',
                'constraint'    => 50
            ],
            'created_at  timestamp default current_timestamp'
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->tbl_token);

        log_message('info', 'API Token Table Created in Database.');
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl_token)) {
            $this->dbforge->drop_table($this->tbl_token);
        }
    }

    private function use_config()
    {
        if ($this->use_config) {
            $this->tbl_token = TBL_API_USERS_TOKEN;
        }
    }
}
/* End of file '20170120211306_create_api_token' */
/* Location: .//var/www/html/projects/jeevan-vigyan-web/application/migrations/20170120211306_create_api_token.php */
