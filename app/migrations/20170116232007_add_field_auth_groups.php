<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Add_field_auth_groups extends CI_Migration
{
    // use config file variables
    private $use_config     = true;

    // Table names
    private $groups          = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        $fields = [
            'is_editable' => [
                'type'          => 'tinyint',
                'constraint'    => 4,
                'default'       => 1
            ],
            'is_userdeletable' => [
                'type'          => 'tinyint',
                'constraint'    => 4,
                'default'       => 1
            ]
        ];
        $this->dbforge->add_column($this->groups, $fields);

        // update data for table 'groups'
        $data = [
            'is_editable'       => 0,
            'is_userdeletable'  => 0
        ];
        $this->db->where('id', 1)->update($this->groups, $data);
    }

    public function down()
    {
        $this->dbforge->drop_column($this->groups, [
            'is_editable',
            'is_userdeletable'
        ]);
    }

    private function use_config()
    {
        /*
        * If you have the parameter set to use the config table and join names
        * this will change them for you
        */
        if ($this->use_config) {
            $this->config->load('ion_auth', TRUE);
            $tables     = $this->config->item('tables', 'ion_auth');

            // table names
            $this->groups            = $tables['groups'];
        }
    }
}
/* End of file '20170116232007_add_field_auth_groups' */
/* Location: .//var/www/html/projects/jeevan-vigyan-web/application/migrations/20170116232007_add_field_auth_groups.php */
