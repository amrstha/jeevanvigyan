<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_home_page extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_home_page   = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        // Drop table if it exists
        if ($this->db->table_exists($this->tbl_home_page)) {
            if ($this->drop_table === false) {
                return;
            }
            $this->dbforge->drop_table($this->tbl_home_page, true);
        }

        // Table structure for table
        $this->dbforge->add_field([
            'id' => [
                'type'          => 'INT',
                'constraint'    => '11',
                'unsigned'      => TRUE,
                'auto_increment'=> TRUE
            ],
        	'title' => [
        		'type'          => 'VARCHAR',
        		'constraint'	=> 100
        	],
        	'title_np' => [
        		'type'          => 'VARCHAR',
        		'constraint'	=> 100
        		],
            'content' => [
                'type'          => 'TEXT',
            ],
        	'content_np' => [
        		'type'          => 'TEXT',
        	],
            'created_at  timestamp default current_timestamp'
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->tbl_home_page);

        log_message('info', 'Home Table Created in Database.');
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl_home_page)) {
            $this->dbforge->drop_table($this->tbl_home_page);
        }
    }

    private function use_config()
    {
        if ($this->use_config) {
            $this->tbl_home_page = TBL_HOME_PAGE;
        }
    }
}
/* End of file '20170126140452_create_home_page.php' */
/* Location: .//var/www/html/projects/jeevan-vigyan-web/application/migrations/20170126140452_create_home_page.php */
