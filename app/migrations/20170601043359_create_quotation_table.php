<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_quotation_table extends CI_Migration
{

    // whether to drop table if exists
    private $drop_table     = false;

     // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_quotations   = '';

    public function __construct()
	{
	    parent::__construct();
		$this->load->dbforge();

        $this->use_config();
	}
	
	public function up()
	{
        // Drop table if it exists
        if($this->db->table_exists($this->tbl_quotations)) {
            if($this->drop_table === false){
                return;
            }
            $this->dbforge->drop_table($this->tbl_quotations, true);
        }

        // Table structure for table
        $this->dbforge->add_field([
            'id' => [
                'type'          =>'INT',
                'constraint'    =>'11',
                'unsigned'      =>TRUE,
                'auto_increment'=> TRUE
            ],
            'quotation' => [
                'type'          => 'TEXT',
                'null'          =>TRUE
            ],
            'language' => [
                'type'          => 'ENUM',
                'constraint'    => ['np','en'],
                'null'          =>TRUE
            ],
            'status' => [
                'type'          => 'INT',
                'constraint'    => '5',
                'default'       => 1
            ],
            'created_at  timestamp default current_timestamp'
        ]);

	    $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->tbl_quotations);
        
        log_message('info', 'Quotations Table Created in Database.');
    }

	public function down()
	{
        if($this->db->table_exists($this->tbl_quotations)){
	       $this->dbforge->drop_table($this->tbl_quotations);
        }
    }

    private function use_config()
    {
        if($this->use_config) {
            $this->tbl_quotations = TBL_QUOTATIONS;
        }
    }
    
}
/* End of file '20170601043359_create_quotation_table' */
/* Location: ./C:\xampp\htdocs\jeevanvigyan\app\migrations/20170601043359_create_quotation_table.php */
