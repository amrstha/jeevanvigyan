<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Seed_pages_category extends CI_Migration
{
    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_category          = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        $data = [
            [
                'id'        => CAT_NEWS,
                'name'      => 'News',
                'name_np'   => 'समचार',
                'description'   => 'Latest news.',
                'description_np'=> '',
                'slug'      => 'news',
                'status'    => 1,
                'editable'  => 0
            ],
            [
                'id'        => CAT_BLOG,
                'name'      => 'Blog',
                'name_np'   => 'Blog',
                'description'   => '',
                'description_np'=> '',
                'slug'      => 'blog',
                'status'    => 1,
                'editable'  => 0
            ]
        ];
        $this->db->insert_batch($this->tbl_category, $data);
    }

    public function down()
    {
        $this->db->where_in('id', [CAT_NEWS,CAT_BLOG])->delete($this->tbl_category);
    }

    private function use_config()
    {
        /*
        * If you have the parameter set to use the config table and join names
        * this will change them for you
        */
        if ($this->use_config) {
            // table names
            $this->tbl_category            = TBL_PAGES_CATEGORY;
        }
    }
}
/* End of file '20170224223101_seed_pages_category' */
/* Location: .//var/www/html/projects/jeevan-vigyan-web/application/migrations/20170224223101_seed_pages_category.php */
