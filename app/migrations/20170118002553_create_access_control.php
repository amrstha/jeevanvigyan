<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_access_control extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table names
    private $access_tbl   = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        // Drop table if it exists
        if ($this->db->table_exists($this->access_tbl)) {
            if ($this->drop_table === false) {
                return;
            }
            $this->dbforge->drop_table($this->access_tbl, true);
        }

        // Table structure for table
        $this->dbforge->add_field([
            'id' => [
                'type'          => 'INT',
                'constraint'    => '11',
                'unsigned'      => TRUE,
                'auto_increment'=> TRUE
            ],
            'ugroup_id' => [
                'type'          => 'MEDIUMINT',
                'constraint'    => '8',
                'unsigned' => TRUE,
            ],
            'amodule_id' => [
                'type'          => 'INT',
                'constraint'    => '11',
                'unsigned'      => TRUE,
            ],
            'created_at  timestamp default current_timestamp'
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field("CONSTRAINT fk_access_control_ugroup_id FOREIGN KEY (`ugroup_id`) REFERENCES ".TBL_AUTH_GROUPS."(`id`)");
        $this->dbforge->add_field("CONSTRAINT fk_access_control_amodule_id FOREIGN KEY (`amodule_id`) REFERENCES ".TBL_ACCESS_MODULES."(`id`)");
        $this->dbforge->create_table($this->access_tbl);

        log_message('info', 'Access Control Table Created in Database.');
    }

    public function down()
    {
        if ($this->db->table_exists($this->access_tbl)) {
            $this->dbforge->drop_table($this->access_tbl);
        }
    }

    private function use_config()
    {
        if ($this->use_config) {
            $this->access_tbl = TBL_ACCESS_CONTROL;
        }
    }
}
/* End of file '20170118002553_create_access_control' */
/* Location: .//var/www/html/projects/jeevan-vigyan-web/application/migrations/20170118002553_create_access_control.php */
