<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_events extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_events   = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        // Drop table if it exists
        if ($this->db->table_exists($this->tbl_events)) {
            if ($this->drop_table === false) {
                return;
            }
            $this->dbforge->drop_table($this->tbl_events, true);
        }

        // Table structure for table
        $this->dbforge->add_field([
            'id' => [
                'type'          => 'INT',
                'constraint'    => '11',
                'unsigned'      => TRUE,
                'auto_increment'=> TRUE
            ],
            'title' => [
                'type'          => 'VARCHAR',
                'constraint'    => '150',
            ],
            'date_start' => [
                'type'          => 'DATETIME',
            ],
            'date_end' => [
                'type'          => 'DATETIME',
            ],
            'description' => [
                'type'          => 'TEXT',
                'null'          => true,
            ],
            'cover_pics' => [
                'type'          => 'VARCHAR',
                'constraint'    => '150',
                'null'          => true,
            ],
            'status' => [
                'type'          => 'INT',
                'constraint'    => '5',
                'default'       => 0,
            ],
            'updated_at' => [
                'type'          => 'DATETIME',
                'null'          => true,
            ],
            'created_at  timestamp default current_timestamp'
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->tbl_events);

        log_message('info', 'Events Table Created in Database.');
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl_events)) {
            $this->dbforge->drop_table($this->tbl_events);
        }
    }

    private function use_config()
    {
        if ($this->use_config) {
            $this->tbl_events = TBL_EVENTS;
        }
    }
}
/* End of file '20170122163607_create_events' */
/* Location: .//var/www/html/projects/jeevan-vigyan-web/application/migrations/20170122163607_create_events.php */
