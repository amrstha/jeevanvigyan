<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_page_article extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_articles   = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        // Drop table if it exists
        if ($this->db->table_exists($this->tbl_articles)) {
            if ($this->drop_table === false) {
                return;
            }
            $this->dbforge->drop_table($this->tbl_articles, true);
        }

        // Table structure for table
        $this->dbforge->add_field([
            'id' => [
                'type'          => 'INT',
                'constraint'    => '11',
                'unsigned'      => TRUE,
                'auto_increment'=> TRUE
            ],
            'cat_id' => [
                'type'          => 'INT',
                'constraint'    => '11',
                'unsigned'      => TRUE,
            ],
            'title' => [
                'type'          => 'VARCHAR',
                'constraint'    => '150',
            ],
            'title_np' => [
                'type'          => 'VARCHAR',
                'constraint'    => '200',
            ],
            'content' => [
                'type'          => 'TEXT',
                'null'          => true,
            ],
            'content_np' => [
                'type'          => 'TEXT',
                'null'          => true,
            ],
            'image' => [
                'type'          => 'VARCHAR',
                'constraint'    => '200',
                'null'          => true,
            ],
            'slug' => [
                'type'          => 'VARCHAR',
                'constraint'    => '200',
            ],
            'status' => [
                'type'          => 'INT',
                'constraint'    => '5',
                'default'       => 1,
            ],
            'pub_date' => [
                'type'          => 'DATETIME',
                'null'          => true,
            ],
            'created_at  timestamp default current_timestamp'
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field("CONSTRAINT fk_page_article_cat_id FOREIGN KEY (`cat_id`) REFERENCES ".TBL_PAGES_CATEGORY."(`id`)");
        $this->dbforge->create_table($this->tbl_articles);

        log_message('info', 'Pages Article Table Created in Database.');
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl_articles)) {
            $this->dbforge->drop_table($this->tbl_articles);
        }
    }

    private function use_config()
    {
        if ($this->use_config) {
            $this->tbl_articles = TBL_PAGES_ARTICLE;
        }
    }
}
/* End of file '20170125154100_create_page_article' */
/* Location: .//var/www/html/projects/jeevan-vigyan-web/application/migrations/20170125154100_create_page_article.php */
