<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_program_table extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_program   = '';

    public function __construct()
	{
	    parent::__construct();
		$this->load->dbforge();

        $this->use_config();
	}
	
	public function up()
	{
        // Drop table if it exists
        if ($this->db->table_exists($this->tbl_program)) {
            if ($this->drop_table === false) {
                return;
            }
            $this->dbforge->drop_table($this->tbl_program, true);
        }

	    // Table structure for table
       
        $this->dbforge->add_field([
            'id' => [
                'type'          => 'INT',
                'constraint'    => '11',
                'unsigned'      => TRUE,
                'auto_increment'=> TRUE
            ],
            'title' => [
                'type'          => 'VARCHAR',
                'constraint'    => '150',
            ],
            'slug' => [
                'type'          => 'VARCHAR',
                'constraint'    => '150',
            ],
            'title_np' => [
                'type'          => 'VARCHAR',
                'constraint'    => '200',
            ],
            'description' => [
                'type'          => 'TEXT',
                'null'          => true,
            ],
            'description_np' => [
                'type'          => 'TEXT',
                'null'          => true,
            ],
            'image' => [
                'type'          => 'VARCHAR',
                'constraint'    => '255',
                'null'          => true
            ],
            'video' => [
                'type'          => 'VARCHAR',
                'constraint'    => '255',
                'null'          => true
            ],
            'status' => [
                'type'          => 'INT',
                'constraint'    => '5',
                'default'       => 1,
            ],
            'created_at  timestamp default current_timestamp'
        ]);

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->tbl_program);

        log_message('info', 'Program Table Created in Database.');
    }
    
	public function down()
	{
	    if($this->db->table_exists($this->tbl_program)) {
            $this->dbforge->drop_table($this->tbl_program);
        }
    }

    private function use_config()
    {
        if ($this->use_config) {
            $this->tbl_program = TBL_PROGRAM;
        }
    }
}
/* End of file '20170801135127_create_program_table' */
/* Location: ./C:\xampp\htdocs\jeevanvigyan\app\migrations/20170801135127_create_program_table.php */
