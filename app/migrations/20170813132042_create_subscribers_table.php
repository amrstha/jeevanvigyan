<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_subscribers_table extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_subscribers   = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();

    }

    public function up()
    {
        // Drop table if it exists
        if($this->db->table_exists($this->tbl_subscribers)) {
            if($this->drop_table === false){
                return;
            }
            $this->dbforge->drop_table($this->tbl_subscribers, true);
        }

        // Table structure for table
        $this->dbforge->add_field([
            'id' => [
                'type'          =>'INT',
                'constraint'    => 11,
                'unsigned'      => true,
                'auto_increment'=> true
            ],
            'email' => [
                'type'          => 'VARCHAR',
                'constraint'    => 100
            ],
            'status' => [
                'type'          => 'INT',
                'constraint'    => 5,
                'default'       => 1
            ],
            'created_at  timestamp default current_timestamp'
        ]);

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->tbl_subscribers);

        log_message('info', 'Subscribers Table Created in Database.');
    }

    public function down()
    {
        if($this->db->table_exists($this->tbl_subscribers)){
            $this->dbforge->drop_table($this->tbl_subscribers);
        }
    }

    private function use_config()
    {
        if($this->use_config) {
            $this->tbl_subscribers = TBL_SUBSCRIBERS;
        }
    }
}
/* End of file '20170813132042_create_subscribers_table' */
/* Location: ./C:\xampp\htdocs\jeevanvigyan\app\migrations/20170813132042_create_subscribers_table.php */
