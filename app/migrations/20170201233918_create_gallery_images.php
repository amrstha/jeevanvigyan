<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_gallery_images extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_images   = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        // Drop table if it exists
        if ($this->db->table_exists($this->tbl_images)) {
            if ($this->drop_table === false) {
                return;
            }
            $this->dbforge->drop_table($this->tbl_images, true);
        }

        // Table structure for table
        $this->dbforge->add_field([
            'id' => [
                'type'          => 'INT',
                'constraint'    => '11',
                'unsigned'      => TRUE,
                'auto_increment'=> TRUE
            ],
            'album' => [
                'type'          => 'INT',
                'constraint'    => '11',
                'unsigned'      => TRUE,
            ],
            'title' => [
                'type'          => 'VARCHAR',
                'constraint'    => '150',
                'null'          => TRUE
            ],
            'slug' => [
                'type'          => 'VARCHAR',
                'constraint'    => '150',
                'null'          => TRUE
            ],
            'title_np' => [
                'type'          => 'VARCHAR',
                'constraint'    => '200',
                'null'          => TRUE
            ],
            'caption' => [
                'type'          => 'TEXT',
                'null'          => true,
            ],
            'caption_np' => [
                'type'          => 'TEXT',
                'null'          => true,
            ],
            'file' => [
                'type'          => 'VARCHAR',
                'constraint'    => '150',
                'null'          => true,
            ],
            'thumbnail' => [
                'type'          => 'VARCHAR',
                'constraint'    => '255',
                'null'          => true
            ],
            'status' => [
                'type'          => 'INT',
                'constraint'    => '5',
                'default'       => 1,
            ],
            'created_at  timestamp default current_timestamp'
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field("CONSTRAINT fk_gallery_files_album_gallery_album_id FOREIGN KEY (`album`) REFERENCES ".TBL_GALLERY_ALBUM."(`id`)");
        $this->dbforge->create_table($this->tbl_images);

        log_message('info', 'Gallery Images Table Created in Database.');
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl_images)) {
            $this->dbforge->drop_table($this->tbl_images);
        }
    }

    private function use_config()
    {
        if ($this->use_config) {
            $this->tbl_images = TBL_GALLERY_FILES;
        }
    }
}
/* End of file '20170201233918_create_gallery_images' */
/* Location: .//var/www/html/projects/jeevan-vigyan-web/application/migrations/20170201233918_create_gallery_images.php */
