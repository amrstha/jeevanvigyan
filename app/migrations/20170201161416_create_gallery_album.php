<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_gallery_album extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_album   = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        // Drop table if it exists
        if ($this->db->table_exists($this->tbl_album)) {
            if ($this->drop_table === false) {
                return;
            }
            $this->dbforge->drop_table($this->tbl_album, true);
        }

        // Table structure for table
        $this->dbforge->add_field([
            'id' => [
                'type'          => 'INT',
                'constraint'    => '11',
                'unsigned'      => TRUE,
                'auto_increment'=> TRUE
            ],
            'name' => [
                'type'          => 'VARCHAR',
                'constraint'    => '150',
            ],
            'slug' => [
                'type'          => 'VARCHAR',
                'constraint'    => '150',
            ],
            'name_np' => [
                'type'          => 'VARCHAR',
                'constraint'    => '200',
            ],
            'description' => [
                'type'          => 'TEXT',
                'null'          => true,
            ],
            'description_np' => [
                'type'          => 'TEXT',
                'null'          => true,
            ],
            'image' => [
                'type'          => 'VARCHAR',
                'constraint'    => '150',
                'null'          => true,
            ],
            'type' => [
                'type'          => 'INT',
                'constraint'    => '5',
                'default'       => 1,
            ],
            'status' => [
                'type'          => 'INT',
                'constraint'    => '5',
                'default'       => 1,
            ],
            'updated_at' => [
                'type'          => 'DATETIME',
                'null'          => true,
            ],
            'created_at  timestamp default current_timestamp'
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->tbl_album);

        log_message('info', 'Gallery Album Table Created in Database.');
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl_album)) {
            $this->dbforge->drop_table($this->tbl_album);
        }
    }

    private function use_config()
    {
        if ($this->use_config) {
            $this->tbl_album = TBL_GALLERY_ALBUM;
        }
    }
}
/* End of file '20170201161416_create_gallery_album' */
/* Location: .//var/www/html/projects/jeevan-vigyan-web/application/migrations/20170201161416_create_gallery_album.php */
