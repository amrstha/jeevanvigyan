<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_contact_us_table extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_contact_us = '';

    public function __construct()
	{
	    parent::__construct();
		$this->load->dbforge();

		$this->use_config();
	}
	
	public function up()
	{
        //Drop table if exists
        if($this->db->table_exists($this->tbl_contact_us)) {
            if($this->drop_table === false){
                return;
            }
            $this->dbforge->drop_table($this->tbl_contact_us, true);
        }

        //Table structure for table
        $this->dbforge->add_field([
            'id' => [
                'type'          => 'INT',
                'constraint'    => 11,
                'unsigned'      => true,
                'auto_increment'=> true
            ],
            'name' => [
                'type'          => 'VARCHAR',
                'constraint'    => 100,
                'null'          => true
            ],
            'email' => [
                'type'          => 'VARCHAR',
                'constraint'    => 100,
                'null'          => true
            ],
            'subject' => [
                'type'          => 'VARCHAR',
                'constraint'    => 200,
                'null'          => true
            ],
            'message' => [
                'type'          => 'TEXT',
                'null'          => true
            ],
            'is_read' => [
                'type'          => 'INT',
                'constraint'    => 1,
                'default'       => 0
            ],
            'created_at timestamp default current_timestamp',
        ]);

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->tbl_contact_us);

        log_message('info', 'Contact Us Table Created in Database.');
    }
    
	public function down()
	{
	    if($this->db->table_exists($this->tbl_contact_us)){
	        $this->dbforge->drop_table($this->tbl_contact_us);
        }
    }

    private function use_config()
    {
        if($this->use_config) {
            $this->tbl_contact_us = TBL_CONTACT_US;
        }
    }

}
/* End of file '20170604075637_create_contact_us_table' */
/* Location: ./C:\xampp\htdocs\jeevanvigyan\app\migrations/20170604075637_create_contact_us_table.php */
