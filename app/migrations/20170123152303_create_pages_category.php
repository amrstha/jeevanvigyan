<?php  (defined('BASEPATH')) || exit('No direct script access allowed');

// You can find dbforge usage examples here: http://ellislab.com/codeigniter/user-guide/database/forge.html


class Migration_Create_pages_category extends CI_Migration
{
    // whether to drop table if exists
    private $drop_table     = false;

    // use config file variables
    private $use_config     = true;

    // Table names
    private $tbl_category   = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->dbforge();

        $this->use_config();
    }

    public function up()
    {
        // Drop table if it exists
        if ($this->db->table_exists($this->tbl_category)) {
            if ($this->drop_table === false) {
                return;
            }
            $this->dbforge->drop_table($this->tbl_category, true);
        }

        // Table structure for table
        $this->dbforge->add_field([
            'id' => [
                'type'          => 'INT',
                'constraint'    => '11',
                'unsigned'      => TRUE,
                'auto_increment'=> TRUE
            ],
            'name' => [
                'type'          => 'VARCHAR',
                'constraint'    => '150',
            ],
            'name_np' => [
                'type'          => 'VARCHAR',
                'constraint'    => '150',
            ],
            'description' => [
                'type'          => 'TEXT',
                'null'          => true,
            ],
            'description_np' => [
                'type'          => 'TEXT',
                'null'          => true,
            ],
            'slug' => [
                'type'          => 'VARCHAR',
                'constraint'    => '200',
            ],
            'status' => [
                'type'          => 'INT',
                'constraint'    => '5',
                'default'       => 1,
            ],
            'updated_at' => [
                'type'          => 'DATETIME',
                'null'          => true,
            ],
            'created_at  timestamp default current_timestamp'
        ]);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table($this->tbl_category);

        log_message('info', 'Pages Category Table Created in Database.');
    }

    public function down()
    {
        if ($this->db->table_exists($this->tbl_category)) {
            $this->dbforge->drop_table($this->tbl_category);
        }
    }

    private function use_config()
    {
        if ($this->use_config) {
            $this->tbl_category = TBL_PAGES_CATEGORY;
        }
    }
}
/* End of file '20170123152303_create_pages_category' */
/* Location: .//var/www/html/projects/jeevan-vigyan-web/application/migrations/20170123152303_create_pages_category.php */
