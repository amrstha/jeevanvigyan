<?php

(defined('BASEPATH')) || exit('No direct script access allowed');

/**
* Exception Class
*/
class MY_Exceptions extends CI_Exceptions
{
	public function __construct()
	{
		parent::__construct();
	}

}