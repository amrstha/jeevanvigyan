<?php

(defined('BASEPATH')) || exit('No direct script access allowed');

require_once(APPPATH.'models/Repository.php');

use Model\Front\Repository as Repository;

/**
* Front / Public Page Controller
*/
class Front_Controller extends MY_Controller
{
	protected static $viewData = [];
	
    function __construct()
    {
        parent::__construct();

        $front_template_path = './templates/front/'.FRONT_TMPL.'/';        
        if (file_exists($front_template_path.'functions'.EXT)) {
            Modules::load_file('functions'.EXT, $front_template_path);
        }

        $this->load->model('menu/socialModel', 'social');
        $this->load->model('program/programModel', 'program');
        $this->load->model('quotations/quotationsModel', 'quotations');

        $lang = $this->session->userdata('lang');
        if ($lang === 'np') {
            self::$viewData['lang'] = 'np';
            self::$viewData['langAlias'] = 'nepali';
        } else {
            self::$viewData['lang'] = 'en';
            self::$viewData['langAlias'] = 'english';
        }
        $this->config->set_item('language', self::$viewData['langAlias']);

        $this->widgets();

        $this->benchmark->mark('front_controller_start');
    }

    private function widgets()
    {
    }
}