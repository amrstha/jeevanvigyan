<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/16/17
 * Time: 8:56 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class SettingsController extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('settings_module')
        ];
        $this->breadcrumb->append('Settings', admin_url('settings'));
    }

    public function index()
    {
        $this->accounts();
    }

    public function accounts()
    {
        $this->form_validation->set_rules('old_password', 'Old Password', 'prep_for_form|strip_image_tags|encode_php_tags|required');
        $this->form_validation->set_rules('password', 'New Password', 'prep_for_form|strip_image_tags|encode_php_tags|required|matches[confirm_password]|min_length['. $this->config->item('min_password_length', 'ion_auth') .']');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'prep_for_form|strip_image_tags|encode_php_tags|required');

        if($this->form_validation->run() === false) {
            // load form
            $this->breadcrumb->append('Account', admin_url('settings/accounts'));

            self::$viewData['pageDetail'] = (object)[
                'title'     => 'Settings: Account',
                'subTitle'  => 'Accounts related configuration page'
            ];
            $this->load->admintheme('accounts', self::$viewData);
        } else {
            // validation success
            $identity = $this->config->item('identity', 'ion_auth');
            $identity = $this->authentication->getUser()->$identity;
            $old_password = $this->input->post('old_password');
            $new_password = $this->input->post('password');

            if($this->authentication->changePassword($identity, $old_password, $new_password)) {
                $this->session->set_notification('success', $this->lang->line('password_changed'));
            } else {
                $this->session->set_notification('error', $this->authentication->errors());
            }

            admin_redirect('settings/accounts');
        }
    }

    public function profile()
    {
        $this->form_validation->set_rules('name_prefix', 'Name Prefix', 'prep_for_form|strip_image_tags|encode_php_tags');
        $this->form_validation->set_rules('first_name', 'First Name', 'prep_for_form|strip_image_tags|encode_php_tags|required');
        $this->form_validation->set_rules('middle_name', 'Middle Name', 'prep_for_form|strip_image_tags|encode_php_tags');
        $this->form_validation->set_rules('last_name', 'Last Name', 'prep_for_form|strip_image_tags|encode_php_tags|required');
        $this->form_validation->set_rules('name_suffix', 'Name Suffix', 'prep_for_form|strip_image_tags|encode_php_tags');
        $this->form_validation->set_rules('gender', 'Gender', 'required');
        $this->form_validation->set_rules('phone', 'Phone', 'prep_for_form|strip_image_tags|encode_php_tags');
        $this->form_validation->set_rules('company', 'Organization', 'prep_for_form|strip_image_tags|encode_php_tags');
        $this->form_validation->set_rules('designation', 'Designation', 'prep_for_form|strip_image_tags|encode_php_tags');

        if($this->form_validation->run() === false) {
            // load form
            $this->breadcrumb->append('Profile', admin_url('settings/profile'));

            self::$viewData['pageDetail'] = (object)[
                'title'     => 'Settings: Profile',
                'subTitle'  => 'Settings and managing current user profile.'
            ];
            $this->load->admintheme('profile', self::$viewData);
        } else {
            // validation success

            // Upload image first if available
            $prevPPName = $this->input->post('prevPPName');
            if( ! empty($_FILES['profile_pic']['name']) ) {
                $imgResponse = $this->upload_image();
                // delete previous image
                $this->authentication->deleteProfilePicture($prevPPName);
            } else {
                $imgResponse = $prevPPName;
            }

            if($imgResponse !== false) {
                // Update Information in database
                $id = $this->authentication->getUser()->id;
                $updateData = [
                    'name_prefix' 	=> $this->input->post('name_prefix'),
                    'first_name' 	=> $this->input->post('first_name'),
                    'middle_name' 	=> $this->input->post('middle_name'),
                    'last_name' 	=> $this->input->post('last_name'),
                    'name_suffix' 	=> $this->input->post('name_suffix'),
                    'gender' 		=> $this->input->post('gender'),
                    'phone' 		=> $this->input->post('phone'),
                    'company' 		=> $this->input->post('company'),
                    'designation' 	=> $this->input->post('designation'),
                    'profile_pic'	=> $imgResponse
                ];
                if($this->authentication->update($id, $updateData)) {
                    $this->session->set_notification('success', 'Profile updated successfully!!!');
                } else {
                    $this->session->set_notification('error', 'Error occurred while updating profile!!!');
                }
            }

            admin_redirect('settings/profile');
        }
    }

    /*
	 * Private function to upload image
	 */
    private function upload_image()
    {
        $config['upload_path'] = $this->authentication->getUploadDir('profile_pics');
        $config['allowed_types'] = 'jpg|png|gif|jpeg';
        $config['max_size']	= '2048';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if( ! $this->upload->do_upload('profile_pic')) {
            $this->session->set_notification('error', $this->upload->display_errors());
            return false;
        } else {
            $data = ['upload_data' => $this->upload->data()];
            return $data['upload_data']['file_name'];
        }
    }
}