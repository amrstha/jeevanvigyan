<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 09/06/2017
 * Time: 11:59
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class OrganizationSettingController extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('settings_module')
        ];
        $this->breadcrumb->append('Organization Settings', admin_url('settings/organizationsetting/add'));

        // load model
        $this->load->model('settings/OrganizationSettingModel', 'organization');
    }

    public function index()
    {
        $this->breadcrumb->append('Information', admin_url('organizatonsetting/add'));

        self::$viewData['pageDetail'] = (object)[
            'title' => 'Settings: Organization Information',
            'subTitle' => 'Organization related configuration page'
        ];

        self::$viewData['settings'] = $this->organization->getAll();

        $this->load->admintheme('orgsetting', self::$viewData);
    }

    public function save()
    {
        $this->form_validation->set_rules('key', 'Key', 'required|prep_for_form|strip_image_tags|encode_php_tags');
        $this->form_validation->set_rules('value', 'Value', 'required|prep_for_form|strip_image_tags|encode_php_tags');

        try {
            if ($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
                throw new Exception(json_encode($response));
            }

            $key = $this->input->post('key');
            $value = $this->input->post('value');

            if ($this->organization->save($key, $value) === false) {
                $response = [
                    'status' => 'error',
                    'data' => [
                        'key' => $this->security->get_csrf_token_name(),
                        'value' => $this->security->get_csrf_hash()
                    ],
                    'message' => 'Unknown error occurred.'
                ];
                throw new Exception(json_encode($response));
            }

            $response = [
                'status' => 'success',
                'data' => [
                    'key' => $this->security->get_csrf_token_name(),
                    'value' => $this->security->get_csrf_hash()
                ],
                'message' => 'Save successfully!!!'
            ];
        } catch (Exception $exception) {
            $response = json_decode($exception->getMessage());
        }

        $this->output->json($response);
    }
}