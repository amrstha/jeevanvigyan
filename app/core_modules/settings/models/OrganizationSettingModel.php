<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 09/06/2017
 * Time: 12:03
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class OrganizationSettingModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getOrgName()
    {
        $data = $this->db->where('key', 'organization_name')->get(TBL_SETTING_ORG);
        return $data->num_rows() > 0 ? $data->row() : false;
    }

    public function getOrgAddress()
    {
        $data = $this->db->where('key', 'organization_address')->get(TBL_SETTING_ORG);
        return $data->num_rows() > 0 ? $data->row() : false;
    }

    public function getOrgEmail()
    {
        $data = $this->db->where('key', 'organization_email')->get(TBL_SETTING_ORG);
        return $data->num_rows() > 0 ? $data->row() : false;
    }

    public function getOrgPhone()
    {
        $data = $this->db->where('key', 'organization_phone')->get(TBL_SETTING_ORG);
        return $data->num_rows() > 0 ? $data->row() : false;
    }

    public function save($key, $value)
    {
        $data = $this->db->where('key', $key)->from(TBL_SETTING_ORG)->count_all_results();

        if ($data > 0) {
            // update
            return $this->db->where('key', $key)->update(TBL_SETTING_ORG, ['value' => $value]);
        }

        // create
        return $this->db->insert(TBL_SETTING_ORG, ['key' => $key, 'value' => $value]);
    }

    public function getAll()
    {
        $data = $this->db->get(TBL_SETTING_ORG)->result();

        $dataArr = [];
        foreach ($data as $datum) {
            $dataArr[$datum->key] = $datum->value;
        }

        return (object) $dataArr;
    }


}