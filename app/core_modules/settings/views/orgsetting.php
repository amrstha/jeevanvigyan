<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 10/06/2017
 * Time: 07:18
 */
?>


<div class="x_panel">
    <div class="x_title">
        <h2>
            <?=$pageDetail->subTitle?>
            <span class="fa fa-spinner fa-spin ajax-loader-setting"></span>
        </h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        $attributes = [
            'class' => 'form-horizontal',
            'role'  => 'form',
            'name'  => 'org-setting-form',
            'id'    => 'org-setting-form'
        ];
        $action = admin_url('settings/organizationsetting/save');
        echo form_open($action, $attributes);
        ?>

        <div class="col-sm-12">

            <div class="form-group">
                <label class="col-sm-2 control-label" for="organization_name">Organisation Name:</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control organizational_setting_field" name="organization_name" id="organization_name" value="<?=isset($settings->organization_name) ? $settings->organization_name : ''?>" placeholder="Organization Name" >
                    <?=form_error('organization_name', '<span class="form-msg text-red">', '</span>')?>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="form-group">
                <label class="col-sm-2 control-label" for="organization_address">Organization Address:</label>
                <div class="col-sm-4">
                    <textarea class="form-control organizational_setting_field" name="organization_address" id="organization_address"><?=isset($settings->organization_address) ? $settings->organization_address : ''?></textarea>
                    <?=form_error('organization_address', '<span class="form-msg text-red">', '</span>')?>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="form-group">
                <label class="col-sm-2 control-label" for="organization_email">Organization Email:</label>
                <div class="col-sm-4">
                    <input type="email" class="form-control organizational_setting_field" name="organization_email" id="organization_email" value="<?=isset($settings->organization_email) ? $settings->organization_email : ''?>" placeholder="Organization Email" >
                    <?=form_error('organization_email', '<span class="form-msg text-red">', '</span>')?>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="form-group">
                <label class="col-sm-2 control-label" for="organization_phone">Organization Phone:</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control organizational_setting_field" name="organization_phone" id="organization_phone" value="<?=isset($settings->organization_phone) ? $settings->organization_phone : ''?>" placeholder="Organization Phone" >
                    <?=form_error('organization_phone', '<span class="form-msg text-red">', '</span>')?>
                </div>
            </div>
        </div>

        <?=form_close()?>

    </div>
</div>

