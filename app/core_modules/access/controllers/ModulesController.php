<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/17/17
 * Time: 4:27 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class ModulesController extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('access_module')
        ];
        $this->breadcrumb->append('Access Control', admin_url('access'));

        $this->load->model('modulesModel', 'modules');
    }

    public function index()
    {
        $this->breadcrumb->append('Modules', admin_url('access/modules'));

        self::$viewData['pageDetail'] = (object)[
            'title'     => 'Access Control : Modules',
            'subTitle'  => 'List of access modules'
        ];

        self::$viewData['modules'] = $this->modules->getAll();
        $this->load->admintheme('modules/lists', self::$viewData);
    }

    public function add()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $response = [];
        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('name', 'Module Name', 'required');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Save Information in database
                $insertData = [
                    'name'			=> $this->input->post('name'),
                    'description'	=> $this->input->post('description')
                ];

                $result = $this->modules->add($insertData);
                if(!$result) {
                    $response = [
                        'status' 	=> 'error',
                        'data'		=> NULL,
                        'message'	=> $this->lang->line('error_added')
                    ];
                } else {
                    $this->session->set_notification('success', $this->lang->line('success_added'));
                    $response = [
                        'status' 	=> 'success',
                        'data'		=> NULL,
                        'message'	=> 'success'
                    ];
                }
            }

        } else {
            // load form
            $form = $this->load->view('modules/form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function edit()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $id = Utility::getIdFromUri(5);
        if ($id === false) {
            $this->output->json([
                'status' 	=> 'error',
                'data'		=> NULL,
                'message'	=> $this->lang->line('invalid_request')
            ]);
        }

        $response = [];
        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('name', 'Modules Name', 'required');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Save Information in database
                $updateData = [
                    'name'			=> $this->input->post('name'),
                    'description'	=> $this->input->post('description')
                ];

                $result = $this->modules->update($id, $updateData);
                if(!$result) {
                    $response = [
                        'status' 	=> 'error',
                        'data'		=> NULL,
                        'message'	=> $this->lang->line('nothing_updated')
                    ];
                } else {
                    $this->session->set_notification('success', $this->lang->line('success_updated'));
                    $response = [
                        'status' 	=> 'success',
                        'data'		=> NULL,
                        'message'	=> 'success'
                    ];
                }
            }

        } else {
            // load form
            self::$viewData['edit'] = $this->modules->getById($id);
            $form = $this->load->view('modules/form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function delete()
    {
        $id = Utility::getIdFromUri(5);

        if ($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if( ! $this->modules->delete($id) ) {
                $this->session->set_notification('error', $this->lang->line('error_delete'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_delete'));
            }
        }
        admin_redirect('access/modules', 'refresh');
    }

    public function undodelete()
    {
        $id = Utility::getIdFromUri(5);

        if ($id === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if( ! $this->modules->undodelete($id) ) {
                $this->session->set_notification('error', $this->lang->line('error_updated'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_updated'));
            }
        }
        admin_redirect('access/modules', 'refresh');
    }
}