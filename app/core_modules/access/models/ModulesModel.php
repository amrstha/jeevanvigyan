<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/17/17
 * Time: 4:33 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class ModulesModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function add($data)
    {
        $this->db->insert(TBL_ACCESS_MODULES, $data);
        return $this->db->affected_rows() > 0;
    }

    public function update($id, $data)
    {
        $this->db->where('id', $id)->update(TBL_ACCESS_MODULES, $data);
        return $this->db->affected_rows() > 0;
    }

    public function delete($id)
    {
        $this->db->where('id', $id)->update(TBL_ACCESS_MODULES, ['status' => 0]);
        return $this->db->affected_rows() > 0;
    }

    public function undodelete($id)
    {
        $this->db->where('id', $id)->update(TBL_ACCESS_MODULES, ['status' => 1]);
        return $this->db->affected_rows() > 0;
    }

    public function getAll($activeOnly = false, $limit = null, $offset = 0)
    {
        if ($activeOnly) {
            $this->db->where('status', 1);
        }

        if (!is_null($limit)) {
            $this->db->limit($limit, $offset);
        }

        return $this->db->get(TBL_ACCESS_MODULES)->result();
    }

    public function getById($id)
    {
        $cat = $this->db->where('id', $id)->get(TBL_ACCESS_MODULES);
        return $cat->num_rows() > 0 ? $cat->row() : false;
    }
}