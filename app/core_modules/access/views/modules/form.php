<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/17/17
 * Time: 4:57 PM
 */
?>

<div class="row">
    <div class="col-sm-12">

        <?php
        $attributes = [
            'class' => 'form-horizontal',
            'role'  => 'form',
            'name'  => 'popup_form',
            'id'    => 'popup_form'
        ];
        $action = admin_url('access/modules/'.(isset($edit) ? 'edit/'.url_encrypt($edit->id) : 'add'));
        echo form_open($action, $attributes);
        ?>

        <div class="form-group has-feedback">
            <label class="col-sm-3 control-label" for="name">Name: <span class="text-red">*</span></label>
            <div class="col-sm-9">
                <input type="text" name="name" id="name" class="form-control required" placeholder="Module name" value="<?=(isset($edit) ? $edit->name : '')?>" />
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group has-feedback">
            <label class="col-sm-3 control-label" for="description">Description:</label>
            <div class="col-sm-9">
                <textarea class="form-control" name="description" id="description" placeholder="Module short description"><?=(isset($edit) ? $edit->description : '')?></textarea>
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>

        <?=form_close()?>

    </div>
</div>
