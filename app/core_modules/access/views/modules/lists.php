<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/17/17
 * Time: 4:33 PM
 */
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="pull-right">
            <a href="<?=admin_url('access/modules/add')?>" class="btn btn-success btn-xs tooltip-success loadForm" data-toggle="tooltip" data-successbtn="Add" data-addclass="bootbox-modal-xs" title="Add New Access Module">
                <i class="fa fa-plus"></i>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="table-responsive">
            <table class="table table-bordered table-hover <?=(count($modules) > 0 ? 'data-table' : '')?>">
                <thead>
                <tr>
                    <th>S.N.</th>
                    <th>Status</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php $cnt = 1; foreach($modules as $module): ?>
                    <tr>
                        <td><?=$cnt++?></td>
                        <td>
                            <span class="label label-<?=($module->status == 1 ? 'success' : 'danger')?>"><?=($module->status == 1 ? 'Active' : 'Deleted')?></span>
                        </td>
                        <td>
                            <?=$module->name?>
                            <span class="label label-default">id:<?php echo $module->id ?></span>
                        </td>
                        <td><?=$module->description?></td>
                        <td nowrap="">
                            <?php if ($module->status == 1): ?>
                                <a href="<?=admin_url('access/modules/edit/'.url_encrypt($module->id))?>" class="btn btn-xs btn-info tooltip-info loadForm" data-toggle="tooltip" data-successbtn="Update" data-addclass="bootbox-modal-xs" title="edit">
                                    <span class="fa fa-edit"></span>
                                </a>

                                <a href="<?=admin_url('access/modules/delete/'.url_encrypt($module->id))?>" class="btn btn-xs btn-danger tooltip-danger confirmDiag" data-toggle="tooltip" data-confirmbtn="Yes, delete it!" data-msg="Are you sure? You are going to delete." title="delete">
                                    <span class="fa fa-remove"></span>
                                </a>
                            <?php else: ?>
                                <a href="<?=admin_url('access/modules/undodelete/'.url_encrypt($module->id))?>" class="btn btn-xs btn-danger tooltip-danger confirmDiag" data-toggle="tooltip" data-confirmbtn="Yes, undo delete!" data-msg="Are you sure? You are going to undo the delete action." title="undo delete">
                                    <span class="fa fa-refresh"></span>
                                </a>
                            <?php endif ?>

                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php if(count($modules) == 0): ?>
                    <tr>
                        <td colspan="4">
                            No modules has been created yet.
                        </td>
                    </tr>
                <?php endif ?>
                </tbody>
            </table>
        </div>

    </div>
</div>