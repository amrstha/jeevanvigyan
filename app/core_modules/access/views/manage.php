<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/18/17
 * Time: 12:21 AM
 */
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <?php $cnt = 0; foreach($accesses[0] as $header => $val): ?>
                            <?php if($cnt++ == 0): ?>
                                <th>&nbsp;</th>
                            <?php else: ?>
                                <th><?=ucfirst($header)?></th>
                            <?php endif ?>
                        <?php endforeach ?>
                    </tr>
                </thead>

                <tbody>

                <?php $cnt = 0; foreach($accesses as $access): ?>
                    <tr>
                        <?php $val_cnt = 0; foreach($access as $val): ?>
                            <?php if($val_cnt++ == 0): ?>
                                <td>
                                    <a href="<?=admin_url('access/control/'.url_encrypt($access->group_id))?>" class="btn btn-xs btn-info tooltip-info loadForm" data-toggle="tooltip" data-successbtn="Update" data-addclass="bootbox-modal-sm" title="access control">
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </a>
                                </td>
                            <?php else: ?>
                                <td>
                                    <?php if($val == '1'): ?>
                                        <span class="glyphicon glyphicon-ok text-green"></span>
                                    <?php elseif($val == '0'): ?>
                                        <span class="glyphicon glyphicon-remove text-red"></span>
                                    <?php else: ?>
                                        <?=$val?>
                                    <?php endif ?>
                                </td>
                            <?php endif ?>
                        <?php endforeach ?>
                    </tr>
                    <?php $cnt++; endforeach; ?>
                <?php if($cnt == 0): ?>
                    <tr>
                        <td colspan="4">
                            No user groups has been added.
                        </td>
                    </tr>
                <?php endif ?>

                </tbody>
            </table>
        </div>

    </div>
</div>
