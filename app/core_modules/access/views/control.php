<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 2/2/17
 * Time: 1:17 AM
 */
?>

<div class="row">
    <div class="col-xs-12">

        <?php
        $attributes = array(
            'class' => 'form-horizontal',
            'role'  => 'form',
            'name'  => 'popup_form',
            'id'    => 'popup_form'
        );
        $url = 'access/accesscontrol/'.url_encrypt($group->id);
        $action = admin_url($url);
        echo form_open($action, $attributes);
        ?>

        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label">User Group:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control required" name="user_group" id="user_group" value="<?php echo $group->name ?>" readonly="">
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="modules">Modules:</label>
            <div class="col-sm-10">
                <div class="row">
                    <div class="col-md-12">

                        <div class="row accesscontrol">

                            <?php foreach($modules as $module): ?>
                                <div class="col-sm-4">
                                    <label class="tooltip-info" data-rel="tooltip" data-placement="top" title="<?php echo $module->description ?>">
                                        <input  type="checkbox"
                                                class="flat array checkbox"
                                                name="access_modules[]"
                                                id="access_modules_<?php echo $module->id ?>"
                                                value="<?php echo $module->id ?>"
                                            <?php echo ($module->has_access == '1') ? 'checked' : '' ?>
                                        />
                                        <?php echo $module->name ?>
                                    </label>
                                </div>
                            <?php endforeach ?>

                        </div> <!-- row -->

                    </div>
                </div>
            </div>
        </div>
        <div class="hr-line-dashed"></div>

        <?php echo form_close() ?>
    </div>
</div>

<script type="text/javascript">
    $('[data-rel=tooltip]').tooltip();
</script>
