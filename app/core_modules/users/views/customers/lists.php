<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="pull-right">
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#active-tab" id="active-tab-btn" role="tab" data-toggle="tab" aria-expanded="true">
                        Active
                    </a>
                </li>
                <li role="presentation" class="">
                    <a href="#inactive-tab" role="tab" id="inactive-tab-btn" data-toggle="tab" aria-expanded="false">
                        Inactive
                    </a>
                </li>
            </ul>

            <div id="userListTab" class="tab-content">
                <!-- Active Users -->
                <div role="tabpanel" class="tab-pane fade active in" id="active-tab" aria-labelledby="active-tab-btn">
                    <div class="table-responsive"><?php $cnt=0;?>
                        <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped <?=($cnt == count($active_customers) ? '' : 'data-table')?>">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Full Name</th>
                                    <th>username</th>
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th>Info</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $cnt = 1;
                            foreach ($active_customers as $active_customer): if($active_customer->active != 1) continue; ?>
                                <tr>
                                    <td><?=$cnt++.'.'?></td>
                                    <td><?=$active_customer->first_name." ".$active_customer->last_name;?></td>
                                    <td><?=$active_customer->username?></td>
                                    <td><?=$active_customer->email?></td>
                                    <td><?=$active_customer->phone?></td>
                                    <td><?=($active_customer->occupation. ' (' . $active_customer->organization . ')')?></td>
                                    <td nowrap="">
                                        <a href="<?=admin_url('users/customers/edit/'.url_encrypt($active_customer->id))?>" class="btn btn-xs btn-success tooltip-success loadForm" data-toggle="tooltip" data-successbtn="Update" data-addclass="bootbox-modal-sm" title="edit">
                                            <span class="fa fa-edit"></span>
                                        </a>

                                            <a href="<?=admin_url('users/customers/editaccount/'.url_encrypt($active_customer->id))?>" class="btn btn-xs btn-info tooltip-info loadForm" data-toggle="tooltip" data-successbtn="Update" data-addclass="bootbox-modal-sm" title="edit account">
                                                <span class="fa fa-user"></span>
                                            </a>

                                            <a href="<?=admin_url('users/customers/editpassword/'.url_encrypt($active_customer->id))?>" class="btn btn-xs btn-warning tooltip-warning loadForm" data-toggle="tooltip" data-successbtn="Update" data-addclass="bootbox-modal-sm" title="change password">
                                                <span class="fa fa-lock"></span>
                                            </a>

                                            <a href="<?=admin_url('users/customers/deactivate/'.url_encrypt($active_customer->id))?>" class="btn btn-xs btn-danger tooltip-danger confirmDiag" data-toggle="tooltip" data-confirmbtn="Yes, deactivate this user!" data-msg="Are you sure?" title="de-activate">
                                                <span class="fa fa-remove"></span>
                                            </a>
                                    </td>
                                </tr>
                            <?php endforeach;
                            if ($cnt == 1): ?>
                                <tr>
                                    <td colspan="8">No record found!</td>
                                </tr>
                            <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /Active Users -->

                <!-- In-active Users -->
                <div role="tabpanel" class="tab-pane fade" id="inactive-tab" aria-labelledby="inactive-tab-btn">
                    <div class="table-responsive"><?php $cnt1=0;?>
                        <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped <?=($cnt1 == count($inactive_customers) ? '' : 'data-table')?>">
                           <?php //print_r($cnt);exit;?> <thead>
                            <tr>
                                <th>SN</th>
                                <th>Full Name</th>
                                <th>username</th>
                                <th>Email</th>
                                <th>Contact</th>
                                <th>Info</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $cnt = 1;
                            foreach ($inactive_customers as $inactive_customer): if($inactive_customer->active != 0) continue; ?>
                                <tr>
                                    <td><?=$cnt++.'.'?></td>
                                    <td><?=$inactive_customer->first_name." ".$inactive_customer->last_name;?></td>
                                    <td><?=$inactive_customer->username?></td>
                                    <td><?=$inactive_customer->email?></td>
                                    <td><?=$inactive_customer->phone?></td>
                                    <td><?=($inactive_customer->occupation. ' (' . $inactive_customer->organization . ')')?></td>
                                    <td nowrap="">
                                        <a href="<?=admin_url('users/customers/deactivate/'.url_encrypt($inactive_customer->id))?>" class="btn btn-xs btn-danger tooltip-danger confirmDiag" data-toggle="tooltip" data-confirmbtn="Yes, re-activate this user!" data-msg="Are you sure?" title="re-activate">
                                            <span class="fa fa-refresh"></span>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach;
                            if ($cnt == 1): ?>
                                <tr>
                                    <td colspan="8">No record found!</td>
                                </tr>
                            <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /In-active Users -->
            </div>
        </div>

    </div>
</div>

