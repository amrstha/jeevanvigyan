<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/17/17
 * Time: 1:26 PM
 */
?>

<div class="row">
    <div class="col-sm-12">

        <?php
        $attributes = [
            'class' => 'form-horizontal',
            'role'  => 'form',
            'name'  => 'popup_form',
            'id'    => 'popup_form'
        ];

        $url = 'users/';
        if(isset($edit)) $url .= 'edit/'.url_encrypt($edit->id);
        else if(isset($editaccount)) $url .= 'editaccount/'.url_encrypt($editaccount->id);
        else if(isset($editpassword)) $url .= 'editpassword/'.url_encrypt($editpassword->id);
        else if(isset($editusergroup)) $url .= 'editgroup/'.url_encrypt($editusergroup->id);
        else $url .= 'add';

        echo form_open(admin_url($url), $attributes);
        ?>

        <?php if(!isset($editaccount) && !isset($editpassword) && !isset($editusergroup)): ?>
        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="first_name">First Name:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control required" name="first_name" id="first_name" value="<?php echo (isset($edit) ? $edit->first_name : '') ?>">
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>
        <?php endif ?>

        <?php if(!isset($editaccount) && !isset($editpassword) && !isset($editusergroup)): ?>
        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="last_name">Last Name:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control required" name="last_name" id="last_name" value="<?php echo (isset($edit) ? $edit->last_name : '') ?>">
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>
        <?php endif ?>

        <?php if(!isset($edit) && !isset($editpassword)): ?>
        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="username">Username:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control required" name="username" id="username" value="<?php echo (isset($editaccount) ? $editaccount->username : '') ?><?php echo (isset($editusergroup) ? $editusergroup->username : '') ?>" <?php echo isset($editusergroup) ? 'disabled' : '' ?>>
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>
        <?php endif ?>

        <?php if(!isset($edit) && !isset($editpassword) && !isset($editusergroup)): ?>
        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="email">Email:</label>
            <div class="col-sm-10">
                <input type="email" class="form-control required" name="email" id="email" value="<?php echo (isset($editaccount) ? $editaccount->email : '') ?>">
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>
        <?php endif ?>

        <?php if(!isset($edit) && !isset($editaccount) && !isset($editusergroup)): ?>
        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="password">Password:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control required" name="password" id="password" value="">
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>
        <?php endif ?>

        <?php if(!isset($edit) && !isset($editaccount) && !isset($editpassword)): ?>
        <div class="form-group has-feedback">
            <label class="col-sm-2 control-label" for="group">User Group:</label>
            <div class="col-sm-10">
                <select multiple="" class="form-control chosen-select tag-input-style required array select" name="group" id="group" data-placeholder="Select user groups">
                    <option value=""></option>
                    <?php foreach($groups as $group): ?>
                        <option value="<?=$group->id?>" <?=(in_array($group->id, $ugroups) ? 'selected' : '')?>><?=$group->name?></option>
                    <?php endforeach ?>
                </select>
                <span class="form-msg" style="display: none"></span>
            </div>
        </div>
        <div class="hr-line-dashed"></div>
        <?php endif ?>

        <?=form_close()?>

    </div>
</div>

<script type="text/javascript">
    $('.chosen-select').chosen();
    $(document).find('.chosen-container').each(function(){
        $(this).find('.chosen-choices').parent().css('width' , '100%');
    });
</script>
