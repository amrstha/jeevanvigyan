<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/16/17
 * Time: 11:10 PM
 */
?>

<div class="x_panel">
    <div class="x_title">
        <h2><?=$pageDetail->subTitle?></h2>
        <div class="pull-right">
            <a href="<?=admin_url('users/groups/add')?>" class="btn btn-success btn-xs tooltip-success loadForm" data-toggle="tooltip" data-successbtn="Add" data-addclass="bootbox-modal-xs" title="Add New User Group">
                <i class="fa fa-plus"></i>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

        <div class="table-responsive">
            <table class="table table-bordered table-hover data-table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($groups as $group): ?>
                        <tr>
                            <td>
                                <?php echo $group->name ?>
                                <span class="label label-default">id:<?php echo $group->id ?></span>
                            </td>
                            <td><?php echo $group->description ?></td>
                            <td nowrap="">
                                <?php if($group->is_editable === '1'): ?>
                                    <a href="<?=admin_url('users/groups/edit/'.url_encrypt($group->id))?>" class="btn btn-xs btn-info tooltip-info loadForm" data-toggle="tooltip" data-successbtn="Update" data-addclass="bootbox-modal-xs" title="edit">
                                        <span class="fa fa-edit"></span>
                                    </a>

                                    <a href="<?=admin_url('users/groups/delete/'.url_encrypt($group->id))?>" class="btn btn-xs btn-danger tooltip-danger confirmDiag" data-toggle="tooltip" data-confirmbtn="Yes, delete it!" data-msg="Are you sure? Once confirm it cannot be undone." title="delete">
                                        <span class="fa fa-remove"></span>
                                    </a>
                                <?php endif ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    <?php if(count($groups) == 0): ?>
                        <tr>
                            <td colspan="4">
                                No groups has been created yet.
                            </td>
                        </tr>
                    <?php endif ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
