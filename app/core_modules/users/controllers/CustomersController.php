<?php
class CustomersController extends Admin_Controller
{
	public function __construct()
	{
		parent::__construct();
	
		self::$viewData['pageInfo'] = (object)[
		'title' => $this->lang->line('users_module')
		];
		$this->breadcrumb->append('User Management', admin_url('users'));
	}
	
	public function lists(){
		$this->breadcrumb->append('Customers', admin_url('users/customers/lists'));
		
		self::$viewData['pageDetail'] = (object)[
		'title'     => 'User Managment : Customers',
		'subTitle'  => 'List of Customers'
				];
		$this->authentication->setToCustomer();
		self::$viewData['active_customers'] = $this->authentication->getAllCustomer("active");
		self::$viewData['inactive_customers'] = $this->authentication->getAllCustomer("inactive");
		$this->authentication->setToAdmin();
		$this->load->admintheme('users/customers/lists', self::$viewData);
	}
	
	public function edit()
	{
		if (!$this->input->is_ajax_request()) {
			show_error($this->lang->line('direct_scripts_access'));
		}
	
		$id = Utility::getIdFromUri(5);
		if ($id === false) {
			$this->output->json([
					'status' 	=> 'error',
					'data'		=> NULL,
					'message'	=> $this->lang->line('invalid_request')
					]);
		}
	
		$response = [];
		if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {
	
			$this->form_validation->set_rules('first_name', 'First Name', 'required');
			$this->form_validation->set_rules('last_name', 'Last Name', 'required');
	
			if($this->form_validation->run() === false) {
				// validation failed
				$response = Utility::getFormError();
			} else {
				// validation success
	
				// Save Information in database
				$userInfo = [
				'first_name' => $this->input->post('first_name'),
				'last_name'	=> $this->input->post('last_name')
				];
				
				$this->authentication->setToCustomer();
				$result = $this->authentication->Update($id, $userInfo);
				$this->authentication->setToAdmin();
				if(!$result) {
					$response = [
					'status' 	=> 'error',
					'data'		=> NULL,
					'message'	=> $this->lang->line('nothing_updated')
					];
				} else {
					$this->session->set_notification('success', $this->lang->line('success_updated'));
					$response = [
					'status' 	=> 'success',
					'data'		=> NULL,
					'message'	=> 'success'
							];
				}
			}
	
		} else {
			// load form
			$this->authentication->setToCustomer();
			self::$viewData['edit'] = $this->authentication->getUser($id);
			$this->authentication->setToAdmin();
			$form = $this->load->view('users/customers/form', self::$viewData, true);
			$response = [
			'status' 	=> 'success',
			'data'		=> null,
			'message'	=> $form
			];
		}
	
		$this->output->json($response);
	}
	
	public function editaccount(){
		
		if (!$this->input->is_ajax_request()) {
			show_error($this->lang->line('direct_scripts_access'));
		}
		
		$id = Utility::getIdFromUri(5);
		
		if ($id === false) {
			$this->output->json([
					'status' 	=> 'error',
					'data'		=> NULL,
					'message'	=> $this->lang->line('invalid_request')
					]);
		}
		$response = [];
		if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {
			
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			
			if($this->form_validation->run() === false) {
				// validation failed
				$response = Utility::getFormError();
			}else{
				// validation success
				
				// Save Information in database
				$userInfo = [
				'email'     => $this->input->post('email'),
				'username'	=> $this->input->post('username')
				];
				$this->authentication->setToCustomer();
				$result = $this->authentication->Update($id, $userInfo);
				$this->authentication->setToAdmin();
				
				if(!$result) {
					$response = [
					'status' 	=> 'error',
					'data'		=> NULL,
					'message'	=> $this->lang->line('nothing_updated')
					];
				}else {
                    $this->session->set_notification('success', $this->lang->line('success_updated'));
                    $response = [
                        'status' 	=> 'success',
                        'data'		=> NULL,
                        'message'	=> 'success'
                    ];
                }
			}
			
		}else{
			
			// load form
				$this->authentication->setToCustomer();
				self::$viewData['editaccount'] = $this->authentication->getUser($id);
				$this->authentication->setToAdmin();
				$form = $this->load->view('users/customers/form', self::$viewData, true);
				$response = [
				'status' 	=> 'success',
				'data'		=> null,
				'message'	=> $form
				];
		}
		
		$this->output->json($response);
	}
	
	public function editpassword(){
		
		if (!$this->input->is_ajax_request()) {
			show_error($this->lang->line('direct_scripts_access'));
		}
		
		$id = Utility::getIdFromUri(5);
		if ($id === false) {
			$this->output->json([
					'status' 	=> 'error',
					'data'		=> NULL,
					'message'	=> $this->lang->line('invalid_request')
					]);
		}
		$response = [];
		if(strtolower($this->input->server("REQUEST_METHOD"))== "post"){
			
			$this->form_validation->set_rules('password', 'Password', 'required|min_length['. $this->config->item('min_password_length', 'ion_auth') .']');
			
			if($this->form_validation->run() == false){
				// validation failed
				$response = Utility::getFormError();
			}else{
				//validation success
				
				//Save Information in databse
				$userInfo = [
				'password'     => $this->input->post('password')
				];
				
				$this->authentication->setToCustomer();
				$result = $this->authentication->Update($id, $userInfo);
				$this->authentication->setToAdmin();
				
				if(!$result) {
					$response = [
					'status' 	=> 'error',
					'data'		=> NULL,
					'message'	=> $this->lang->line('nothing_updated')
					];
				}else {
					$this->session->set_notification('success', $this->lang->line('success_updated'));
					$response = [
					'status' 	=> 'success',
					'data'		=> NULL,
					'message'	=> 'success'
							];
				}
			}
		}else{
			
			$this->authentication->setToCustomer();
			self::$viewData['editpassword'] = $this->authentication->getUser($id);
			$this->authentication->setToAdmin();
			$form = $this->load->view('users/customers/form', self::$viewData, true);
			$response = [
			'status' 	=> 'success',
			'data'		=> null,
			'message'	=> $form
			];
		}
		 $this->output->json($response);
	}
	
	public function deactivate(){
		
		$id = Utility::getIdFromUri(5);
		
		if ($id === false) {
			$this->session->set_notification('error', $this->lang->line('invalid_request'));
		}else {
			$this->authentication->setToCustomer();
            if( ! $this->authentication->changeUserActiveStatus($id) ) {
                $this->session->set_notification('error', $this->lang->line('error_delete'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_delete'));
            }
            $this->authentication->setToAdmin();
        }
        admin_redirect('users/customers/lists', 'refresh');
	}
}