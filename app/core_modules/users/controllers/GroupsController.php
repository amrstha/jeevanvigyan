<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/16/17
 * Time: 10:58 PM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class GroupsController extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('users_module')
        ];
        $this->breadcrumb->append('User Management', admin_url('users'));
    }

    public function index()
    {
        $this->lists();
    }

    public function lists()
    {
        $this->breadcrumb->append('Groups', admin_url('users/groups'));

        self::$viewData['pageDetail'] = (object)[
            'title'     => 'User Managment : Groups',
            'subTitle'  => 'List of User Groups'
        ];

        self::$viewData['groups'] = $this->authentication->groups();
        $this->load->admintheme('groups/lists', self::$viewData);
    }

    public function add()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $response = [];
        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('name', 'Group Name', 'required');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Save Information in database
                $insertData = [
                    'name'			=> $this->input->post('name'),
                    'description'	=> $this->input->post('description')
                ];

                $result = $this->authentication->createGroup($insertData);
                if(!$result) {
                    $response = [
                        'status' 	=> 'error',
                        'data'		=> NULL,
                        'message'	=> $this->lang->line('error_added')
                    ];
                } else {
                    $this->session->set_notification('success', $this->lang->line('success_added'));
                    $response = [
                        'status' 	=> 'success',
                        'data'		=> NULL,
                        'message'	=> 'success'
                    ];
                }
            }

        } else {
            // load form
            $form = $this->load->view('groups/form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function edit()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $id = Utility::getIdFromUri(5);
        if ($id === false) {
            $this->output->json([
                'status' 	=> 'error',
                'data'		=> NULL,
                'message'	=> $this->lang->line('invalid_request')
            ]);
        }

        $response = [];
        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('name', 'Group Name', 'required');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Save Information in database
                $updateData = [
                    'name'			=> $this->input->post('name'),
                    'description'	=> $this->input->post('description')
                ];

                $result = $this->authentication->updateGroup($id, $updateData);
                if(!$result) {
                    $response = [
                        'status' 	=> 'error',
                        'data'		=> NULL,
                        'message'	=> $this->lang->line('nothing_updated')
                    ];
                } else {
                    $this->session->set_notification('success', $this->lang->line('success_updated'));
                    $response = [
                        'status' 	=> 'success',
                        'data'		=> NULL,
                        'message'	=> 'success'
                    ];
                }
            }

        } else {
            // load form
            self::$viewData['edit'] = $this->authentication->groupById($id);
            $form = $this->load->view('groups/form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function delete()
    {
        $id = Utility::getIdFromUri(5);

        if ($id === false || $this->authentication->isGroupEditable($id) === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if( ! $this->authentication->deleteGroup($id) ) {
                $this->session->set_notification('error', $this->lang->line('error_delete'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_delete'));
            }
        }
        admin_redirect('users/groups/lists', 'refresh');
    }
}