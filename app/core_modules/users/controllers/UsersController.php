<?php

/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/17/17
 * Time: 1:55 AM
 */
(defined('BASEPATH')) || exit('No direct script access allowed');

class UsersController extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        self::$viewData['pageInfo'] = (object)[
            'title' => $this->lang->line('users_module')
        ];
        $this->breadcrumb->append('User Management', admin_url('users'));
    }

    public function index()
    {
        $this->lists();
    }

    public function lists()
    {
        $this->breadcrumb->append('Users', admin_url('users/lists'));

        self::$viewData['pageDetail'] = (object)[
            'title'     => 'User Managment : Users',
            'subTitle'  => 'List of Users'
        ];

        self::$viewData['users'] = $this->authentication->getAllUsers();
        $this->load->admintheme('users/lists', self::$viewData);
    }

    public function add()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $response = [];
        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('first_name', 'First Name', 'required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required');
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required|min_length['. $this->config->item('min_password_length', 'ion_auth') .']', [
                'min_length'    => 'Password must be '. $this->config->item('min_password_length', 'ion_auth') .' character long.'
            ]);

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Save Information in database
                $userData = [
                    'first_name'	=> $this->input->post('first_name'),
                    'last_name'		=> $this->input->post('last_name')
                ];
                $username = $this->input->post('username');
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                $group = $this->input->post('group');
                $group = explode(',', $group);

                $result = $this->authentication->register($username, $password, $email, $userData, $group);
                if(!$result) {
                    $response = [
                        'status' 	=> 'error',
                        'data'		=> NULL,
                        'message'	=> $this->lang->line('error_added')
                    ];
                } else {
                    $this->session->set_notification('success', $this->lang->line('success_added'));
                    $response = [
                        'status' 	=> 'success',
                        'data'		=> NULL,
                        'message'	=> 'success'
                    ];
                }
            }

        } else {
            // load form
            self::$viewData['groups'] = $this->authentication->groups(true);
            $form = $this->load->view('form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function edit()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $id = Utility::getIdFromUri(4);
        if ($id === false) {
            $this->output->json([
                'status' 	=> 'error',
                'data'		=> NULL,
                'message'	=> $this->lang->line('invalid_request')
            ]);
        }

        $response = [];
        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('first_name', 'First Name', 'required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'required');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Save Information in database
                $userInfo = [
                    'first_name' => $this->input->post('first_name'),
                    'last_name'	=> $this->input->post('last_name')
                ];

                $result = $this->authentication->Update($id, $userInfo);
                if(!$result) {
                    $response = [
                        'status' 	=> 'error',
                        'data'		=> NULL,
                        'message'	=> $this->lang->line('nothing_updated')
                    ];
                } else {
                    $this->session->set_notification('success', $this->lang->line('success_updated'));
                    $response = [
                        'status' 	=> 'success',
                        'data'		=> NULL,
                        'message'	=> 'success'
                    ];
                }
            }

        } else {
            // load form
            self::$viewData['edit'] = $this->authentication->getUser($id);
            $form = $this->load->view('form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function editaccount()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $id = Utility::getIdFromUri(4);
        if ($id === false) {
            $this->output->json([
                'status' 	=> 'error',
                'data'		=> NULL,
                'message'	=> $this->lang->line('invalid_request')
            ]);
        }

        $response = [];
        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Save Information in database
                $userInfo = [
                    'email'     => $this->input->post('email'),
                    'username'	=> $this->input->post('username')
                ];

                $result = $this->authentication->Update($id, $userInfo);
                if(!$result) {
                    $response = [
                        'status' 	=> 'error',
                        'data'		=> NULL,
                        'message'	=> $this->lang->line('nothing_updated')
                    ];
                } else {
                    $this->session->set_notification('success', $this->lang->line('success_updated'));
                    $response = [
                        'status' 	=> 'success',
                        'data'		=> NULL,
                        'message'	=> 'success'
                    ];
                }
            }

        } else {
            // load form
            self::$viewData['editaccount'] = $this->authentication->getUser($id);
            $form = $this->load->view('form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function editpassword()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $id = Utility::getIdFromUri(4);
        if ($id === false) {
            $this->output->json([
                'status' 	=> 'error',
                'data'		=> NULL,
                'message'	=> $this->lang->line('invalid_request')
            ]);
        }

        $response = [];
        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('password', 'Password', 'required|min_length['. $this->config->item('min_password_length', 'ion_auth') .']', [
                'min_length'    => 'Password must be '. $this->config->item('min_password_length', 'ion_auth') .' character long.'
            ]);

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Save Information in database
                $userInfo = [
                    'password' => $this->input->post('password')
                ];

                $result = $this->authentication->Update($id, $userInfo);
                if(!$result) {
                    $response = [
                        'status' 	=> 'error',
                        'data'		=> NULL,
                        'message'	=> $this->lang->line('nothing_updated')
                    ];
                } else {
                    $this->session->set_notification('success', $this->lang->line('success_updated'));
                    $response = [
                        'status' 	=> 'success',
                        'data'		=> NULL,
                        'message'	=> 'success'
                    ];
                }
            }

        } else {
            // load form
            self::$viewData['editpassword'] = $this->authentication->getUser($id);
            $form = $this->load->view('form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function editgroup()
    {
        if (!$this->input->is_ajax_request()) {
            show_error($this->lang->line('direct_scripts_access'));
        }

        $id = Utility::getIdFromUri(4);
        if ($id === false) {
            $this->output->json([
                'status' 	=> 'error',
                'data'		=> NULL,
                'message'	=> $this->lang->line('invalid_request')
            ]);
        }

        $response = [];
        if (strtolower($this->input->server('REQUEST_METHOD')) == 'post') {

            $this->form_validation->set_rules('group', 'User Group', 'prep_for_form|strip_image_tags|encode_php_tags');

            if($this->form_validation->run() === false) {
                // validation failed
                $response = Utility::getFormError();
            } else {
                // validation success

                // Save Information in database
                $groups = $this->input->post('group');
                $groups = explode(',', $groups);

                $result = $this->authentication->updateUserGroup($id, $groups);
                if(!$result) {
                    $response = [
                        'status' 	=> 'error',
                        'data'		=> NULL,
                        'message'	=> $this->lang->line('nothing_updated')
                    ];
                } else {
                    $this->session->set_notification('success', $this->lang->line('success_updated'));
                    $response = [
                        'status' 	=> 'success',
                        'data'		=> NULL,
                        'message'	=> 'success'
                    ];
                }
            }

        } else {
            // load form
            self::$viewData['groups'] = $this->authentication->groups(true);
            self::$viewData['editusergroup'] = $this->authentication->getUser($id);

            $userGroup = $this->authentication->getUserGroup($id);
            $ugroups = array();
            foreach($userGroup as $group) {
                $ugroups[] = $group->id;
            }
            self::$viewData['ugroups'] = $ugroups;

            $form = $this->load->view('form', self::$viewData, true);
            $response = [
                'status' 	=> 'success',
                'data'		=> null,
                'message'	=> $form
            ];
        }

        $this->output->json($response);
    }

    public function deactivate()
    {
        $id = Utility::getIdFromUri(4);
        if ($id === false || $this->authentication->isUserDeletable($id) === false) {
            $this->session->set_notification('error', $this->lang->line('invalid_request'));
        } else {
            if( ! $this->authentication->changeUserActiveStatus($id) ) {
                $this->session->set_notification('error', $this->lang->line('error_delete'));
            } else {
                $this->session->set_notification('success', $this->lang->line('success_delete'));
            }
        }
        admin_redirect('users/lists', 'refresh');
    }
}