<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/21/17
 * Time: 1:15 AM
 */
?>
<script src="<?=assets('bower_components/jquery/dist/jquery.min.js')?>"></script>
<script src="<?=assets('bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<script src="<?=assets('bower_components/jquery.marquee/jquery.marquee.js')?>"></script>

<script type="text/javascript">
    "use strict";

    // Config
    var base_url = "<?=base_url()?>";
    var current_url = "<?=current_url()?>";
    var current_get_data = "<?=http_build_query($this->input->get())?>";
    var csrf_token_name = "<?=$this->security->get_csrf_token_name()?>";
    var csrf_token = "<?=$this->security->get_csrf_hash()?>";
    var pageTitle = $(document).find("title").text();

</script>
