<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/21/17
 * Time: 1:31 PM
 */
?>

<div class="container">
    <div class="row">

        <!-- Slider -->
        <div class="col-sm-12 col-md-9">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <?php $cnt = 0; foreach ($sliders as $slider): ?>
                        <div class="item <?=($cnt++ === 0 ? 'active' : '')?>">
                            <img src="<?=base_url(Utility::getUploadDir('slider') . $slider->image)?>" alt="<?=($lang === 'np' ? $slider->title_np : $slider->title)?>" class="img-responsive">
                            <div class="carousel-caption">
                                <h2  style="text-align:left;padding-top: 120px;"><?=($lang === 'np' ? $slider->title_np : $slider->title)?></h2>
                            </div>
                        </div>
                    <?php endforeach;
                    if (count($sliders) == 0):
                    ?>
                    <div class="item active">
                        <img src="<?=assets('images/placeholder-1000x600.png', 'front')?>" alt="home-page-slider-placeholder" class="img-responsive">
                    </div>
                    <?php endif ?>
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>
        </div>
        <!-- /Slider -->

        <!-- Navigation -->
        <div class="col-sm-12 col-sm-3">
            <div class="c-production">
                <div class="c-production__box">
                    <a href="<?=front_url('videos')?>" class="odd"><img src="<?=assets('images/video.png', 'front')?>"><h4> <?=($lang=='np'?'हाम्रो भीडीयोहरु हेर्नुहोस':'Watch our Videos') ?></h4></a>
                </div>
                <div class="c-production__box">
                    <a href="<?=front_url('blog')?>" class="even"><img src="<?=assets('images/book.png', 'front')?>"><h4> <?=($lang=='np'?'पुस्तक र लेख पढ्नुहोस्':'Read Books and Articles') ?></h4></a>
                </div>
                <div class="c-production__box">
                    <a href="<?=front_url('audios')?>" class="odd"><img src="<?=assets('images/song.png', 'front')?>"><h4> <?=($lang=='np'?'अडियो र भाषण सुन्नुहोस्':'Listen to Audios and Discourse' )?></h4></a>
                </div>
                <div class="c-production__box">
                    <a href="<?=front_url('news')?>" class="even"><img src="<?=assets('images/news.png', 'front')?>"><h4> <?=($lang=='np'?'हाम्रो ताजा समाचार पढ्नुहोस्':'Read Our Latest News' )?></h4></a>
                </div>
                <div class="c-production__box">
                    <a href="<?=front_url('event')?>" id="upcoming-event" class="odd"><img src="<?=assets('images/event.png', 'front')?>"><h4> <?=($lang=='np'?'आगामी घटनाक्रम':'Upcoming Events') ?></h4></a>
                </div>
<!--                <div class="c-booking">-->
<!--                    <h4>Want to attend our life changing programs?</h4><br/>-->
<!--                    <a href="#" class="btn btn-primary booking-btn"> Book Now</a>-->
<!--                </div>-->
            </div>
        </div>
        <!-- /Navigation -->

    </div>
</div>
