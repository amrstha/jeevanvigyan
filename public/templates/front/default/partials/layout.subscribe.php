<!-- Subscribe and Downloads -->
<section class="c-subscribe" style="text-align: center; align-content: center">
    <div class="container">
        <div class="row">

            <!-- Subscription -->
            <div class="col-sm-12 col-md-12">
                <div class="newsletter">
                    <?=alert()?>
                    <h2><?= ($lang=='np' ? 'हाम्रो न्यूजलेटरको सदस्यता लिनुहोस्': 'Subscribe to our newsletter') ?></h2>
                    <span><?= ($lang=='np' ? 'कृपया जीवन बिज्ञान केन्द्रको न्यूजलेटरकोको लागि सदस्यता लिनुहोस्': 'Please add you for newsletter of Jeevan Vigyan Kendra') ?></span>
                        <?php

                        $attributes = [
                            'class' => 'form row',
                            'role'  => 'form',
                            'name'  => 'subscribe_form',
                            'id'    => 'subscribe-form'
                        ];
                        $action = front_url('subscribers/add');
                        echo form_open($action, $attributes);

                        ?>
                        <input type="hidden" name="current_url" value="<?=current_url()?>">

                        <div class="col-sm-12 col-md-4 col-md-offset-3">
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="<?= ($lang=='np'?'इमेल':'Email') ?>">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-2">
                            <button type="submit" value="submit" class="btn btn-subscribe"><?= ($lang=='np'?'सदस्यता लिनुहोस्':'Subscribe') ?></button>
                        </div>
                        <?=form_close()?>
                </div>
            </div>
            <!-- /Subscription -->

            <!-- Download Links -->
<!--            <div class="col-sm-4">-->
<!--                <div class="download">-->
<!--                    <h3>Download</h3>-->
<!--                    <span>You can get the materialsof kendra</span>-->
<!--                    <ul>-->
<!--                        <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Radio Program</a></li>-->
<!--                        <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Songs</a></li>-->
<!--                        <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Videos</a></li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--            </div>-->
            <!-- Download Links -->

        </div>
    </div>
</section>
<!-- /Subscribe -->