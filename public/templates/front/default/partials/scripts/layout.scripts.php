<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/21/17
 * Time: 1:15 AM
 */
$this->load->view(FRONT_TMPL.'/partials/global/scripts') ?>

<!-- Vendor scripts -->

<!-- App scripts -->
<script src="<?=assets('theme/js/app.js', 'front')?>"></script>
<script src="<?=assets('custom/js/layout.main.js', 'front')?>"></script>
<script src="<?=assets('bower_components/lightbox2/dist/js/lightbox.js')?>"></script>
<script src="<?=assets('theme/js/waterwheelCarousel.min.js', 'front')?>"></script>


<!-- Page Specific Scripts -->

<script type="text/javascript">

    $(function () {
        var language = '<?php echo $lang; ?>' ;
        if(language == 'en'){
            $('h2, span, p, a, li, button, h3, h4, strong').css('font-style','normal');
        }


    });



</script>