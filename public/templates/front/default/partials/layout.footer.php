<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/21/17
 * Time: 1:38 PM
 */
?>
<!-- Address -->
<div class="c-footer__top text-center">
    <div class="container">
        <div class="row">

            <div class="col-sm-4">
                <img src="<?= assets('images/location.png', 'front') ?>">
                <ul>
                    <li><?= ($lang=='np'? 'गणेश मार्ग ( गणेश मन्दिरको नजिकै )':'Ganesh Marga (Near Ganesh Temple)') ?></li>
                    <li><?= ($lang=='np'? 'चाबहिल, काठमाडौँ':'Chabahil, Kathmandu') ?></li>
                </ul>
            </div>

            <div class="col-sm-4">
                <img src="<?= assets('images/phone.png', 'front') ?>">
                <ul>
                    <li><a href="tel:97714472830">+977-1-4472830</a></li>
                </ul>
            </div>

            <div class="col-sm-4">
                <img src="<?= assets('images/mail.png', 'front') ?>">
                <li>
                <li><?= safe_mailto('info@jeevanvigyan.com', 'info@jeevanvigyan.com') ?></li>
                </ul>
            </div>

        </div>
    </div>
</div>
<!-- /Address -->

<!-- Social -->
<div class="social">
    <div class="container">
        <div class="row">
            <ul class="footer__social">
                <?php
                $socialMenus = [];
                $socialMenus = $this->social->getAll(true);
                foreach ($socialMenus as $socialMenu):
                    ?>
                    <li>
                        <a href="<?= $socialMenu->link ?>" class="<?= $socialMenu->title ?>" rel="noopener noreferrer"
                           target="_blank"><img
                                    src="<?= base_url(Utility::getUploadDir('menu_social') . $socialMenu->icon) ?>"
                                    alt="<?= $socialMenu->name ?>"></a>

                    </li>

                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
<!-- /Social -->

<!-- Footer Navigation -->
<div class="c-footer__bottom text-center">
    <div class="container">
        <div class="row">
            <!-- <div class="logo">
                <img src="images/logo-footer.png" alt="Jeevan Vigyan">
            </div> -->
            <ul class="footer__nav">
                <li><a href="<?= front_url('home') ?>"><?=($lang === 'np' ? 'होम' : 'Home')?></a></li>
                <li><a href="about"><?=($lang === 'np' ? ' हाम्रो बारेमा' : 'About')?></a></li>
<!--                <li><a href="#">--><?//=front_url(($lang === 'np' ? 'प्रशिक्षण' : 'Training'))?><!--</a></li>-->
                <li><a href="<?= front_url('videos')?>"><?=($lang === 'np' ? 'भिडियोहरू' : 'Videos')?></a></li>
                <li><a href="<?= front_url('news')?>"><?=($lang === 'np' ? 'समाचार' : 'News')?></a></li>
                <li><a href="<?= front_url('audios')?>"><?=($lang === 'np' ? 'अडियो' : 'Audios')?></a></li>
                <li><a href="<?= front_url('contact')?>"><?=($lang === 'np' ? 'सम्पर्क गर्नुहोस्' : 'Contact Us')?></a></li>
                <li><a href="<?= front_url('privacy')?>"><?=($lang === 'np' ? 'Privacy Policy' : 'Privacy Policy')?></a></li>
            </ul>
            <p class="copyright">&copy; <?= date('Y') ?> Jeevan Vigyan Kendra. All rights reserved</p>

        </div>
    </div>
</div>

<!-- Footer Navigation -->