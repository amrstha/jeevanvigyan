<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/21/17
 * Time: 12:53 AM
 */
$viewData = [];
?>
<?php theme('partials', 'htmlhead', $viewData) ?>
<body>

    <!-- Header -->
    <header class="l-header">
        <?php theme('partials', 'header', $viewData) ?>
    </header>

    <main class="l-main">

        <!-- Introduction -->
        <section class="c-intro" style="margin-bottom: 0px; background-color: #e2e2e2">
            <div class="container">
                <?=$main_body_content?>
            </div>
        </section>
        <!-- /Introduction -->

        <!-- Subscribe and Downloads -->
        <?php theme('partials', 'subscribe', $viewData) ?>
        <!-- /Subscribe -->

    </main>

    <!-- Footer -->
    <footer class="l-footer">
        <?php theme('partials', 'footer', $viewData) ?>
    </footer>
    <!-- Footer -->

<!--    <div class="l-question">-->
<!--        <a href="#">Question Answer <i class="fa fa-chevron-circle-down" aria-hidden="true"></i></a>-->
<!--    </div>-->

    <div class="top pull-right">
        <a href="#"><img src="<?=assets('images/top.png', 'front')?>" alt="to top"></a>
    </div>

    <!--
	// SCRIPTS
	-->
    <?php theme('partials/scripts', 'scripts', $viewData) ?>

</body>
</html>
