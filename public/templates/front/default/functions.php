<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/21/17
 * Time: 12:52 AM
 */

(defined('BASEPATH')) || exit('No direct script access allowed');

if (!function_exists('pagination')) {
    function pagination($base_url, $total_rows, $per_page = 5, $uri_segment = 3, $use_page_numbers = FALSE, $attributes = array('class' => 'pagination-links'))
    {
        if ($base_url != NULL) {
            $config['base_url'] = $base_url;
            $config['total_rows'] = $total_rows;
            $config['per_page'] = $per_page;
            $config['uri_segment'] = $uri_segment;
            $config['use_page_numbers'] = $use_page_numbers;
            $config['attributes'] = $attributes;

            return $config;
        }

        return false;

    }
}

if (!function_exists('theme')) {
    function theme($dir, $page, $data = [], $return = false, $ext = '.php', $theme = 'layout')
    {
        $page = '/' . (!is_null($dir) ? $dir . '/' : '') . (!is_null($theme) ? $theme . '.' : '') . $page . (!is_null($ext) ? $ext : '');
        return get_instance()->load->view(FRONT_TMPL . $page, $data, $return);
    }
}

if (!function_exists('excerpt')) {
    function excerpt($length, $string)
    {
        return substr(strip_tags($string), 0, $length);
    }
}

if (!function_exists('activeNav')) {
    function activeNav($slug)
    {
        $uri_lvl = uriSegments();

        if (!is_array($slug)) {
            return ($slug == $uri_lvl[0]) ? 'active' : false;
        }

        foreach ($slug as $key => $value) {
            if ($value != $uri_lvl[$key]) {
                return false;
            }
        }
        return 'active';
    }
}

if (!function_exists('uriSegments')) {
    function uriSegments()
    {
        $CI =& get_instance();
        $first = $CI->uri->segment(1);
        return [
            empty($first) ? 'home' : $first,
            $CI->uri->segment(2),
            $CI->uri->segment(3),
            $CI->uri->segment(4),
        ];
    }
}

if (!function_exists('alert')) {
    function alert()
    {
        $CI =& get_instance();
        $success_alert = $CI->session->get_alert('success');
        $warning_alert = $CI->session->get_alert('warning');
        $info_alert = $CI->session->get_alert('info');
        $error_alert = $CI->session->get_alert('error');

        $alert_type = (
        (!empty($success_alert)) ? 'success' : (
        (!empty($warning_alert)) ? 'warning' : (
        (!empty($info_alert)) ? 'info' : (
        (!empty($error_alert)) ? 'danger' : ''
        )
        )
        )
        );

        $alertmsg = '';
        switch ($alert_type) {
            case 'success':
                $alertmsg .= <<<__HTML__
<div class="alert alert-success" role="alert">
    <strong>Success!</strong> {$success_alert}
</div>
__HTML__;
                break;

            case 'info':
                $alertmsg .= <<<__HTML__
<div class="alert alert-info" role="alert">
    <strong>Info!</strong> {$info_alert}
</div>
__HTML__;
                break;

            case 'danger':
                $alertmsg .= <<<__HTML__
<div class="alert alert-danger" role="alert">
    <strong>Error!</strong> {$error_alert}
</div>
__HTML__;
                break;

            case 'warning':
                $alertmsg .= <<<__HTML__
<div class="alert alert-warning" role="alert">
    <strong>Warning!</strong> {$warning_alert}
</div>
__HTML__;
                break;
        }

        return $alertmsg;
    }
}
