<h2 style="color:black;"><?= ($lang === 'np' ? 'ध्यान  विवरण' : 'Meditation Details') ?></h2>
<div class="med" style="background-color: white; overflow:hidden;">
    <h2 style="padding-top:5px;"><?= ($lang === 'np' ? $meditation->title_np : $meditation->title) ?></h2>
    <div class="c-content">
        <div class="" role="tabpanel" data-example-id="togglable-tabs">
            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#images" id="images-tab-btn" role="tab" data-toggle="tab" aria-expanded="true">
                        Article
                    </a>
                </li>
                <li role="presentation" class="">
                    <a href="#video" id="images-tab-btn" role="tab" data-toggle="tab" aria-expanded="true">
                        Videos
                    </a>
                </li>
                <li role="presentation" class="">
                    <a href="#audio" id="images-tab-btn" role="tab" data-toggle="tab" aria-expanded="true">
                        Audios
                    </a>
                </li>
            </ul>
        </div>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="images"
                 aria-labelledby="images-tab-btn">

                <?php $count = 0;
                foreach ($meditationDetails as $details): if ($details->type === 'image') { ?>
                    <div class="col-md-2">
                        <a href="<?= base_url(config_item('file_upload_dir_user')['meditation'] . $details->value) ?>"
                           data-lightbox="image">
                            <img style="width:150px;"
                                 src="<?= base_url(config_item('file_upload_dir_user')['meditation'] . $details->value) ?>"
                                 class="thumbnail">
                        </a>
                    </div>
                    <?php $count++;
                } endforeach; ?>
                <?php if ($count == 0): ?>
                    <div class="alert alert-info" role="alert">No images.</div>
                <?php endif ?>

            </div>
            <div role="tabpanel" class="tab-pane fade" id="video" aria-labelledby="video-tab-btn">
                <br>
                <div class="col-md-2">
                    <div class="videolink">
                        <?php $count = 1;
                        foreach ($meditationDetails as $details): if ($details->type === 'video') { ?>
                            <?= $count ?>. <img height="100px" width="150px"
                                                src="https://i.ytimg.com/vi/<?= $details->value ?>/hqdefault.jpg"
                                                style="margin: 2px;"
                                                data-videoId="<?= $details->value ?>"
                                                class="videoicon">
                            <?php $count++;
                            echo '<br>';

                        } endforeach; ?>
                    </div>
                    <?php if ($count == 1): ?>
                        <div class="alert alert-info" role="alert">No Video.</div>
                    <?php endif ?>
                </div>
                <div class="col-md-9">
                    <div class="video-container">
                        <iframe width="500" height="300"
                                src=""
                                frameborder="0" allowfullscreen
                        >
                        </iframe>
                    </div>
                </div>
                <br>
            </div>

            <div role="tabpanel" class="tab-pane fade" id="audio" aria-labelledby="audio-tab-btn">
                <br>
                <?php $count = 0;
                foreach ($meditationDetails as $details): if ($details->type === 'audio') { ?>
                    <?= $count + 1 ?>.
                    <div class="col-md-12">
                        <audio controls>
                            <source src="<?= base_url(config_item('file_upload_dir_user')['meditation'] . $details->value) ?>"
                                    type="audio/ogg">
                            <source src="<?= base_url(config_item('file_upload_dir_user')['meditation'] . $details->value) ?>"
                                    type="audio/mpeg">
                        </audio>
                    </div>
                    <br>
                    <br>

                    <?php $count++;
                } endforeach; ?>
                <?php if ($count == 0): ?>
                    <div class="alert alert-info" role="alert">No Audio.</div>
                <?php endif ?>

            </div>

        </div>


    </div>
    <br>
</div>
<br>
<div style="text-align:center;color:black; background-color: white">
    <br>
    <?= ($lang === 'np' ? $meditation->description_np : $meditation->description) ?>
    <br>
</div>
