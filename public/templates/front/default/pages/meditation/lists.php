<?php
?>

<div class="row article">
    <main class="col-sm-8">

        <div class="c-heading">
            <h2 style="color:black;"> <?= ($lang === 'np' ? 'ध्यान' : 'Meditation') ?></h2>
        </div>

        <div class="c-content" style="overflow:scroll;height:600px;">
            <?php foreach ($meditations as $meditation): ?>
                <div class="meditation-list">
                    <header class="col-sm-12">
                        <div class="row" style="margin-top: 5px;">
                            <div class="col-sm-12 col-md-12">
                                <div class="col-sm-3 col-md-3">
                                    <p style="font-style:italic;text-align:justify;">
                                        <code> <?= date('j F, Y', strtotime($meditation->created_at)) ?> </code></p>
                                </div>
                                <div class="col-sm-9 col-md-6">
                                    <h2><?= word_limiter(($lang === 'np' ? $meditation->title_np : $meditation->title), 3) ?></h2>
                                </div>
                            </div>
                    </header>
                    <div class="col-sm-4">
                        <img src="<?= base_url(config_item('file_upload_dir_user')['meditation'] . $meditation->image) ?>"
                             class="thumbnail">
                    </div>

                    <div class="col-sm-8">
                        <p><?php echo word_limiter($lang == 'np' ? $meditation->description_np : $meditation->description, 25); ?> </p>
                        <a href="<?= front_url('meditation/view/' . url_encrypt($meditation->id)) ?>"
                           class="btn btn-default btn-xs">read more</a>
                    </div>

<!--                    <div class="clearfix"></div>-->
                </div>
            <?php endforeach;
            if (count($meditations) == 0): ?>
                <div class="alert alert-info" role="alert">No meditations uploaded in list yet.</div>
            <?php endif ?>
        </div>
    </main>

    <aside class="col-sm-4">
        <strong><h2><?= ($lang === 'np' ? 'दैनिक कार्यक्रमहरु' : 'Daily Programmes') ?></h2></strong>
        <hr>
        <div class="">
            <div class="list-group">
                <div class="list-group-item">
                    <p>We offer specialised and unique meditation and spiritual practice programs for advanced seekers
                        of Truth. Within a few days of practice, participants will be able to attain the state of
                        spiritual growth that can otherwise be attained only through years of efforts.</p>
                </div>

                <div class="list-group-item">
                    <p>We offer specialised and unique meditation and spiritual practice programs for advanced seekers
                        of Truth. Within a few days of practice, participants </p>
                </div>

                <div class="list-group-item">
                    <p>We offer specialised and unique meditation and spiritual practice programs for advanced seekers
                        of Truth. Within a few days of practice, participants </p>
                </div>

            </div>
        </div>


    </aside>
</div>
