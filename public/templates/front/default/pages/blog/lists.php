<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 08/01/17
 * Time: 03:55 PM
 */
?>

<div class="row article">
    <main class="col-sm-8">

        <div class="c-heading">
            <h2><?= ($lang === 'np' ? 'ब्लग' : 'Blog') ?></h2>
        </div>

        <div class="c-content">
            <?php foreach ($blog as $bloglist): ?>
                <div class="news-list">
                    <header class="col-sm-12">
                        <h3>
                            <a href="<?= front_url(sprintf("blog/%s", $bloglist->slug)) ?>"><?= ($lang === 'np' ? $bloglist->title_np : $bloglist->title) ?></a>
                        </h3>
                        <time><?= date('Y-m-d', strtotime($bloglist->pub_date)) ?></time>
                    </header>
                    <div class="col-sm-8">
                        <p><?= excerpt(500, $lang === 'np' ? $bloglist->content_np : $bloglist->content) ?> ...</p>
                        <a href="<?= front_url(sprintf("blog/%s", $bloglist->slug)) ?>" class="btn btn-default btn-xs">read
                            more</a>
                    </div>

                    <div class="col-sm-4">
                        <img src="<?= base_url(sprintf("%s/%s", Utility::getUploadDir('pages'), $bloglist->image)) ?>"
                             class="thumbnail">
                    </div>

                    <div class="clearfix"></div>
                </div>
            <?php endforeach; ?>
        </div>


    </main>
    <?php theme('partials', 'sidebar', []) ?>

</div>

<div class="pagination-links pull-left row">
   <?=isset($blogPagination)?$blogPagination:''?>
</div>
