<?php
?>

<div class="row article">
    <main class="col-sm-8">

        <div class="c-heading">
            <h2 style="color:black;"><?= ($lang === 'np' ? 'सामान्यतः सोधिने प्रश्नहरू ' : 'FAQ') ?></h2>
        </div>

        <div class="c-content" style="overflow:scroll;height:600px;">

            <?php if ($lang == 'np') { ?>
                <!-- nepali-panel-group -->
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    <div class="panel panel-default" >
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                   aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    १. ध्यान के हो?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingOne">
                            <div class="panel-body">
                                दैनिक जीवनको भाग–दौडले हाम्रो चित्त बहिर्मुखी भएर हामी आफ्नो स्व–केन्द्रबाट धेरै टाढा
                                पुग्छौं । मनमा उठ्ने कामना, वासना र विचारले चित्तलाई आन्दोलित र बिस्तारै अशान्त बनाउँछ ।
                                ध्यानले हामीलाई स्व–केन्द्रमा फर्काई मन र ह्दयलाई स्थाई रुपमा शान्त र सहज बनाउँछ । ध्यान
                                हाम्रो सहज आन्तरिक स्थिति हो जहाँ शरीर, मन र ह्दय एक गहन विश्रामपूर्ण, सरल र स्वभाविक
                                अवस्थामा रहन्छन् । ध्यानस्थ अवस्थामा हामी आफैभित्र लुकेको अनन्त शान्ति, ज्ञान, सत्य,
                                प्रेम,
                                सम्पन्नता, सौन्दर्य र रहस्यमा डुब्ने र एकाकार भई स्वस्थ, सफल र मस्त जीवन जिउने सूत्र
                                प्राप्त
                                गर्दछौं ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    २. ध्यान कसले गर्न सक्छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingTwo">
                            <div class="panel-body">
                                ध्यान प्रत्येक व्यक्तिले गर्न सक्छ । यो कुनै बाहिरी शिक्षा, प्रेरणा वा शीप नभएर मनुष्यको
                                आन्तरिक प्रयोग भएको हुनाले हरेक उमेर, पेशा, जाति र धर्मको व्यक्तिले गर्न सक्छ ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    ३. ध्यान गर्न कुनै निश्चित आसन या भौतिक परिस्थितिको आवश्यकता पर्छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingThree">
                            <div class="panel-body">
                                शरीरलाई स्थिर र सहज राख्ने जुनसुकै आसनमा ध्यान गर्न सकिन्छ । ध्यान गर्नको लागि अनुकूल
                                मनको
                                स्थितिको विकास गर्नु पर्छ, भौतिक परिस्थितिको ध्यानसँग धेरै सम्बन्ध छैन । त्यस्तै, आफ्नो
                                घर,
                                कार्यालय, प्रार्थनास्थल (मन्दिर, मस्जिद, गुम्बा, गिर्जाघर), ध्यानकेन्द्र, जहाँ पनि ध्यान
                                गर्न सकिन्छ ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFour">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    ४. के ध्यान कुनै निश्चित समयमा मात्र गर्नु पर्छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingFour">
                            <div class="panel-body">
                                दिनको चौबीस घण्टामध्ये आफूलाई अनुकूल हुने कुनै पनि समयमा ध्यान गर्न सकिन्छ । हरेक दिन
                                एउटै
                                समयमा नै ध्यान गर्नु पर्छ भन्ने पनि होईन । साधारणतया, बिहानको समयमा हाम्रो शरीर र मन
                                ध्यानको
                                लागि बढि अनुकूल हुने हुनाले बिहानको समय ध्यानको लागि छुट्याउनु बढि उपयुक्त हुन्छ ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFive">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    ५. के ध्यान कुनै निश्चित धर्म या सम्प्रदायसँग सम्बन्धित छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingFive">
                            <div class="panel-body">
                                ध्यान मनुष्यको आन्तरिक शान्ति, खुशियाली र आनन्दको प्राचीनतम् र आधुनिकतम् विज्ञान हो । यो
                                कुनै धर्म, जाति या सम्प्रदायसँग सम्बन्धित छैन । पृथ्वीमा कुनै धर्म वा सम्प्रदाय प्रारम्भ
                                हुनुभन्दा अघिदेखि नै ध्यान थियो । जुनसुकै धर्मका शिक्षक, गुरु या सिद्ध पुरुषहरुको
                                ज्ञानको
                                स्रोेत पनि ध्यान नै हो ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSix">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    ६. के ध्यान गर्न कुनै परम्परा या गुरुसग दीक्षित हुनु जरुरी छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingSix">
                            <div class="panel-body">
                                ध्यान हामी आफूभित्र नै रहेको चेतनाको परम सम्भावना हो । यसका लागि कोही व्यक्ति, परम्परा
                                या
                                केन्द्रसँग दीक्षित हुनु जरुरी छैन ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSeven">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    ७. ध्यान गर्न कुनै निश्चित विचार, पूर्व मान्यता, धारणा, आस्था या मत आवश्यक छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingSeven">
                            <div class="panel-body">
                                ध्यान विशुद्ध निर्विचार चैतन्यको दशा हो । यो आफूभित्रै रहेको परम सम्भावना उजागर गर्ने
                                विशुद्ध वैज्ञानिक पद्धति हो । सबै मान्यता, धारणा आस्थाबाट मुक्त नभई ध्यानको गहिराईमा
                                प्रवेश
                                गर्न सकिंदैन ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingEight">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    ८. के ध्यान गर्न घर–व्यवहार, कार्य र सम्बन्धबाट अलग्गिनु पर्छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseEight" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingEight">
                            <div class="panel-body">
                                ध्यानले व्यक्तिगत, पारिवारिक र कर्म जीवनमा हामीलाई सहयोग पु¥याउँछ । एउटा ध्यानी व्यक्ति
                                प्रेमले भरिपूर्ण, आफ्नो कर्मप्रति निष्ठावान र व्यवहार कुशल पनि हुन्छ । त्यसकारणले, ध्यान
                                गर्न केही त्याग्नु पर्दैन । वास्तवमा, ध्यानले हाम्रो व्यक्तित्वलाई निखार्छ÷कर्ममा कुशलता
                                ल्याउँछ । यसले पारिवारिक सम्बन्ध अझ सुमधुर बनाउँछ ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingNine">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    ९. ध्यान कसैबाट सिक्नुपर्छ कि आफै गरे पनि हुन्छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseNine" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingNine">
                            <div class="panel-body">
                                ध्यानको बारेमा समाजमा अधिकांश गलत धारणाहरु प्रचलित छन् । यस्तो अवस्थामा हामी आफ्नै ज्ञान
                                र
                                प्रयासबाट मात्र ध्यान सिक्न सक्दैनौं । कोही यस्तो व्यक्तिस“ग ध्यान सिक्न उपयुक्त हुन्छ
                                जो
                                ध्यानको गहिराईमा डुबिसकेको छ र जसले अरुलाई पनि सिकाउन सक्छ । त्यस्तो व्यक्तिसँग ध्यान
                                सिक्नु
                                उपयुक्त हुन्छ । त्यस्तै, ध्यानको क्रममा अनेकौं प्रश्न र संदेहहरू पनि उठ्छन्, जसको
                                निराकरणका
                                लागि पनि ध्यानस्थ आचार्यको सत्संग अपरिहार्य हुन्छ ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTen">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    १०. ध्यानका फाईदाहरु के–के हुन?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTen" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingTen">
                            <div class="panel-body">
                                ध्यान केवल साधन मात्र नभई आफैमा साध्य अवस्था हो । ध्यान मार्ग पनि र गन्तव्य पनि हो ।
                                ध्यान
                                घटित हुनु परम लक्ष्य हो, जीवनको सम्भावनालाई उजागर गरेर आफ्नो गरिमामा प्रतिष्ठित हुनु हो
                                ।
                                छोटकरीमा, ध्यानको उपलब्धि ध्यान नै हो । तथापि, पाठक वर्गको जानकारीका लागि चिकित्सा
                                विज्ञान,
                                मनोविज्ञान, व्यवस्थापन एवं ध्यान केन्द्रहरुको अध्ययन, अनुसन्धान र अनुभवका आधारमा ध्यानका
                                केही प्रमुख फाईदाहरु यहाँ उल्लेख गरिएका छन् ।
                                शारीरिक फाईदाः

                                उच्च रक्तचाप नियन्त्रण, गहन विश्रामको अनुभव, मांसपेसीहरूको तनावबाट मुक्ति, उपचार एवं
                                शल्यचिकित्सा पश्चात्को स्वास्थ्यमा तीव्र सुधार, ऊर्जा एवं शक्तिमा वृद्धि, सक्रियता एवं
                                आयु
                                वृद्धि, ह्दय, पेट एवं शिरको रोगको जोखिममा कटौति, श्वास–प्रश्वासमा सहजता, शरीरको वजनमा
                                नियन्त्रण, स्नायु प्रणालीमा शिथिलता, दुःखाई एवं पीडामा कमी आदि ।
                                मानसिक फाईदाः

                                सोच विचारमा सकारात्मकता, प्रभावकारी एवं सन्तुलित व्यक्तित्वको निर्माण, आत्मविश्वास,
                                उत्साह,
                                आँट एवं जाँगरमा अभिवृद्धि, विचारमाथि नियन्त्रण, सिक्ने क्षमता, स्मरण शक्ति एवं
                                एकाग्रतामा
                                अभिवृद्धि, निर्णय एवं संकल्प क्षमताको विकास, चरित्रमा पवित्रता, नकारात्मक आदत एवं
                                कुलतबाट
                                मुक्ति, अनिद्राबाट मुक्ति, सम्बन्धमा मधुरता, नेतृत्व क्षमता, कार्य सम्पादन क्षमता,
                                जिम्मेवारीपन एवं सामाजिकतामा अभिवृद्ध्रि, कार्यमा कुशलता एवं सन्तुष्टि, मनोरोग जस्तै–
                                चञ्चलता, तनाव, भय, चिन्ता एवं अवसादवाट मुक्ति, आदि ।
                                आध्यात्मिक फाईदाः

                                जीवनमा सार्थकताको अनुभव, आफ्नो उद्देश्य र स्व–यथार्थको ज्ञान, शरीर, मन र चेतनामा
                                सामञ्जस्य,
                                विगतको शोक र भविष्यको चिन्ताबाट मुक्त भई वर्तमानमा क्षण–क्षण जिउने क्षमता प्राप्ति,
                                दिव्य
                                प्रेमको उदय, साक्षी एवं बोधमा प्रतिष्ठा, आत्मज्ञान प्राप्ति एवं परामात्मामय जीवनमा
                                प्रवेश,
                                जीवनको समग्र स्वीकार एवं तथाता, आदि ।

                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingEleven">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    ११. के ध्यान गर्ने व्यक्ति एकोहोरिने या दैनिक जिम्मेवारीबाट भाग्ने स्वभावको हुन्छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseEleven" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingEleven">
                            <div class="panel-body">
                                ध्यानले हाम्रो व्यक्तित्व सन्तुलित बनाउनुका साथै जीवनमा सम्यक्ता ल्याउँछ । आत्म–विश्वास,
                                ऊर्जा र उमंगमा अभिवृद्धि पनि ध्यानका सहज परिणाम भएको हुनाले सही ध्यानले व्यक्ति अझ बढि
                                मिलनसार,व्यवहार कुशल र कर्तव्य परायण हुन्छ ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwelve">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    १२. ध्यान गर्दा विचार या नकारात्मक भावले सताएमा के गर्नु पर्छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwelve" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingTwelve">
                            <div class="panel-body">
                                ध्यान गर्ने व्यक्तिको जीवनमा आउने सर्वा्धिक ठुलो चुनौति अनियन्त्रित विचारको श्रृंखला र
                                नकारात्मक भाव हो । ध्यान भनेकै चित्तमा उठ्ने विचार र ह्रदयका भावहरुलाई जागृत भईं विना
                                कुनै
                                तादात्म्य या निर्णय साक्षीपूर्वक हेर्ने कला हो । अन्नतोगत्वा, ध्यानी व्यक्तिले आफूलाई
                                सम्पूर्ण विचार एवं भावनाहरूबाट पृथक् द्रष्टा एवं बोधको रूपमा अनुभव गर्छ । त्यसकारण,
                                चित्तका
                                हरेक तरंगलाई साक्षीपूर्वक हेर्नु नै पूर्ण समाधान हो ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThirteen">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseThirteen" aria-expanded="false" aria-controls="collapseThirteen">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    १३. ध्यानका चरणहरु के के हुन्?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThirteen" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingThirteen">
                            <div class="panel-body">
                                त्यसो त ध्यान आफैभित्रको दिव्यतामा सिधा छलाड्ड हो । तथापि, ध्यानको क्रममा आउने केही
                                महत्वपूर्ण कोशेढुंगाहरू हुन– पहिलो–द्रष्टाभाव, दोश्रो–साक्षित्व, तेश्रो– बोध । बोधको
                                गहनतामा
                                प्रवेश गरेपछि ध्यानी आफ्नो अन्तर्आकाशमा गुञ्जायमान परमत्व ‘ॐकार’ का विभिन्न आयाममा डुबेर
                                समाधिस्थ हुन्छ । अगाडि आत्मज्ञान, निर्वाण एवं सहजको मार्ग हुँदै सच्चिदानन्दमा विश्राम
                                प्राप्त हुन्छ ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFourteen">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseFourteen" aria-expanded="false" aria-controls="collapseFourteen">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    १४. पूजा, जप, पाठ, प्रार्थना, तीर्थाटन आदिको ध्यानसग के सम्बन्ध छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFourteen" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingFourteen">
                            <div class="panel-body">
                                मनुष्यले गर्ने हरेक प्रयास एवं कर्मको अन्तिम अभिप्राय ध्यानको उपलव्धि हो । ध्यान गर्ने
                                व्यक्तिले पूजा, जप, पाठ लगायत हरेक कर्मलाई ध्यानको गहिराईमा डुब्ने अवसरको रूपमा देख्छ र
                                प्रयोग गर्छ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFifteen">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseFifteen" aria-expanded="false" aria-controls="collapseFifteen">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    १५. के ध्यान सिक्न आफ्नो गुरू, ईष्ट देवता या धर्म परिवर्तन गर्नु आवश्यक छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFifteen" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingFifteen">
                            <div class="panel-body">
                                हामीले ध्यानको माध्यमबाट आफैभित्रको गुरुतत्व र ईष्टसँग परिचित भई एक हुने अवसर प्राप्त
                                गर्छौं
                                । तसर्थ, गुरू, ईष्टदेव या धर्म परिवर्तन गर्ने प्रश्न नै उठ्दैन ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSixteen">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseSixteen" aria-expanded="false" aria-controls="collapseSixteen">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    १६. ध्यान गर्ने व्यक्तिले के के सावधानी अपनाउनु पर्छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSixteen" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingSixteen">
                            <div class="panel-body">
                                ध्यानको कुनै पनि नकारात्मक प्रभाव हुँदैन । ध्यानमा पारंगत आचार्यबाट ध्यान सिकेपछि हामी
                                निश्चिन्त भई ध्यानको अभ्यास गर्न सक्छौं ।
                            </div>
                        </div>
                    </div>

                </div>
            <?php } else { ?>
                <!-- english-panel-group -->
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                   aria-expanded="true" aria-controls="collapseOne">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    १. ध्यान के हो?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingOne">
                            <div class="panel-body">
                                दैनिक जीवनको भाग–दौडले हाम्रो चित्त बहिर्मुखी भएर हामी आफ्नो स्व–केन्द्रबाट धेरै टाढा
                                पुग्छौं । मनमा उठ्ने कामना, वासना र विचारले चित्तलाई आन्दोलित र बिस्तारै अशान्त बनाउँछ ।
                                ध्यानले हामीलाई स्व–केन्द्रमा फर्काई मन र ह्दयलाई स्थाई रुपमा शान्त र सहज बनाउँछ । ध्यान
                                हाम्रो सहज आन्तरिक स्थिति हो जहाँ शरीर, मन र ह्दय एक गहन विश्रामपूर्ण, सरल र स्वभाविक
                                अवस्थामा रहन्छन् । ध्यानस्थ अवस्थामा हामी आफैभित्र लुकेको अनन्त शान्ति, ज्ञान, सत्य,
                                प्रेम,
                                सम्पन्नता, सौन्दर्य र रहस्यमा डुब्ने र एकाकार भई स्वस्थ, सफल र मस्त जीवन जिउने सूत्र
                                प्राप्त
                                गर्दछौं ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    २. ध्यान कसले गर्न सक्छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingTwo">
                            <div class="panel-body">
                                ध्यान प्रत्येक व्यक्तिले गर्न सक्छ । यो कुनै बाहिरी शिक्षा, प्रेरणा वा शीप नभएर मनुष्यको
                                आन्तरिक प्रयोग भएको हुनाले हरेक उमेर, पेशा, जाति र धर्मको व्यक्तिले गर्न सक्छ ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    ३. ध्यान गर्न कुनै निश्चित आसन या भौतिक परिस्थितिको आवश्यकता पर्छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingThree">
                            <div class="panel-body">
                                शरीरलाई स्थिर र सहज राख्ने जुनसुकै आसनमा ध्यान गर्न सकिन्छ । ध्यान गर्नको लागि अनुकूल
                                मनको
                                स्थितिको विकास गर्नु पर्छ, भौतिक परिस्थितिको ध्यानसँग धेरै सम्बन्ध छैन । त्यस्तै, आफ्नो
                                घर,
                                कार्यालय, प्रार्थनास्थल (मन्दिर, मस्जिद, गुम्बा, गिर्जाघर), ध्यानकेन्द्र, जहाँ पनि ध्यान
                                गर्न सकिन्छ ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFour">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    ४. के ध्यान कुनै निश्चित समयमा मात्र गर्नु पर्छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingFour">
                            <div class="panel-body">
                                दिनको चौबीस घण्टामध्ये आफूलाई अनुकूल हुने कुनै पनि समयमा ध्यान गर्न सकिन्छ । हरेक दिन
                                एउटै
                                समयमा नै ध्यान गर्नु पर्छ भन्ने पनि होईन । साधारणतया, बिहानको समयमा हाम्रो शरीर र मन
                                ध्यानको
                                लागि बढि अनुकूल हुने हुनाले बिहानको समय ध्यानको लागि छुट्याउनु बढि उपयुक्त हुन्छ ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFive">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    ५. के ध्यान कुनै निश्चित धर्म या सम्प्रदायसँग सम्बन्धित छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingFive">
                            <div class="panel-body">
                                ध्यान मनुष्यको आन्तरिक शान्ति, खुशियाली र आनन्दको प्राचीनतम् र आधुनिकतम् विज्ञान हो । यो
                                कुनै धर्म, जाति या सम्प्रदायसँग सम्बन्धित छैन । पृथ्वीमा कुनै धर्म वा सम्प्रदाय प्रारम्भ
                                हुनुभन्दा अघिदेखि नै ध्यान थियो । जुनसुकै धर्मका शिक्षक, गुरु या सिद्ध पुरुषहरुको
                                ज्ञानको
                                स्रोेत पनि ध्यान नै हो ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSix">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    ६. के ध्यान गर्न कुनै परम्परा या गुरुसग दीक्षित हुनु जरुरी छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingSix">
                            <div class="panel-body">
                                ध्यान हामी आफूभित्र नै रहेको चेतनाको परम सम्भावना हो । यसका लागि कोही व्यक्ति, परम्परा
                                या
                                केन्द्रसँग दीक्षित हुनु जरुरी छैन ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSeven">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    ७. ध्यान गर्न कुनै निश्चित विचार, पूर्व मान्यता, धारणा, आस्था या मत आवश्यक छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingSeven">
                            <div class="panel-body">
                                ध्यान विशुद्ध निर्विचार चैतन्यको दशा हो । यो आफूभित्रै रहेको परम सम्भावना उजागर गर्ने
                                विशुद्ध वैज्ञानिक पद्धति हो । सबै मान्यता, धारणा आस्थाबाट मुक्त नभई ध्यानको गहिराईमा
                                प्रवेश
                                गर्न सकिंदैन ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingEight">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    ८. के ध्यान गर्न घर–व्यवहार, कार्य र सम्बन्धबाट अलग्गिनु पर्छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseEight" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingEight">
                            <div class="panel-body">
                                ध्यानले व्यक्तिगत, पारिवारिक र कर्म जीवनमा हामीलाई सहयोग पु¥याउँछ । एउटा ध्यानी व्यक्ति
                                प्रेमले भरिपूर्ण, आफ्नो कर्मप्रति निष्ठावान र व्यवहार कुशल पनि हुन्छ । त्यसकारणले, ध्यान
                                गर्न केही त्याग्नु पर्दैन । वास्तवमा, ध्यानले हाम्रो व्यक्तित्वलाई निखार्छ÷कर्ममा कुशलता
                                ल्याउँछ । यसले पारिवारिक सम्बन्ध अझ सुमधुर बनाउँछ ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingNine">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    ९. ध्यान कसैबाट सिक्नुपर्छ कि आफै गरे पनि हुन्छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseNine" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingNine">
                            <div class="panel-body">
                                ध्यानको बारेमा समाजमा अधिकांश गलत धारणाहरु प्रचलित छन् । यस्तो अवस्थामा हामी आफ्नै ज्ञान
                                र
                                प्रयासबाट मात्र ध्यान सिक्न सक्दैनौं । कोही यस्तो व्यक्तिस“ग ध्यान सिक्न उपयुक्त हुन्छ
                                जो
                                ध्यानको गहिराईमा डुबिसकेको छ र जसले अरुलाई पनि सिकाउन सक्छ । त्यस्तो व्यक्तिसँग ध्यान
                                सिक्नु
                                उपयुक्त हुन्छ । त्यस्तै, ध्यानको क्रममा अनेकौं प्रश्न र संदेहहरू पनि उठ्छन्, जसको
                                निराकरणका
                                लागि पनि ध्यानस्थ आचार्यको सत्संग अपरिहार्य हुन्छ ।
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTen">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    १०. ध्यानका फाईदाहरु के–के हुन?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTen" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingTen">
                            <div class="panel-body">
                                ध्यान केवल साधन मात्र नभई आफैमा साध्य अवस्था हो । ध्यान मार्ग पनि र गन्तव्य पनि हो ।
                                ध्यान
                                घटित हुनु परम लक्ष्य हो, जीवनको सम्भावनालाई उजागर गरेर आफ्नो गरिमामा प्रतिष्ठित हुनु हो
                                ।
                                छोटकरीमा, ध्यानको उपलब्धि ध्यान नै हो । तथापि, पाठक वर्गको जानकारीका लागि चिकित्सा
                                विज्ञान,
                                मनोविज्ञान, व्यवस्थापन एवं ध्यान केन्द्रहरुको अध्ययन, अनुसन्धान र अनुभवका आधारमा ध्यानका
                                केही प्रमुख फाईदाहरु यहाँ उल्लेख गरिएका छन् ।
                                शारीरिक फाईदाः

                                उच्च रक्तचाप नियन्त्रण, गहन विश्रामको अनुभव, मांसपेसीहरूको तनावबाट मुक्ति, उपचार एवं
                                शल्यचिकित्सा पश्चात्को स्वास्थ्यमा तीव्र सुधार, ऊर्जा एवं शक्तिमा वृद्धि, सक्रियता एवं
                                आयु
                                वृद्धि, ह्दय, पेट एवं शिरको रोगको जोखिममा कटौति, श्वास–प्रश्वासमा सहजता, शरीरको वजनमा
                                नियन्त्रण, स्नायु प्रणालीमा शिथिलता, दुःखाई एवं पीडामा कमी आदि ।
                                मानसिक फाईदाः

                                सोच विचारमा सकारात्मकता, प्रभावकारी एवं सन्तुलित व्यक्तित्वको निर्माण, आत्मविश्वास,
                                उत्साह,
                                आँट एवं जाँगरमा अभिवृद्धि, विचारमाथि नियन्त्रण, सिक्ने क्षमता, स्मरण शक्ति एवं
                                एकाग्रतामा
                                अभिवृद्धि, निर्णय एवं संकल्प क्षमताको विकास, चरित्रमा पवित्रता, नकारात्मक आदत एवं
                                कुलतबाट
                                मुक्ति, अनिद्राबाट मुक्ति, सम्बन्धमा मधुरता, नेतृत्व क्षमता, कार्य सम्पादन क्षमता,
                                जिम्मेवारीपन एवं सामाजिकतामा अभिवृद्ध्रि, कार्यमा कुशलता एवं सन्तुष्टि, मनोरोग जस्तै–
                                चञ्चलता, तनाव, भय, चिन्ता एवं अवसादवाट मुक्ति, आदि ।
                                आध्यात्मिक फाईदाः

                                जीवनमा सार्थकताको अनुभव, आफ्नो उद्देश्य र स्व–यथार्थको ज्ञान, शरीर, मन र चेतनामा
                                सामञ्जस्य,
                                विगतको शोक र भविष्यको चिन्ताबाट मुक्त भई वर्तमानमा क्षण–क्षण जिउने क्षमता प्राप्ति,
                                दिव्य
                                प्रेमको उदय, साक्षी एवं बोधमा प्रतिष्ठा, आत्मज्ञान प्राप्ति एवं परामात्मामय जीवनमा
                                प्रवेश,
                                जीवनको समग्र स्वीकार एवं तथाता, आदि ।

                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingEleven">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    ११. के ध्यान गर्ने व्यक्ति एकोहोरिने या दैनिक जिम्मेवारीबाट भाग्ने स्वभावको हुन्छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseEleven" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingEleven">
                            <div class="panel-body">
                                ध्यानले हाम्रो व्यक्तित्व सन्तुलित बनाउनुका साथै जीवनमा सम्यक्ता ल्याउँछ । आत्म–विश्वास,
                                ऊर्जा र उमंगमा अभिवृद्धि पनि ध्यानका सहज परिणाम भएको हुनाले सही ध्यानले व्यक्ति अझ बढि
                                मिलनसार,व्यवहार कुशल र कर्तव्य परायण हुन्छ ।
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwelve">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    १२. ध्यान गर्दा विचार या नकारात्मक भावले सताएमा के गर्नु पर्छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwelve" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingTwelve">
                            <div class="panel-body">
                                ध्यान गर्ने व्यक्तिको जीवनमा आउने सर्वा्धिक ठुलो चुनौति अनियन्त्रित विचारको श्रृंखला र
                                नकारात्मक भाव हो । ध्यान भनेकै चित्तमा उठ्ने विचार र ह्रदयका भावहरुलाई जागृत भईं विना
                                कुनै
                                तादात्म्य या निर्णय साक्षीपूर्वक हेर्ने कला हो । अन्नतोगत्वा, ध्यानी व्यक्तिले आफूलाई
                                सम्पूर्ण विचार एवं भावनाहरूबाट पृथक् द्रष्टा एवं बोधको रूपमा अनुभव गर्छ । त्यसकारण,
                                चित्तका
                                हरेक तरंगलाई साक्षीपूर्वक हेर्नु नै पूर्ण समाधान हो ।
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThirteen">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseThirteen" aria-expanded="false" aria-controls="collapseThirteen">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    १३. ध्यानका चरणहरु के के हुन्?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThirteen" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingThirteen">
                            <div class="panel-body">
                                त्यसो त ध्यान आफैभित्रको दिव्यतामा सिधा छलाड्ड हो । तथापि, ध्यानको क्रममा आउने केही
                                महत्वपूर्ण कोशेढुंगाहरू हुन– पहिलो–द्रष्टाभाव, दोश्रो–साक्षित्व, तेश्रो– बोध । बोधको
                                गहनतामा
                                प्रवेश गरेपछि ध्यानी आफ्नो अन्तर्आकाशमा गुञ्जायमान परमत्व ‘ॐकार’ का विभिन्न आयाममा डुबेर
                                समाधिस्थ हुन्छ । अगाडि आत्मज्ञान, निर्वाण एवं सहजको मार्ग हुँदै सच्चिदानन्दमा विश्राम
                                प्राप्त हुन्छ ।
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFourteen">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseFourteen" aria-expanded="false" aria-controls="collapseFourteen">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    १४. पूजा, जप, पाठ, प्रार्थना, तीर्थाटन आदिको ध्यानसग के सम्बन्ध छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFourteen" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingFourteen">
                            <div class="panel-body">
                                मनुष्यले गर्ने हरेक प्रयास एवं कर्मको अन्तिम अभिप्राय ध्यानको उपलव्धि हो । ध्यान गर्ने
                                व्यक्तिले पूजा, जप, पाठ लगायत हरेक कर्मलाई ध्यानको गहिराईमा डुब्ने अवसरको रूपमा देख्छ र
                                प्रयोग गर्छ।
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFifteen">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseFifteen" aria-expanded="false" aria-controls="collapseFifteen">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    १५. के ध्यान सिक्न आफ्नो गुरू, ईष्ट देवता या धर्म परिवर्तन गर्नु आवश्यक छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFifteen" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingFifteen">
                            <div class="panel-body">
                                हामीले ध्यानको माध्यमबाट आफैभित्रको गुरुतत्व र ईष्टसँग परिचित भई एक हुने अवसर प्राप्त
                                गर्छौं
                                । तसर्थ, गुरू, ईष्टदेव या धर्म परिवर्तन गर्ने प्रश्न नै उठ्दैन ।
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSixteen">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapseSixteen" aria-expanded="false" aria-controls="collapseSixteen">
                                    <i class="more-less glyphicon glyphicon-chevron-down"></i>
                                    १६. ध्यान गर्ने व्यक्तिले के के सावधानी अपनाउनु पर्छ?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSixteen" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingSixteen">
                            <div class="panel-body">
                                ध्यानको कुनै पनि नकारात्मक प्रभाव हुँदैन । ध्यानमा पारंगत आचार्यबाट ध्यान सिकेपछि हामी
                                निश्चिन्त भई ध्यानको अभ्यास गर्न सक्छौं ।
                            </div>
                        </div>
                    </div>

                </div>
            <?php } ?>

    </main>


    <aside class="col-sm-4">
        <strong><h2><?= ($lang === 'np' ? 'दैनिक कार्यक्रमहरु' : 'Daily Programmes') ?></h2></strong>
        <hr>
        <div class="">
            <div class="list-group">
                <div class="list-group-item">
                    <p>We offer specialised and unique meditation and spiritual practice programs for advanced seekers
                        of Truth. Within a few days of practice, participants will be able to attain the state of
                        spiritual growth that can otherwise be attained only through years of efforts.</p>
                </div>

                <div class="list-group-item">
                    <p>We offer specialised and unique meditation and spiritual practice programs for advanced seekers
                        of Truth. Within a few days of practice, participants </p>
                </div>

                <div class="list-group-item">
                    <p>We offer specialised and unique meditation and spiritual practice programs for advanced seekers
                        of Truth. Within a few days of practice, participants </p>
                </div>

            </div>
        </div>


    </aside>
</div>
