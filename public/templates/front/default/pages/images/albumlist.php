<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 03/08/2017
 * Time: 15:18
 */
?>


<div class="c-heading">
    <h2>Album: Images</h2>
</div>

<div class="c-content row">

    <?php foreach ($albums as $album): ?>
        <a href="<?= front_url(sprintf("images/album/%s", $album->slug)) ?>">
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="thumbnail applepie" style="height: 30rem;">
                    <img src="<?= base_url(config_item('file_upload_dir_user')['gallery_album'] . $album->image) ?>"
                         alt="meditation.png" class="thumb_img" style="">
                    <div class="caption">
                        <h4 title="<?= ($lang === 'np' ? $album->description_np : $album->description) ?>">
                            <strong>
                                <?= word_limiter($lang === 'np' ? $album->name_np : $album->name, 4) ?>
                            </strong>
                        </h4>
                        <p><?= date('j F, Y', strtotime($album->created_at)) ?></p>
                    </div>
                </div>
            </div>
        </a>
    <?php endforeach;
    if (count($albums) == 0): ?>
        <div class="alert alert-info" role="alert">No Image albums uploaded in gallery yet.</div>
    <?php endif ?>

</div>


<div class="pagination-links pull-right row">
    <?= isset($albumPagination) ? $albumPagination : '' ?>
</div>
