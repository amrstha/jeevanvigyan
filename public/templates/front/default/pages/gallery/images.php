<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 5/20/17
 * Time: 11:03 AM
 */
?>

<div class="c-heading">
    <h2><?=($lang === 'np' ? $album->name_np: $album->name)?>
        <span><?=($lang === 'np' ? $album->description_np: $album->description)?></span>
    </h2>
</div>

<div class="c-content">

    <?php foreach ($images as $image): ?>
        <div class="col-sm-4 col-md-3 col-lg-2">
            <div class="thumbnail imagepie">
                <img src="<?=base_url(config_item('file_upload_dir_user')['gallery_images'].$image->file)?>" alt="meditation.png" class="image_img">
                <div class="caption">
                    <p>
                        <?=($lang === 'np' ? $image->caption_np: $image->caption)?>
                    </p>
                </div>
            </div>
        </div>
    <?php endforeach;
    if (count($images) == 0): ?>
        <div class="alert alert-info" role="alert">No images uploaded in this album yet.</div>
    <?php endif ?>

</div>
