<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 5/20/17
 * Time: 11:03 AM
 */
?>

<div class="c-heading">
    <h2><?=($lang === 'np' ? $album->name_np: $album->name)?>
        <span><?=($lang === 'np' ? $album->description_np: $album->description)?></span>
    </h2>
</div>

<div class="c-content">

    <?php foreach ($videos as $video): ?>
        <div class="col-sm-4 col-md-3 col-lg-2">
            <div class="thumbnail imagepie" >
                <img src="<?=assets('custom/img/video-placeholder.jpg', 'admin')?>" alt="meditation.png" class="image_img">
                <div class="caption">
                    <p>
                        <?=($lang === 'np' ? $video->caption_np: $video->caption)?>
                    </p>
                    <p><a href="<?=$video->file?>" target="_blank" class="btn btn-xs btn-preview"><span class="glyphicon glyphicon-eye-open"></span> Watch</a>
                    </p>
                </div>
            </div>
        </div>
    <?php endforeach;
    if (count($videos) == 0): ?>
        <div class="alert alert-info" role="alert">No videos uploaded in this album yet.</div>
    <?php endif ?>

</div>
