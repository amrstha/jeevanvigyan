<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 5/28/17
 * Time: 8:43 AM
 */
?>

<div class="row article">
    <main class="col-sm-8">

        <div class="c-heading">
            <h2><?=($lang === 'np' ? $article->title_np : $article->title)?></h2>
        </div>

        <div class="c-content">
            <?=($lang === 'np' ? $article->content_np : $article->content)?>
        </div>

    </main>

    <?php theme('partials', 'sidebar', []) ?>
</div>
