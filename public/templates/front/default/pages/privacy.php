<div class="c-heading">
    <h2><?= ($lang === 'np' ? 'Privacy Policy' : 'Privacy Policy') ?></h2>
</div>

<div style="text-align: left">
    <p>
        <strong>Effective Date: 2 April, 2018</strong>
    </p>

    <p>
        <strong>Introduction</strong>
    </p>
    <p>
        Our privacy policy will help you understand what information
        we collect at Jeevan Vigyan, how Jeevan Vigyan uses it, and what choices you have.
    </p>
    <p>
        When we talk about &ldquo;Jeevan Vigyan,&rdquo; &ldquo;we,&rdquo; &ldquo;our,&rdquo; or &ldquo;us&rdquo;
        in this policy, we are referring to Jeevan Vigyan Kendra, the company which provides the Services.
        When we talk about the &ldquo;Services&rdquo; in this policy, we are referring to our online media
        publication and information management portal. Our Services are currently available for
        use via a web browser or applications specific to your desktop or mobile device.
    </p>
    <p>
        <strong>Information we collect and receive</strong>
    </p>
    <ol>
        <li><em> Customer Data</em></li>
    </ol>
    <p>Content and information submitted by users to the Services is referred to in this policy as&nbsp;<strong>&ldquo;Customer Data.&rdquo;</strong>&nbsp;As further explained below, Customer Data is controlled by the organization or other third party that created the community (the&nbsp;<strong>&ldquo;Customer&rdquo;</strong>). Where Jeevan Vigyan collects or processes Customer Data, it does so
        on behalf of the Customer.</p>
    <p>If you create a user account, you are a &ldquo;user,&rdquo; as further described in our User Terms and Conditions. If you are using the Services by invitation of a Customer, whether that Customer is your employer, another organization, or an individual, that Customer determines its own policies regarding storage, access, modification, deletion, sharing, and retention of Customer Data which
        may apply to your use of the Services. Please check with the Customer about the policies and settings it has in place.</p>
    <ol start="2">
        <li><em> Other information</em></li>
    </ol>
    <p>Jeevan Vigyan may also collect and receive the following information:</p>
    <ul>
        <li><strong>Account creation information.</strong>&nbsp;Users may provide information such as an email address, phone number, and password to create an account.</li>
        <li><strong>Community setup information.</strong>&nbsp;When a Customer creates a community using the Services, we may collect an email address, a community name, community photo, domain details (such as community-name.Jeevan Vigyan.com), user name for the individual setting up the community, and password. We may also collect administrative team contact info, such as a mailing address. For
            more information on community set-up.
        </li>
        <li><strong>Services usage information.</strong>&nbsp;This is information about how you are accessing and using the Services, which may include administrative and support communications with us and information about the communities, channels, people, features, content, and links you interact with, and what third party integrations you use (if any).</li>
        <li><strong>Contact information.</strong>&nbsp;With your permission, any contact information you choose to import is collected (such as an address book from a device) when using the Services.</li>
        <li><strong>Log data.</strong>&nbsp;When you use the Services our servers automatically record information, including information that your browser sends whenever you visit a website or your mobile app sends when you are using it. This log data may include your Internet Protocol address, the address of the web page you visited before using the Services, your browser type and settings, the
            date and time of your use of the Services, information about your browser configuration and plug-ins, language preferences, and cookie data.
        </li>
        <li><strong>Device information.</strong>&nbsp;We may collect information about the device you are using the Services on, including what type of device it is, what operating system you are using, device settings, application IDs, unique device identifiers, and crash data. Whether we collect some or all of this information often depends on what type of device you are using and its settings.
        </li>
        <li><strong>Geo-location information.</strong>&nbsp;Precise GPS location from mobile devices is collected only with your permission. WiFi and IP addresses received from your browser or device may be used to determine approximate location.</li>
        <li><strong>Services integrations.</strong>&nbsp;If, when using the Services, you integrate with a third party service, we will connect that service to ours. The third party provider of the integration may share certain information about your account with Jeevan Vigyan. However, we do not receive or store your passwords for any of these third party services. For more information on service
            integrations.
        </li>
        <li><strong>Third party data.</strong>&nbsp;Jeevan Vigyan may also receive information from affiliates in our corporate group, our partners, or others that we use to make our own information better or more useful. This might be aggregate level information, such as which IP addresses go with which zip codes, or it might be more specific information, such as about how well an online
            marketing or email campaign performed.
        </li>
    </ul>
    <p><strong>Our Cookie Policy</strong></p>
    <p>Jeevan Vigyan use cookies and similar technologies to provide and support our websites and Services, as more fully explained in our Cookie Policy.</p>
    <p><strong>How we use your information</strong></p>
    <p>We use your information to provide and improve the Services.</p>
    <ol>
        <li><em> Customer Data</em></li>
    </ol>
    <p>Jeevan Vigyan may access and use Customer Data as reasonably necessary and in accordance with Customer&rsquo;s instructions to (a) provide,
        maintain and improve the Services; (b) to prevent or address service, security, technical issues or at a Customer&rsquo;s request
        in connection with customer support matters; (c) as required by law or as permitted by the
        <a style="display: inline" href="https://slack.com/user-data-request-policy"> Data Request Policy </a>
         and (d) as set forth in our agreement with the Customer or as expressly permitted in writing by the Customer.
        Additional information about Jeevan Vigyan&rsquo;s confidentiality and security practices with respect to Customer Data is available at our
        <a style="display: inline" href="https://slack.com/security-practices">Security Practices page</a>.</p>
    <ol start="2">
        <li><em> Other information</em></li>
    </ol>
    <p><strong>We use other kinds of information in providing the Services. Specifically:</strong></p>
    <ul>
        <li><strong>To understand and improve our Services.</strong>&nbsp;We carry out research and analyze trends to better understand how users are using the Services and improve them.</li>
        <li><strong>To communicate with you by:</strong></li>
        <ul>
            <li><strong>Responding to your requests.</strong>&nbsp;If you contact us with a problem or question, we will use your information to respond.</li>
            <li><strong>Sending emails and Jeevan Vigyan messages.</strong>&nbsp;We may send you Service and administrative emails and messages. We may also contact you to inform you about changes in our Services, our Service offerings, and important Service related notices, such as security and fraud notices. These emails and messages are considered part of the Services and you may not opt-out of
                them. In addition, we sometimes send emails about new product features or other news about Jeevan Vigyan. You can opt out of these at any time.
            </li>
        </ul>
        <li><strong>Billing and account management.</strong>&nbsp;We use account data to administer accounts and keep track of billing and payments.</li>
        <li><strong>Communicating with you and marketing.</strong>&nbsp;We often need to contact you for invoicing, account management and similar reasons. We may also use your contact information for our own marketing or advertising purposes. You can opt out of these at any time.</li>
        <li><strong>Investigating and preventing bad stuff from happening.</strong>&nbsp;We work hard to keep the Services secure and to prevent abuse and fraud.</li>
    </ul>
    <p>This policy is not intended to place any limits on what we do with data that is aggregated and/or de-identified so it is no longer associated with an identifiable user or Customer of the Services.</p>
    <p><strong>Your choices</strong></p>
    <ol>
        <li><em> Customer Data</em></li>
    </ol>
    <p>Customer provides us with instructions on what to do with Customer Data. A Customer has many choices and control over Customer Data.</p>
    <ol start="2">
        <li><em> Other information</em></li>
    </ol>
    <p>If you have any questions about your information, our use of this information, or your rights when it comes to any of the foregoing, contact us at&nbsp;<a style="display: inline;" href="mailto:feedback@jeevanvigyan.com">feedback@jeevanvigyan.com</a>.</p>
    <p><strong><em>Other Choices</em></strong></p>
    <p>In addition, the browser you use may provide you with the ability to control cookies or other types of local data storage. Your mobile device may provide you with choices around how and whether location or other data is collected and shared. Jeevan Vigyan does not control these choices, or default settings, which are offered by makers of your browser or mobile device operating system.</p>
    <p><strong>Sharing and Disclosure</strong></p>
    <p>There are times when information described in this privacy policy may be shared by Jeevan Vigyan. This section discusses only how Jeevan Vigyan may share such information. Customers determine their own policies for the sharing and disclosure of Customer Data. Jeevan Vigyan does not control how Customers or their third parties choose to share or disclose Customer Data.</p>
    <ol>
        <li><em> Customer Data</em></li>
    </ol>
    <p>Jeevan Vigyan may share Customer Data in accordance with our agreement with the Customer and the Customer&rsquo;s instructions, including:</p>
    <ul>
        <li><strong>With third party service providers and agents.</strong>&nbsp;We may engage third party companies or individuals to process Customer Data.</li>
        <li><strong>With affiliates.</strong>&nbsp;We may engage affiliates in our corporate group to process Customer Data.</li>
        <li><strong>With third party integrations.</strong>&nbsp;Jeevan Vigyan may, acting on our Customer&rsquo;s behalf, share Customer Data with the provider of an integration added by Customer. Jeevan Vigyan is not responsible for how the provider of an integration may collect, use, and share Customer Data.</li>
    </ul>
    <ol start="2">
        <li><em> Other information</em></li>
    </ol>
    <p>Jeevan Vigyan may share other information as follows:</p>
    <ul>
        <li><strong>About you with the Customer.</strong>&nbsp;There may be times when you contact Jeevan Vigyan to help resolve an issue specific to a community of which you are a member. In order to help resolve the issue and given our relationship with our Customer, we may share your concern with our Customer.</li>
        <li><strong>With third party service providers and agents.</strong>&nbsp;We may engage third party companies or individuals, such as third party payment processors, to process information on our behalf.</li>
        <li><strong>With affiliates.</strong>&nbsp;We may engage affiliates in our corporate group to process other information.</li>
    </ul>
    <ol start="3">
        <li><em> Other types of disclosure</em></li>
    </ol>
    <p>Jeevan Vigyan may share or disclose Customer Data and other information as follows:</p>
    <ul>
        <li><strong>During changes to our business structure.</strong>&nbsp;If we engage in a merger, acquisition, bankruptcy, dissolution, reorganization, sale of some or all of Jeevan Vigyan's assets, financing, acquisition of all or a portion of our business, a similar transaction or proceeding, or steps in contemplation of such activities (e.g. due diligence).</li>
        <li><strong>To comply with laws.</strong>&nbsp;To comply with legal or regulatory requirements and to respond to lawful requests, court orders and legal process.</li>
        <li><strong>To enforce our rights, prevent fraud and for safety.</strong>&nbsp;To protect and defend the rights, property, or safety of us or third parties, including enforcing contracts or policies, or in connection with investigating and preventing fraud.</li>
    </ul>
    <p>We may disclose or use aggregate or de-identified information for any purpose. For example, we may share aggregated or de-identified information with our partners or others for business or research purposes like telling a prospective Jeevan Vigyan Customer the average number of messages sent within a Jeevan Vigyan community in a day or partnering with research firm or academics to explore
        interesting questions about workplace communications.</p>
    <p><strong>Security</strong></p>
    <p>Jeevan Vigyan takes security seriously. We take various steps to protect information you provide to us from loss, misuse, and unauthorized access or disclosure. These steps take into account the sensitivity of the information we collect, process and store, and the current state of technology.</p>
    <p>To learn more about current practices and policies regarding security and confidentiality of Customer Data and other information, please see our&nbsp;<a style="display: inline" href="https://slack.com/security-practices">Security Practices</a>; we keep that document updated as these practices evolve over time.</p>
    <p><strong>Children&rsquo;s information</strong></p>
    <p>Our Services are not directed to children under 13. If you learn that a child under 13 has provided us with personal information without consent, please contact us.</p>
    <p><strong>Changes to this Privacy Policy</strong></p>
    <p>We may change this policy from time to time, and if we do we will post any changes on this page. If you continue to use the Services after those changes are in effect, you agree to the revised policy.</p>
    <p><strong><em>&nbsp;</em></strong></p>
    <p><strong><em>Contacting Jeevan Vigyan</em></strong></p>
    <p>Please also feel free to contact us if you have any questions about Jeevan Vigyan&rsquo;s Privacy Policy or practices. You may contact us at&nbsp;<a style="display: inline" href="mailto:feedback@jeevanvigyan.com">feedback@jeevanvigyan.com</a>&nbsp;or at our mailing address below:</p>
    <p><strong>Jeevan Vigyan Kendra<br/> </strong>Chabahil, Kathmandu<br/> Nepal</p>
    <p>&nbsp;</p>
</div>


