<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 5/20/17
 * Time: 11:37 AM
 */
?>
<div class="c-heading">
    <h2><?= ($lang === 'np' ? 'सम्पर्क गर्नुहोस' : 'Contact Us') ?></h2>
</div>

<div class="c-content">

    <p>
        <?= ($lang === 'np' ? 'तलको फर्म प्रयोग गरेर हामीलाई सुझाब, सल्ला र प्रतिकृया लेखेर पठाउन सक्नुहुन्छ' : 'Use this form to write some messages, complains, information or any kinda things that you consider to share with us.') ?>
    </p>

    <hr class="divider"/>

    <div class="contact-form col-sm-6">
        <p class="small">
            Fields with <span class="text-red">*</span> are mandatory.
        </p>

        <?php

        $attributes = [
            'class' => 'form-horizontal',
            'role' => 'form',
            'name' => 'contact_form',
            'id' => 'contact_form'
        ];
        $action = front_url('contact/add');
        echo form_open($action, $attributes);

        ?>

        <div class="form-group">
            <label for="full_name">Full Name
                <span class="text-red">*</span>
                <span class="fa fa-question-circle help" data-toggle="tooltip" data-placement="right"
                      title="Required. Enter your Full Name."></span>
            </label>
            <input
                    type="text"
                    name="name"
                    id="full_name"
                    class="form-control"
                    placeholder="How may we call you?"
                    value=""
                    required=""
            >
        </div>

        <div class="form-group">
            <label for="email">Email
                <span class="text-red">*</span>
                <span class="fa fa-question-circle help" data-toggle="tooltip" data-placement="right"
                      title="Required. Enter your valid email."></span>
            </label>
            <input
                    type="email"
                    name="email"
                    id="email"
                    class="form-control"
                    placeholder="How may we contact you?"
                    value=""
                    required=""
            >
        </div>

        <div class="form-group">
            <label for="subject">Subject
                <span class="text-red">*</span>
                <span class="fa fa-question-circle help" data-toggle="tooltip" data-placement="right"
                      title="Required. Enter appropriate subject so that we can response you as soon as possible."></span>
            </label>
            <input
                    type="text"
                    name="subject"
                    id="subject"
                    class="form-control"
                    placeholder="What is a reason behind this message?"
                    value=""
                    required=""
            >
        </div>

        <div class="form-group">
            <label for="message">Message
                <span class="text-red">*</span>
                <span class="fa fa-question-circle help" data-toggle="tooltip" data-placement="right"
                      title="Required. Enter your message."></span>
            </label>
            <textarea
                    name="message"
                    id="message"
                    class="form-control vertical"
                    placeholder="Write your message here..."
                    required=""
            ></textarea>
        </div>

        <div class="form-group">
            <input
                    type="submit"
                    name="submit"
                    id="submit"
                    value="Send"
                    class="btn btn-default"
            >
        </div>

        <?= form_close() ?>
    </div>

    <div class="col-sm-6">
        <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.0215142257384!2d85.34202059218026!3d27.716622005268633!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb1985611baa13%3A0x3c9b84940e98408a!2sJeevan+Vigyan!5e0!3m2!1sen!2snp!4v1496560499890"
                width="600" height="450" frameborder="0" style="border:0" allowfullscreen>

        </iframe>
    </div>

    <div class="clearfix"></div>

</div>


