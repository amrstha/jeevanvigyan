<?php
?>

<div class="row article">
    <main class="col-sm-8">

        <div class="c-heading">
        <h2 style="color:black;"><?= ($lang === 'np' ? 'घटनाहरू ' : 'Events') ?></h2>
        </div>

        <div class="c-content" style="overflow:scroll;height:600px;">
            <?php foreach ($events as $event): ?>
            <div class="event-list">
                <header class="col-sm-12">
                <div class="row" style="margin-top: 5px;">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <strong><?= ($lang === 'np' ? 'सुरु हुने मिति:' : 'Start Date:') ?></strong>
                            <div><code><?= date("M d, l", strtotime($event->date_start)); ?></code></div>
                        </div>
                        <div class="col-md-6">
                            <h2><?= ($lang === 'np' ? $event->title_np : $event->title) ?></h2>
                        </div>
                        <div class="col-md-3">
                            <strong><?= ($lang === 'np' ? 'सकिने मिति:' : 'End Date:') ?></strong>
                            <div><code>
                                    <?= date("M d, l", strtotime($event->date_end)); ?>
                                </code></div>
                        </div>
                    </div>
                </header>
                <div class="col-sm-4">
                    <img src="<?= base_url(config_item('file_upload_dir_user')['events'] . $event->cover_pics) ?>"
                    class="thumbnail"">
                </div>

                <div class="col-sm-8">
                <p><?php echo word_limiter($lang == 'np' ? $event->description_np : $event->description, 25); ?> </p>
                    <a href="<?= front_url('event/view/' . url_encrypt($event->id)) ?>" class="btn btn-default btn-xs">read more</a>
                </div>

                <div class="clearfix"></div>
            </div>
            <?php endforeach; ?>
        </div>
    </main>
   <aside class="col-sm-4">
    <strong><h2><?=($lang === 'np' ? 'दैनिक कार्यक्रमहरु' : 'Daily Programmes')?></h2></strong>
    <hr>
    <div class="">
        <div class="list-group">
            <div class="list-group-item">
            <p>We offer specialised and unique meditation and spiritual practice programs for advanced seekers of Truth. Within a few days of practice, participants will be able to attain the state of spiritual growth that can otherwise be attained only through years of efforts.</p>
            </div>
  
            <div class="list-group-item">
            <p>We offer specialised and unique meditation and spiritual practice programs for advanced seekers of Truth. Within a few days of practice, participants </p>
            </div>
 
            <div class="list-group-item">
            <p>We offer specialised and unique meditation and spiritual practice programs for advanced seekers of Truth. Within a few days of practice, participants </p>
            </div>
           
        </div>
    </div>
    

</aside>
</div>
