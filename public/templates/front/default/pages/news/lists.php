<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 5/28/17
 * Time: 9:24 AM
 */
?>

<div class="row article">
    <main class="col-sm-8">

        <div class="c-heading">
            <h2><?=($lang === 'np' ? 'समचार' : 'News')?></h2>
        </div>

        <div class="c-content">
            <?php foreach ($news as $newslist): ?>
            <div class="news-list">
                <header class="col-sm-12">
                    <h3><a href="<?=front_url(sprintf("news/%s", $newslist->slug))?>"><?=($lang === 'np' ? $newslist->title_np : $newslist->title)?></a></h3>
                    <time><?=date('Y-m-d', strtotime($newslist->pub_date))?></time>
                </header>
                <div class="col-sm-8">
                    <p><?=excerpt(500, $lang === 'np' ? $newslist->content_np : $newslist->content)?> ...</p>
                    <a href="<?=front_url(sprintf("news/%s", $newslist->slug))?>" class="btn btn-default btn-xs">read more</a>
                </div>

                <div class="col-sm-4">
                    <img src="<?=base_url(sprintf("%s/%s", Utility::getUploadDir('pages'), $newslist->image))?>" class="thumbnail">
                </div>

                <div class="clearfix"></div>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="pagination-links pull-right row">
            <?=isset($newsPagination)?$newsPagination:''?>
        </div>
    </main>

    <?php theme('partials', 'sidebar', []) ?>
</div>
