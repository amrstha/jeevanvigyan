<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/12/17
 * Time: 3:19 PM
 */
?>
<div class="navbar nav_title" style="border: 0;">
    <a href="<?= admin_url('dashboard') ?>" class="site_title">
        <img class="logo" src="<?= assets('res/sites-general/logo-full.png') ?>" alt="JeevanViggyan">
    </a>
</div>

<div class="clearfix"></div>

<!-- menu profile quick info -->
<div class="profile">
    <div class="profile_pic">
        <img src="<?= userProfilePics(getUser()) ?>" alt="<?= getUser()->username ?>" class="img-circle profile_img">
    </div>
    <div class="profile_info">
        <span>Hello,</span>
        <h2><?= getFullName(getUser()) ?></h2>
    </div>
</div>
<!-- /menu profile quick info -->
<div class="clearfix"></div>


<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <ul class="nav side-menu">

            <!-- Dashboard -->
            <li class="<?= activeNav('dashboard') ?>">
                <a href="<?= admin_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a>
            </li>

            <!-- About Us -->
            <li class="<?=activeNav('about')?>">
                <a href="<?=admin_url('about')?>"><i class="fa fa-info"></i> About US</a>
            </li>

            <!-- Slider -->
            <li class="<?= activeNav('slider') ?>">
                <a href="<?= admin_url('slider') ?>"><i class="fa fa-image"></i> Slider</a>
            </li>

            <!-- Slider -->
            <li class="<?= activeNav('gallery') ?>">
                <a href="<?= admin_url('gallery/album') ?>"><i class="fa fa-table"></i> Gallery</a>
            </li>

            <!-- Page Managment -->
            <li class="<?= activeNav('pages') ?>">
                <a href="javascript:;"><i class="fa fa-files-o"></i> Pages Managment <span
                            class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li class="<?= activeNav(['pages', 'category']) ?>"><a href="<?= admin_url('pages/category') ?>">Categories</a>
                    </li>
                    <li class="<?= activeNav(['pages', 'article']) ?>"><a href="<?= admin_url('pages/article') ?>">Articles</a>
                    </li>
                    <li class="<?= activeNav(['home', 'home']) ?>"><a href="<?= admin_url('pages/home') ?>">Home</a>
                    </li>
                </ul>
            </li>

            <!-- Menu Managment -->
            <li class="<?= activeNav('menu') ?>">
                <a href="javascript:;"><i class="fa fa-bars"></i> Menu Managment <span
                            class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li class="<?= activeNav(['menu', 'social']) ?>"><a
                                href="<?= admin_url('menu/social') ?>">Social</a></li>

                </ul>
            </li>

            <!-- Meditation-->
            <li class="<?= activeNav('meditation') ?>">
                <a href="<?= admin_url('meditation') ?>"><i class="fa fa-user-o"></i> Meditation</a>
            </li>

            <!-- Upcoming Events-->
            <li class="<?= activeNav('events') ?>">
                <a href="<?= admin_url('events/lists') ?>"><i class="fa fa-calendar"></i> Events</a>
            </li>

            <!-- Programmes -->
            <li class="<?= activeNav('program') ?>">
                <a href="<?= admin_url('program/lists') ?>"><i class="fa fa-briefcase"></i> Program</a>
            </li>

            <!-- NewsLetter -->
            <li class="<?=activeNav('newsletter')?>">
                <a href="javascript:;"><i class="fa fa-newspaper-o"></i> NewsLetter <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li class="<?=activeNav(['newsletter','subscribers'])?>"><a href="<?=admin_url('newsletter/subscribers')?>">Subscribers</a></li>

                </ul>
            </li>

            <!-- User Managment -->
            <li class="<?= activeNav('users') ?>">
                <a href="javascript:;"><i class="fa fa-users"></i> User Managment <span
                            class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li class="<?= activeNav(['users', 'groups']) ?>">
                        <a href="<?= admin_url('users/groups/lists') ?>">Groups</a>
                    </li>
                    <li class="<?= activeNav(['users', 'lists']) ?>">
                        <a href="<?= admin_url('users/lists') ?>">Users</a>
                    </li>
                    <li class="<?= activeNav(['users', 'customers']) ?>">
                        <a href="<?= admin_url('users/customers/lists') ?>">Customers</a>
                    </li>
                </ul>
            </li>

            <!--Question and Answer-->
            <li class="<?= activeNav('qna') ?>">
                <a href="<?= admin_url('qna') ?>"><i class="fa fa-question"></i>Question & Answers</a>
            </li>

            <!-- Quotations-->
            <li class="<?= activeNav('quotations') ?>">
                <a href="<?= admin_url('quotations/lists') ?>"><i class="fa fa-quote-right"></i> Quotations</a>
            </li>

            <!-- Access Control -->
            <!-- <li class="<?= activeNav('access') ?>">
                <a href="javascript:;"><i class="fa fa-key"></i> Access Control <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li class="<?= activeNav(['access', 'modules']) ?>"><a href="<?= admin_url('access/modules') ?>">Modules</a></li>
                    <li class="<?= activeNav(['access', 'manage']) ?>"><a href="<?= admin_url('access/manage') ?>">Manage</a></li>
                </ul>
            </li> -->
            <!-- Organization  Setting-->
<!--            <li class="--><?//= activeNav('orgsetting') ?><!--">-->
<!--                <a href="--><?//= admin_url('settings/organizationsetting') ?><!--"><i class="fa fa-cog"></i> Setting</a>-->
<!--            </li>-->

        </ul>
    </div>
</div>
<!-- /sidebar menu -->

<!-- /menu footer buttons -->
<div class="sidebar-footer hidden-small">
    <a data-toggle="tooltip" data-placement="top" title="Settings">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Lock">
        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
    </a>
    <a data-toggle="tooltip" data-placement="top" title="Logout">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
    </a>
</div>
<!-- /menu footer buttons -->
