<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/11/17
 * Time: 5:00 PM
 */
?>
<!--
// STYLESHEETS
-->
<!-- Vendor Styles -->
<link rel="stylesheet" type="text/css" href="<?=assets('bower_components/bootstrap/dist/css/bootstrap.min.css')?>">
<link rel="stylesheet" type="text/css" href="<?=assets('bower_components/font-awesome/css/font-awesome.min.css')?>" />
<link rel="stylesheet" type="text/css" href="<?=assets('bower_components/animate.css/animate.min.css')?>" />

<!-- App styles -->
<link rel="stylesheet" type="text/css" href="<?=assets('theme/css/theme.min.css', 'admin')?>">

<!-- Custom Styles -->
<link rel="stylesheet" type="text/css" href="<?=assets('custom/css/login.styles.css', 'admin') ?>">

<!--
// IE CORRECTION
-->
<!--[if lt IE 9]>
    <script src="<?=assets('bower_components/html5shiv/dist/html5shiv.min.js') ?>"></script>
    <script src="<?=assets('bower_components/respond/dest/respond.min.js') ?>"></script>
<![endif]-->