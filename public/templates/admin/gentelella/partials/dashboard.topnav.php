<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/12/17
 * Time: 3:21 PM
 */
?>
<div class="nav toggle">
    <a id="menu_toggle"><i class="fa fa-bars"></i></a>
</div>

<ul class="nav navbar-nav navbar-right">

    <li class="">
        <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <img src="<?=userProfilePics(getUser())?>" alt="<?=getUser()->username?>"> <?=getUser()->username?>
            <span class=" fa fa-angle-down"></span>
        </a>
        <ul class="dropdown-menu dropdown-usermenu pull-right">
            <li><a href="<?=admin_url('settings/profile')?>"> Edit Profile</a></li>
            <li><a href="<?=admin_url('settings/accounts')?>"> Accounts</a></li>
            <li><a href="javascript:;">Help</a></li>
            <li><a href="<?=admin_url('logout')?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
        </ul>
    </li>

    <li role="view_public_page" class="">
        <a href="<?=front_url()?>" class="" target="_blank">
            <i class="fa fa-space-shuttle"></i>
        </a>
    </li>

    <li role="presentation" class="dropdown">
        <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-envelope-o"></i>
            <?php $unreadCount = $this->contact->getAll(true); ?>
            <span class="badge bg-green"><?=count($unreadCount)?></span>
        </a>
        <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
         <?php 
            $getMessages = [];
            $getMessages = $this->contact->getAll(false,3);

            foreach ($getMessages as $getMessage) : ?>
            <li>
                <a href="<?=admin_url('contact/message_details/'.url_encrypt($getMessage->id))?>" class="<?=($getMessage->is_read == 0?'unread-message':'')?> tooltip-info loadForm" data-toggle="tooltip" data-successbtn="" data-addclass="bootbox-modal-sm" title="view" >
                    <span class="image"><img src="<?=userProfilePics(getUser())?>" alt="Profile Image" /></span>
                    <span>
                      <span><strong><?=$getMessage->name?></strong></span>
                      <span class="time">
                            <?php echo timespan(strtotime($getMessage->created_at), time(), 1); ?>
                      </span>
                    </span>
                    <span class="message">
                      <?=word_limiter($getMessage->message, 12)?>
                    </span>
                </a>
            </li>
        <?php endforeach; ?>
            <li>
                <div class="text-center">
                    <a href="<?=admin_url('contact/messages')?>">
                        <strong>See All Messages</strong>
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </li>
        </ul>
    </li>
</ul>