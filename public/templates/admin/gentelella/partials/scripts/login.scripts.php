<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/12/17
 * Time: 1:51 PM
 */
theme('partials/global', 'scripts', [], false, null, null) ?>

<!-- Vendor scripts -->
<script src="<?=assets('bower_components/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<script src="<?=assets('bower_components/jquery-validation/dist/jquery.validate.min.js')?>"></script>

<!-- App scripts -->
<script type="text/javascript" src="<?=assets('custom/js/login.main.js', 'admin')?>"></script>