<?php
/**
 * Created by PhpStorm.
 * User: ManjulSigdel
 * Date: 10/06/2017
 * Time: 15:10
 */
?>

<script type="application/javascript">

    $(function () {

        $('.ajax-loader-setting').hide();

        $(document).off('change', '.organizational_setting_field').on('change', '.organizational_setting_field', function (e) {
            var obj = $(this),
                val = obj.val(),
                key = obj.attr('name'),
                url = $("#org-setting-form").attr('action'),
                formdata = new FormData();

            formdata.append('key', key);
            formdata.append('value', val);
            formdata.append(csrf_token_name, csrf_token);

            $.ajax({
                url: url,
                type: 'post',
                data: formdata,
                dataType: 'json',
                processData: false,
                contentType: false,
                beforeSend: function () {
                    $('.ajax-loader-setting').show();
                },
                success: function (resp) {
                    $('.ajax-loader-setting').hide();

                    if (resp.status == 'success') {
                        // show success message
                        $.fn.notification('success', resp.message);
                    } else {
                        // show error message
                        var msg = resp.message;
                        if (resp.message == 'form_error') {
                            msg = 'Invalid data.';
                        }
                        $.fn.notification('error', msg);
                    }

                    // update csrf token
                    csrf_token_name = resp.data.key;
                    csrf_token = resp.data.value;
                }
            })
        });

    });

</script>
