<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/11/17
 * Time: 3:43 PM
 */
$viewData = [];
?>
<?php theme('partials', 'htmlhead', $viewData, false, '.php', 'login') ?>
<body class="login">
<!--[if lt IE 7]>
<p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<noscript><p class="alert alert-danger">You must enable <strong>javascript</strong> for better experiences.</p></noscript>

<div style="margin: 5% auto">
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div id="alert-message" class="error-messgage-wrapper"><?=alert()?></div>

    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <?php
                $attributes = [
                    'class' => '',
                    'role'  => 'form',
                    'name'  => 'loginForm',
                    'id'    => 'loginForm',
                    'style' => ''
                ];
                $action = admin_url('login');
                $hidden = ['requested_url'=>$requested_url];
                echo form_open($action, $attributes, $hidden);
                ?>
                    <h1>Admin Login</h1>
                    <div class="input-field">
                        <input type="text"
                               autofocus=""
                               class="form-control"
                               name="identity"
                               id="identity"
                               placeholder="Username"
                               title="Please enter you username"
                               required="" />
                    </div>
                    <div class="input-field">
                        <input type="password"
                               class="form-control"
                               name="password"
                               id="password"
                               title="Please enter your password"
                               placeholder="Password"
                               required="" />
                    </div>
                    <div>
                        <input type="submit" class="btn btn-success btn-block" value="Login">
                        <a class="reset_pass to_register text-center" href="#signup">Lost your password?</a>
                    </div>

                    <div class="clearfix"></div>

                    <div class="separator">
                        <div class="clearfix"></div>
                        <br />

                        <div>
                            <h1><img class="logo" src="<?=assets('res/sites-general/logo-full.png')?>" alt="JeevanVigyan"></h1>
                            <p>&copy; <?=date('Y')?> All Rights Reserved. Powered by <a href="http://www.mylifemark.com">MyLifeMark</a></p>
                        </div>
                    </div>
                <?=form_close()?>
            </section>
        </div>

        <div id="register" class="animate form registration_form">
            <section class="login_content">
                <?php
                $attributes = [
                    'class' => '',
                    'role'  => 'form',
                    'name'  => 'password-reset',
                    'id'    => 'password-reset'
                ];
                $action = admin_url('reset-password');
                echo form_open($action, $attributes, $hidden);
                ?>
                    <h1>Forget Password</h1>
                    <p class="help-block">Please fill in your registered email address, we will send you a mail with instructions on how to reset your password.</p>
                    <div>
                        <input type="email"
                               autofocus=""
                               class="form-control"
                               name="email"
                               id="email"
                               placeholder="Email"
                               title="Please enter you registered email"
                               required="" />
                    </div>
                    <div>
                        <input type="submit" class="btn btn-success btn-block" value="Reset">
                        <a class="reset_pass to_register text-center" href="#signin">Login to your account.</a>
                    </div>

                    <div class="clearfix"></div>

                    <div class="separator">
                        <div class="clearfix"></div>
                        <br />

                        <div>
                            <h1><img class="logo" src="<?=assets('res/sites-general/logo-full.png')?>" alt="JeevanVigyan"></h1>
                            <p>&copy; <?=date('Y')?> All Rights Reserved. Powered by <a href="http://www.mylifemark.com">MyLifeMark</a></p>
                        </div>
                    </div>
                <?=form_close()?>
            </section>
        </div>
    </div>
</div>

<!--
// SCRIPTS
-->
<?php theme('partials/scripts', 'scripts', $viewData, false, '.php', 'login') ?>
</body>
</html>