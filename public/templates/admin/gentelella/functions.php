<?php
/**
 * Created by PhpStorm.
 * User: puncoz
 * Date: 1/11/17
 * Time: 4:24 PM
 */

(defined('BASEPATH')) || exit('No direct script access allowed');

if (!function_exists('theme')) {
    function theme($dir, $page, $data = [], $return = false, $ext = '.php', $theme = 'dashboard')
    {
        $page = '/' . (!is_null($dir) ? $dir . '/' : '') . (!is_null($theme) ? $theme . '.' : '') . $page . (!is_null($ext) ? $ext : '');
        return get_instance()->load->view(ADMIN_TMPL.$page, $data, $return);
    }
}

if (!function_exists('activeNav')) {
    function activeNav($slug)
    {
        $uri_lvl = uriSegments();

        if (!is_array($slug)) {
            return ($slug == $uri_lvl[0]) ? 'active' : false;
        }

        foreach ($slug as $key => $value) {
            if ($value != $uri_lvl[$key]) {
                return false;
            }
        }
        return 'active';
    }
}

if (!function_exists('uriSegments')) {
    function uriSegments()
    {
        $CI =& get_instance();
        $first = $CI->uri->segment(2);
        return [
            empty($first) ? 'dashboard' : $first,
            $CI->uri->segment(3),
            $CI->uri->segment(4),
            $CI->uri->segment(5),
        ];
    }
}

if (!function_exists('breadcrumbOutput')) {
    function breadcrumbOutput()
    {
        return '<ol class="hbreadcrumb breadcrumb">' . get_instance()->breadcrumb->output() . '</ol>';
    }
}

if (!function_exists('getUser')) {
    function getUser()
    {
        return get_instance()->authentication->getUser();
    }
}

if (!function_exists('getFullName')) {
    function getFullName($user)
    {
        return trim($user->name_prefix.' '
            .$user->first_name.' '
            .$user->middle_name.' '
            .$user->last_name.' '
            .$user->name_suffix);
    }
}

if (!function_exists('userProfilePics')) {
    function userProfilePics($user)
    {
        $pics_dir = get_instance()->authentication->getUploadDir('profile_pics');
        if (empty($user->profile_pic)) {
            return strtolower($user->gender) == 'female' ? assets('custom/img/female-blank.jpg', 'admin') : assets('custom/img/male-blank.png', 'admin');
        }
        return base_url($pics_dir.$user->profile_pic);
    }
}

if (!function_exists('alert')) {
    function alert()
    {
        $CI =& get_instance();
        $success_alert  = $CI->session->get_alert('success');
        $warning_alert  = $CI->session->get_alert('warning');
        $info_alert     = $CI->session->get_alert('info');
        $error_alert    = $CI->session->get_alert('error');

        $alert_type = (
        (!empty($success_alert)) ? 'success' : (
        (!empty($warning_alert)) ? 'warning' : (
        (!empty($info_alert)) ? 'info' : (
        (!empty($error_alert)) ? 'danger' : ''
        )
        )
        )
        );

        $alertmsg = '';
        switch ($alert_type) {
            case 'success':
                $alertmsg .= <<<__HTML__
<div class="alert alert-success" role="alert">
    <strong>Success!</strong> {$success_alert}
</div>
__HTML__;
                break;

            case 'info':
                $alertmsg .= <<<__HTML__
<div class="alert alert-info" role="alert">
    <strong>Info!</strong> {$info_alert}
</div>
__HTML__;
                break;

            case 'danger':
                $alertmsg .= <<<__HTML__
<div class="alert alert-danger" role="alert">
    <strong>Error!</strong> {$error_alert}
</div>
__HTML__;
                break;

            case 'warning':
                $alertmsg .= <<<__HTML__
<div class="alert alert-warning" role="alert">
    <strong>Warning!</strong> {$warning_alert}
</div>
__HTML__;
                break;
        }

        return $alertmsg;
    }
}

if (!function_exists('pageSpecificScripts')) {
    function pageSpecificScripts()
    {
        $uri_lvl = uriSegments();
        switch ($uri_lvl[0]) {
            case 'pages':
                if ($uri_lvl[1] == 'article' && ($uri_lvl[2] == 'add' || $uri_lvl[2] == 'edit')) {
                    return theme('partials/scripts/pages', 'article', [], true, null, null);
                } else if ($uri_lvl[1] == 'home') {
                    return theme('partials/scripts/pages', 'home', [], true, null, null);
                }
            break;

            case 'settings':
                if ($uri_lvl[1] == 'organizationsetting') {
                    return theme('partials/scripts/pages', 'organizationsetting', [], true, null, null);
                }
            break;
        }
    }
}
