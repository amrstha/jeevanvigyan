# config valid only for current version of Capistrano
lock "3.8.0"


# Application #
#####################################################################################
set :application,     'jeevanvigyan'
set :branch,          ENV["branch"] || "master"
set :user,            ENV["user"] || ENV["USER"] || "jeevanvigyan"
set :tmp_dir,         '/home/jeevanvigyan/web/tmp'



# SCM #
#####################################################################################
set :repo_url,        'git@gitlab.com:puncoz/jeevanvigyan.git'
set :repo_base_url,   :'http://gitlab.com/'
set :repo_diff_path,  :'puncoz/jeevanvigyan/compare/master...'



# Multistage Deployment #
#####################################################################################
set :stages,              %w(dev staging prod)
set :default_stage,       "staging"



# Other Options #
#####################################################################################
set :ssh_options,         { :forward_agent => true }
set :default_run_options, { :pty => true }

# Permissions #
#####################################################################################
set :use_sudo,            false
set :permission_method,   :acl
set :use_set_permissions, true
set :webserver_user,      "www-data"
set :group,               "www-data"
set :keep_releases,       1



# Set current time #
#######################################################################################
require 'date'
set :current_time, DateTime.now



# Procedures #
#######################################################################################
namespace :vendor do
    desc 'Copy vendor directory from last release'
    task :copy do
        on roles(:web) do
            puts ("--> Copy vendor folder from previous release")
            execute "vendorDir=#{current_path}/vendor; if [ -d $vendorDir ] || [ -h $vendorDir ]; then cp -a $vendorDir #{release_path}/vendor; fi;"
        end
    end
end


namespace :composer do
    desc "Running Composer Install"
    task :install do
        on roles(:app) do
            within release_path do
                execute :composer, "install"
                execute :composer, "dump-autoload -o"
            end
        end
    end
end


namespace :environment do
    desc "Set environment variables"
    task :set_variables do
        on roles(:app) do
              puts ("--> Copying environment configuration file")
              execute "cp #{fetch(:overlay_path)}/config.server.php #{release_path}/public/config.php"
        end
    end
end


namespace :jeevanvigyan do

    desc "Symbolic link for shared folders"
    task :create_symlink do
        on roles(:app) do
            within release_path do
                execute "rm -rf #{release_path}/public/uploads"
                execute "ln -s #{shared_path}/uploads/ #{release_path}/public"
            end
        end
    end

    desc "Create ver.txt"
    task :create_ver_txt do
        on roles(:all) do
            puts ("--> Copying ver.txt file")
            execute "cp #{release_path}/config/deploy/ver.txt.example #{release_path}/public/ver.txt"
            execute "sed --in-place 's/%date%/#{fetch(:current_time)}/g
                        s/%branch%/#{fetch(:branch)}/g
                        s/%revision%/#{fetch(:current_revision)}/g
                        s/%deployed_by%/#{fetch(:user)}/g' #{release_path}/public/ver.txt"
            execute "find #{release_path}/public -type f -name 'ver.txt' -exec chmod 664 {} \\;"
        end
    end

end


namespace :nginx do
    desc 'Reload nginx server'
        task :reload do
            on roles(:all) do
            execute :sudo, :service, "nginx reload"
        end
    end
end


namespace :php_fpm do
    desc 'Restart php-fpm'
        task :restart do
            on roles(:all) do
            execute :sudo, :service, "php7.0-fpm restart"
        end
    end
end



# Procedures Calls #
#######################################################################################
namespace :deploy do
    after :starting, "vendor:copy"
    after :updated, "composer:install"
    after :updated, "environment:set_variables"
    after :published, "jeevanvigyan:create_symlink"
    after :finished, "jeevanvigyan:create_ver_txt"
end

after "deploy", "nginx:reload"
after "deploy", "php_fpm:restart"




# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml", "config/secrets.yml"

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5
